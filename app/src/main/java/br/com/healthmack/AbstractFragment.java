package br.com.healthmack;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.zip.Inflater;

import br.com.healthmack.utils.BaseView;
import br.com.healthmack.utils.ToastHelper;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public abstract class AbstractFragment<T> extends Fragment implements BaseView<T> {
    /**
     * The M presenter.
     */
    protected T mPresenter;
    public Unbinder unbinder;

    /**
     * Inflate a child layout with {@link LayoutInflater#inflate(int, ViewGroup)} and config ButterKnife
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(getLayoutRes(), container, false);

        unbinder = ButterKnife.bind(this, view);

        onCustomCreateView(view, container);

        return view;
    }

    @Override
    public void showProgressBar() {
        if (this.getActivity() != null && this.getActivity().findViewById(getProgressBar()) != null)
            this.getActivity().findViewById(getProgressBar()).setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        if (this.getActivity() != null && this.getActivity().findViewById(getProgressBar()) != null)
            this.getActivity().findViewById(getProgressBar()).setVisibility(View.GONE);
    }

    @Override
    public void showCommunicationError() {
        hideProgressBar();
        ToastHelper.show(this, R.string.communnication_failure, Toast.LENGTH_LONG);
    }

    public void showMessage(@StringRes int messageId) {
        hideProgressBar();
        ToastHelper.show(this, messageId, Toast.LENGTH_LONG);
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void setPresenter(T presenter) {
        mPresenter = presenter;
    }

    /**
     * Gets layout resource of fragment.
     *
     * @return the layout res
     */
    public abstract int getLayoutRes();

    /**
     * Gets main view to show the communication error.
     *
     * @return the main view
     */
    public abstract int getProgressBar();

    /**
     * Customize Views of ButterKnife injection with necessary
     *
     * @param view      the view
     * @param container
     */
    public abstract void onCustomCreateView(View view, ViewGroup container);
}
