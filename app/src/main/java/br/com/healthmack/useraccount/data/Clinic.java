package br.com.healthmack.useraccount.data;

import android.content.Context;

import java.util.HashMap;
import java.util.Map;

import br.com.healthmack.R;
import br.com.healthmack.utils.Mask;

/**
 * Created by Ramon Cardoso on 11/10/2017.
 */

public class Clinic {

    private String address;
    private String complement;
    private String number;
    private String cep;
    private String phone;

    public Clinic() {
    }

    public Clinic(String address, String complement, String number, String cep, String phone) {
        this.address = address;
        this.complement = complement;
        this.number = number;
        this.cep = cep;
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCompleteAddress() {
        return getAddress() + ", " + getNumber() + ", " + getComplement();
    }

    public String getComplement() {
        return complement;
    }

    public void setComplement(String complement) {
        this.complement = complement;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getPhone() {
        return phone;
    }

    public String getFormattedPhone(Context context) {
        return context.getString(R.string.format_phone, getPhone().substring(0, 2),
                getPhone().substring(2, 6),
                getPhone().substring(6, 10));
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public boolean isValidAddress() {
        return this.address != null && this.address.length() > 5;
    }

    public boolean isValidNumber() {
        return this.number != null && this.number.length() > 0;
    }

    public boolean isValidCep() {
        return this.cep != null && this.cep.length() == 8;
    }

    public boolean isValidPhone() {
        return this.phone != null && this.phone.length() == 10;
    }

    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("address", address);
        result.put("complement", complement);
        result.put("number", number);
        result.put("cep", cep);
        result.put("phone", phone);
        return result;
    }
}
