/*
 * Copyright (c) 2017. App Media S/A - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Ramon Cardoso <ramon.cardoso@moving.com.br>, 9/12/17 11:43 AM
 *
 */

package br.com.healthmack.useraccount.data.source;

import javax.inject.Singleton;

import br.com.healthmack.modules.FirebaseAuthModule;
import br.com.healthmack.modules.FirebaseDatabaseModule;
import dagger.Component;

@Singleton
@Component(modules = {UserRepositoryModule.class, FirebaseDatabaseModule.class, FirebaseAuthModule.class})
public interface UserRepositoryComponent {

    UserRepository getChangePasswordRepository();
}
