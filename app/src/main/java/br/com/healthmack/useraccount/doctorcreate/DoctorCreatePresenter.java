package br.com.healthmack.useraccount.doctorcreate;

import java.util.List;

import javax.inject.Inject;

import br.com.healthmack.R;
import br.com.healthmack.useraccount.data.Clinic;
import br.com.healthmack.shared.model.User;
import br.com.healthmack.useraccount.data.source.UserDataSource;
import br.com.healthmack.useraccount.data.source.UserRepository;

final class DoctorCreatePresenter implements DoctorCreate.Presenter, UserDataSource.UserCreateCallback, UserDataSource.DoctorCreateCallback {

    private final UserDataSource mUserRepository;
    private final DoctorCreate.View mDoctorCreateView;
    private Clinic clinic;
    private User user;

    @Inject
    DoctorCreatePresenter(UserRepository userLoginRepository, DoctorCreate.View userLoginView) {
        mUserRepository = userLoginRepository;
        mDoctorCreateView = userLoginView;
    }

    @Inject
    @Override
    public void setupListeners() {
        mDoctorCreateView.setPresenter(this);
    }

    @Override
    public void createDoctor(List<String> specialties, String crm, String address, String complement,
                             String number, String cep, String phone, User user) {
        user.setCrm(crm);
        user.setSpecialties(specialties);
        this.user = user;

        if (specialties != null && specialties.size() == 0)
            mDoctorCreateView.showInvalidField(R.id.specialtieLayout, R.string.error_invalid_specialtie);

        clinic = new Clinic(address, complement, number, cep, phone);
        if (!clinic.isValidAddress())
            mDoctorCreateView.showInvalidField(R.id.clinicAddressLayout, R.string.error_invalid_address);
        if (!clinic.isValidNumber())
            mDoctorCreateView.showInvalidField(R.id.clinicNumberLayout, R.string.error_invalid_number);
        if (!clinic.isValidCep())
            mDoctorCreateView.showInvalidField(R.id.clinicCepLayout, R.string.error_invalid_cep);
        if (!clinic.isValidPhone())
            mDoctorCreateView.showInvalidField(R.id.clinicPhoneLayout, R.string.error_invalid_phone);

        if (user.isValidName() && user.isValidEmail() && user.isValidPassword() && user.isValidRePassword() &&
                clinic.isValidAddress() && clinic.isValidNumber() && clinic.isValidCep() && clinic.isValidPhone()) {
            mDoctorCreateView.showProgressBar();
            mUserRepository.saveUser(user, this);
        }
    }

    @Override
    public void loadSpecialties() {
        mDoctorCreateView.showProgressBar();
        mUserRepository.loadSpecialties(this);
    }

    @Override
    public void userCreated(User ourUser) {
        mUserRepository.saveUser(ourUser, this);
    }

    @Override
    public void userNotCreated() {
        mDoctorCreateView.showMessage(R.string.error_create_user);
    }

    @Override
    public void userSaved(User user) {
        mUserRepository.createClinic(clinic, this.user, this);
    }

    @Override
    public void userNotSaved() {
        mDoctorCreateView.showMessage(R.string.error_create_user);
    }

    @Override
    public void invalidEmail() {
        mDoctorCreateView.showInvalidField(R.id.emailLayout, R.string.error_invalid_new_email);
    }

    @Override
    public void invalidCpf() {

    }

    @Override
    public void doctorSaved() {
        mDoctorCreateView.hideProgressBar();
        mDoctorCreateView.showSuccess();
    }

    @Override
    public void doctorNotSaved() {
        mDoctorCreateView.hideProgressBar();
        mDoctorCreateView.showMessage(R.string.error_create_user);
    }

    @Override
    public void specialtiesLoaded(List<String> specialties) {
        mDoctorCreateView.hideProgressBar();
        mDoctorCreateView.prepareSpecialties(specialties);
    }
}
