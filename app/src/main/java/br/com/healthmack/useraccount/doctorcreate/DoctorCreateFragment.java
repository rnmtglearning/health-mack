package br.com.healthmack.useraccount.doctorcreate;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.TextInputLayout;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import br.com.healthmack.AbstractFragment;
import br.com.healthmack.R;
import br.com.healthmack.menu.MenuActivity;
import br.com.healthmack.shared.model.User;
import br.com.healthmack.useraccount.usercreate.UserCreateActivity;
import br.com.healthmack.utils.ActivityUtils;
import br.com.healthmack.utils.AlertDialogHelper;
import br.com.healthmack.utils.Mask;
import butterknife.BindView;
import butterknife.OnClick;

public class DoctorCreateFragment extends AbstractFragment<DoctorCreate.Presenter> implements DoctorCreate.View {

    public static final String ARG_USER = "user";
    @BindView(R.id.specialtieLayout)
    TextInputLayout specialtieLayout;
    @BindView(R.id.specialtieEdit)
    TextView specialtieEdit;
    @BindView(R.id.doctorCrmLayout)
    TextInputLayout doctorCrmLayout;
    @BindView(R.id.doctorCrmEdit)
    EditText doctorCrmEdit;
    @BindView(R.id.clinicPhoneLayout)
    TextInputLayout clinicPhoneLayout;
    @BindView(R.id.clinicPhoneEdit)
    EditText clinicPhoneEdit;
    @BindView(R.id.clinicAddressLayout)
    TextInputLayout clinicAddressLayout;
    @BindView(R.id.clinicAddressEdit)
    EditText clinicAddressEdit;
    @BindView(R.id.clinicComplementLayout)
    TextInputLayout clinicComplementLayout;
    @BindView(R.id.clinicComplementEdit)
    EditText clinicComplementEdit;
    @BindView(R.id.clinicNumberLayout)
    TextInputLayout clinicNumberLayout;
    @BindView(R.id.clinicNumberEdit)
    EditText clinicNumberEdit;
    @BindView(R.id.clinicCepLayout)
    TextInputLayout clinicCepLayout;
    @BindView(R.id.clinicCepEdit)
    EditText clinicCepEdit;
    @BindView(R.id.createButton)
    Button createButton;
    private List<String> specialties = new ArrayList<>();
    private List<String> specialtiesSelected = new ArrayList<>();
    private List<Boolean> specialtiesSelectedNumber = new ArrayList<>();

    private DialogInterface.OnMultiChoiceClickListener multiListener = new DialogInterface.OnMultiChoiceClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which, boolean isChecked) {
            if (isChecked) {
                specialtiesSelected.add(specialties.get(which));
                specialtiesSelectedNumber.add(which, true);
            } else {
                specialtiesSelected.remove(specialties.get(which));
                specialtiesSelectedNumber.add(which, false);
            }
        }
    };

    private AlertDialogHelper.SelectAllOptionsListener allListener = new AlertDialogHelper.SelectAllOptionsListener() {
        @Override
        public void onClick(List<String> itens) {
            specialtiesSelected = itens;
            for (int i = 0; i < specialtiesSelected.size(); i++) {
                specialtiesSelectedNumber.add(i, true);
            }
        }
    };

    @Override

    public int getLayoutRes() {
        return R.layout.doctor_create_frag;
    }

    @Override
    public int getProgressBar() {
        return R.id.progressBar;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_create_user, menu);
        menu.findItem(R.id.menu_create_doctor).setVisible(false);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_create_doctor) {
            startActivity(new Intent(getActivity(), UserCreateActivity.class));
            ActivityUtils.setAnimationOnClosed(this);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCustomCreateView(View view, ViewGroup container) {
        mPresenter.loadSpecialties();
        clinicCepEdit.addTextChangedListener(Mask.insert("#####-###", clinicCepEdit));
        clinicPhoneEdit.addTextChangedListener(Mask.insert("(##)####-####", clinicPhoneEdit));
    }

    @OnClick(R.id.createButton)
    public void createListener() {
        mPresenter.createDoctor(specialtiesSelected,
                doctorCrmEdit.getText().toString(),
                clinicAddressEdit.getText().toString(),
                clinicComplementEdit.getText().toString(),
                clinicNumberEdit.getText().toString(),
                Mask.unmask(clinicCepEdit.getText().toString()),
                Mask.unmask(clinicPhoneEdit.getText().toString()),
                (User) getArguments().getSerializable(ARG_USER));
    }

    @Override
    public void showInvalidField(@IdRes int layoutId, @StringRes int stringId) {
        hideProgressBar();
        if (getView() != null) {
            TextInputLayout layout = getView().findViewById(layoutId);
            layout.setErrorEnabled(true);
            layout.setError(getString(stringId));
        }
    }

    @Override
    public void showSuccess() {
        startActivity(new Intent(getActivity(), MenuActivity.class));
        ActivityUtils.setAnimationOnClosed(this);
    }

    @Override
    public void prepareSpecialties(final List<String> specialties) {
        this.specialties = specialties;
        this.specialtiesSelectedNumber = new ArrayList<>();

        for (String bool : specialties) {
            specialtiesSelectedNumber.add(false);
        }
        specialtieEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialogHelper.multiChoiseDialog(getContext(), R.string.Specialties, DoctorCreateFragment.this.specialties,
                        DoctorCreateFragment.this.specialtiesSelectedNumber, multiListener, allListener);
            }
        });
    }
}