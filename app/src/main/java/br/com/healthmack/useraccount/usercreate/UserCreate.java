package br.com.healthmack.useraccount.usercreate;

import android.support.annotation.IdRes;
import android.support.annotation.StringRes;

import br.com.healthmack.shared.model.User;
import br.com.healthmack.utils.BasePresenter;
import br.com.healthmack.utils.BaseView;

public interface UserCreate {

    interface View extends BaseView<Presenter> {

        void showInvalidFiled(@IdRes int layout, @StringRes int string);

        void showSuccess(User user);
    }

    interface Presenter extends BasePresenter {

        void createUser(String name, String email, String cpf, String rg, String birthday, String cellPhone, String password, String rePassword, boolean isDoctor);
    }
}
