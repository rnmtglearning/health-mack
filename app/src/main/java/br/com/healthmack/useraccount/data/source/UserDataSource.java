package br.com.healthmack.useraccount.data.source;

import java.util.List;

import br.com.healthmack.useraccount.data.Clinic;
import br.com.healthmack.shared.model.User;

public interface UserDataSource {

    void ourLogin(User user, UserLoginCallback callback);

    void createUser(User user, UserCreateCallback callback);

    void createClinic(Clinic clinic, User user, DoctorCreateCallback callback);

    void saveUser(User user, UserCreateCallback callback);

    void loadSpecialties(DoctorCreateCallback callback);

    interface UserLoginCallback {
        void userValid();

        void userInvalid();
    }

    interface UserCreateCallback {

        void userCreated(User ourUser);

        void userNotCreated();

        void userSaved(User user);

        void userNotSaved();

        void invalidEmail();

        void invalidCpf();
    }

    interface DoctorCreateCallback {
        void doctorSaved();

        void doctorNotSaved();

        void specialtiesLoaded(List<String> specialties);
    }
}
