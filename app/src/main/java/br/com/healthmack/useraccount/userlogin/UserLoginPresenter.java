package br.com.healthmack.useraccount.userlogin;

import javax.inject.Inject;

import br.com.healthmack.shared.model.User;
import br.com.healthmack.useraccount.data.source.UserDataSource;
import br.com.healthmack.useraccount.data.source.UserRepository;

final class UserLoginPresenter implements UserLogin.Presenter, UserDataSource.UserLoginCallback {

    private final UserDataSource mUserLoginRepository;
    private final UserLogin.View mUserLoginView;

    @Inject
    UserLoginPresenter(UserRepository userLoginRepository, UserLogin.View userLoginView) {
        mUserLoginRepository = userLoginRepository;
        mUserLoginView = userLoginView;
    }

    @Inject
    @Override
    public void setupListeners() {
        mUserLoginView.setPresenter(this);
    }

    @Override
    public void ourLogin(String email, String password) {
        User user = new User(email, password);

        if (!user.isValidEmail())
            mUserLoginView.showInvalidEmail();
        if (!user.isValidPassword())
            mUserLoginView.showInvalidPassword();

        if (user.isValidEmail() && user.isValidPassword()) {
            mUserLoginView.showProgressBar();
            mUserLoginRepository.ourLogin(user, this);
        }
    }

    @Override
    public void userValid() {
        mUserLoginView.showLoginSuccess();
    }

    @Override
    public void userInvalid() {
        mUserLoginView.hideProgressBar();
        mUserLoginView.showInvalidUser();
    }
}
