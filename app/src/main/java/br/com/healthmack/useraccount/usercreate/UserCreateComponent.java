package br.com.healthmack.useraccount.usercreate;

import br.com.healthmack.modules.ContextModule;
import br.com.healthmack.useraccount.data.source.UserRepositoryComponent;
import br.com.healthmack.utils.FragmentScoped;
import dagger.Component;

@FragmentScoped
@Component(dependencies = UserRepositoryComponent.class, modules = {UserCreatePresenterModule.class,
        ContextModule.class})
public interface UserCreateComponent {

    void inject(UserCreateActivity userCreateActivity);
}
