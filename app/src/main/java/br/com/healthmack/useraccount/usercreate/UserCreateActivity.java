package br.com.healthmack.useraccount.usercreate;

import android.os.Bundle;

import javax.inject.Inject;

import br.com.healthmack.AbstractActivity;
import br.com.healthmack.HealthMackApplication;
import br.com.healthmack.R;
import br.com.healthmack.modules.ContextModule;
import br.com.healthmack.utils.ActivityUtils;

public class UserCreateActivity extends AbstractActivity {

    @Inject
    UserCreatePresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        UserCreateFragment userCreateFragment = ActivityUtils.addFragmentToActivity(this, UserCreateFragment.class);

        DaggerUserCreateComponent.builder()
                .contextModule(new ContextModule(getBaseContext()))
                .userCreatePresenterModule(new UserCreatePresenterModule(userCreateFragment))
                .userRepositoryComponent(((HealthMackApplication) getApplication()).getUserRepositoryComponent())
                .build()
                .inject(this);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.default_act;
    }

    @Override
    public String getStringResTitle() {
        return getString(R.string.Create_Account);
    }

    @Override
    public Integer getMenuRes() {
        return null;
    }
}