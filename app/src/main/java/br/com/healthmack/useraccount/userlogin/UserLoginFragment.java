package br.com.healthmack.useraccount.userlogin;

import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import br.com.healthmack.AbstractFragment;
import br.com.healthmack.R;
import br.com.healthmack.menu.MenuActivity;
import br.com.healthmack.useraccount.usercreate.UserCreateActivity;
import br.com.healthmack.utils.ActivityUtils;
import butterknife.BindView;

public class UserLoginFragment extends AbstractFragment<UserLogin.Presenter> implements UserLogin.View {

    @BindView(R.id.emailText)
    EditText emailText;
    @BindView(R.id.emailLayout)
    TextInputLayout emailLayout;
    @BindView(R.id.passwordText)
    EditText passwordText;
    @BindView(R.id.passwordLayout)
    TextInputLayout passwordLayout;
    @BindView(R.id.loginButton)
    Button loginButton;


    @BindView(R.id.createButton)
    Button createButton;

    private View.OnClickListener loginListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            mPresenter.ourLogin(emailText.getText().toString(), passwordText.getText().toString());
        }
    };
    private View.OnClickListener createListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            startActivity(new Intent(getActivity(), UserCreateActivity.class));
            ActivityUtils.setAnimationOnClosed(UserLoginFragment.this);
        }
    };

    private TextWatcher listener = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            emailLayout.setErrorEnabled(false);
            passwordLayout.setErrorEnabled(false);
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    };

    @Override
    public int getLayoutRes() {
        return R.layout.user_login_frag;
    }

    @Override
    public int getProgressBar() {
        return R.id.progressBar;
    }

    @Override
    public void onCustomCreateView(View view, ViewGroup container) {
        loginButton.setOnClickListener(loginListener);
        createButton.setOnClickListener(createListener);
        emailText.addTextChangedListener(listener);
        passwordText.addTextChangedListener(listener);
    }

    @Override
    public void showInvalidUser() {
        emailLayout.setErrorEnabled(true);
        emailLayout.setError(getString(R.string.error_email_password_invalid));
        passwordLayout.setErrorEnabled(true);
        passwordLayout.setError(getString(R.string.error_email_password_invalid));
    }

    @Override
    public void showLoginSuccess() {
        startActivity(new Intent(getActivity(), MenuActivity.class));
        getActivity().finish();
    }

    @Override
    public void showInvalidEmail() {
        emailLayout.setErrorEnabled(true);
        emailLayout.setError(getString(R.string.error_invalid_email));
    }

    @Override
    public void showInvalidPassword() {
        passwordLayout.setErrorEnabled(true);
        passwordLayout.setError(getString(R.string.error_invalid_password));
    }
}