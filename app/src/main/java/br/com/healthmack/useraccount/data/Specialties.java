package br.com.healthmack.useraccount.data;

/**
 * Created by Ramon Cardoso on 11/10/2017.
 */

public class Specialties {

    private String name;
    private double price;

    public Specialties(String name, double price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getFormattesPrice() {
        return "Apartir de R$ " + price;
    }
}
