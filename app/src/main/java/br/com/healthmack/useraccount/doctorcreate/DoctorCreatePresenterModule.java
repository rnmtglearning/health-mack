package br.com.healthmack.useraccount.doctorcreate;

import dagger.Module;
import dagger.Provides;

@Module
public class DoctorCreatePresenterModule {

    private final DoctorCreate.View mView;

    public DoctorCreatePresenterModule(DoctorCreate.View view) {
        mView = view;
    }

    @Provides
    DoctorCreate.View providesUserCreateView() {
        return mView;
    }
}
