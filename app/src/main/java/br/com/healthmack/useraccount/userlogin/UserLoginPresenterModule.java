package br.com.healthmack.useraccount.userlogin;

import dagger.Module;
import dagger.Provides;

@Module
public class UserLoginPresenterModule {

    private final UserLogin.View mView;

    public UserLoginPresenterModule(UserLogin.View view) {
        mView = view;
    }

    @Provides
    UserLogin.View providesUserLoginView() {
        return mView;
    }
}
