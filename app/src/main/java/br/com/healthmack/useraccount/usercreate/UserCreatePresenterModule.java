package br.com.healthmack.useraccount.usercreate;

import dagger.Module;
import dagger.Provides;

@Module
public class UserCreatePresenterModule {

    private final UserCreate.View mView;

    public UserCreatePresenterModule(UserCreate.View view) {
        mView = view;
    }

    @Provides
    UserCreate.View providesUserCreateView() {
        return mView;
    }
}
