package br.com.healthmack.useraccount.userlogin;

import br.com.healthmack.useraccount.data.source.UserRepositoryComponent;
import br.com.healthmack.utils.FragmentScoped;
import dagger.Component;

@FragmentScoped
@Component(dependencies = UserRepositoryComponent.class, modules = UserLoginPresenterModule.class)
public interface UserLoginComponent {

    void inject(UserLoginActivity userLoginActivity);
}
