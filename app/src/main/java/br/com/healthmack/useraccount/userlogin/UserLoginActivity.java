package br.com.healthmack.useraccount.userlogin;

import android.os.Bundle;

import javax.inject.Inject;

import br.com.healthmack.AbstractActivity;
import br.com.healthmack.HealthMackApplication;
import br.com.healthmack.R;
import br.com.healthmack.utils.ActivityUtils;

public class UserLoginActivity extends AbstractActivity {

    @Inject
    UserLoginPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        UserLoginFragment userLoginFragment = ActivityUtils.addFragmentToActivity(this, UserLoginFragment.class);

        DaggerUserLoginComponent.builder()
                .userLoginPresenterModule(new UserLoginPresenterModule(userLoginFragment))
                .userRepositoryComponent(((HealthMackApplication) getApplication()).getUserRepositoryComponent())
                .build()
                .inject(this);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.default_act_without_toolbar;
    }

    @Override
    public String getStringResTitle() {
        return null;
    }

    @Override
    public Integer getMenuRes() {
        return null;
    }
}