package br.com.healthmack.useraccount.usercreate;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import br.com.healthmack.AbstractFragment;
import br.com.healthmack.R;
import br.com.healthmack.menu.MenuActivity;
import br.com.healthmack.shared.model.User;
import br.com.healthmack.useraccount.doctorcreate.DoctorCreateActivity;
import br.com.healthmack.useraccount.doctorcreate.DoctorCreateFragment;
import br.com.healthmack.utils.ActivityUtils;
import br.com.healthmack.utils.Mask;
import butterknife.BindView;

public class UserCreateFragment extends AbstractFragment<UserCreate.Presenter> implements UserCreate.View {

    @BindView(R.id.nameLayout)
    TextInputLayout nameLayout;
    @BindView(R.id.nameEdit)
    EditText nameEdit;
    @BindView(R.id.emailLayout)
    TextInputLayout emailLayout;
    @BindView(R.id.emailText)
    EditText emailEdit;
    @BindView(R.id.cpfLayout)
    TextInputLayout cpfLayout;
    @BindView(R.id.cpfText)
    EditText cpfEdit;
    @BindView(R.id.rgLayout)
    TextInputLayout rgLayout;
    @BindView(R.id.rgText)
    EditText rgEdit;
    @BindView(R.id.birthdayLayout)
    TextInputLayout birthdayLayout;
    @BindView(R.id.birthdayText)
    EditText birthdayEdit;
    @BindView(R.id.cellPhoneLayout)
    TextInputLayout cellPhoneLayout;
    @BindView(R.id.cellPhoneText)
    EditText cellPhoneEdit;
    @BindView(R.id.passwordLayout)
    TextInputLayout passwordLayout;
    @BindView(R.id.passwordText)
    EditText passwordEdit;
    @BindView(R.id.rePasswordLayout)
    TextInputLayout rePasswordLayout;
    @BindView(R.id.rePasswordText)
    EditText rePasswordEdit;
    @BindView(R.id.createButton)
    Button createButton;
    private boolean isDoctor;

    private TextWatcher simpleWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            nameLayout.setErrorEnabled(false);
            emailLayout.setErrorEnabled(false);
            cpfLayout.setErrorEnabled(false);
            rgLayout.setErrorEnabled(false);
            birthdayLayout.setErrorEnabled(false);
            passwordLayout.setErrorEnabled(false);
            rePasswordLayout.setErrorEnabled(false);
            cellPhoneLayout.setErrorEnabled(false);
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    };

    @Override
    public int getLayoutRes() {
        return R.layout.user_create_frag;
    }

    @Override
    public int getProgressBar() {
        return R.id.progressBar;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_create_user, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getTitle().equals(getString(R.string.Create_doctor))) {
            isDoctor = true;
            createButton.setText(getString(R.string.Next));
            createButton.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, R.drawable.ic_navigate_next, 0);
            createButton.setCompoundDrawablePadding(10);
            item.setTitle(R.string.Create_user);
            return true;
        } else {
            isDoctor = false;
            createButton.setText(R.string.Create_Account);
            createButton.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0, 0, 0);
            createButton.setCompoundDrawablePadding(10);
            item.setTitle(R.string.Create_doctor);
            return true;
        }
    }

    @Override
    public void onCustomCreateView(View view, ViewGroup container) {
        isDoctor = false;

        cpfEdit.addTextChangedListener(Mask.insert("###.###.###-##", cpfEdit));
        rgEdit.addTextChangedListener(Mask.insert("###.###.###-#", rgEdit));
        cellPhoneEdit.addTextChangedListener(Mask.insert("(##)#####-####", cellPhoneEdit));
        birthdayEdit.addTextChangedListener(Mask.insert("##/##/####", birthdayEdit));

        nameEdit.addTextChangedListener(simpleWatcher);
        emailEdit.addTextChangedListener(simpleWatcher);
        cpfEdit.addTextChangedListener(simpleWatcher);
        rgEdit.addTextChangedListener(simpleWatcher);
        birthdayEdit.addTextChangedListener(simpleWatcher);
        cellPhoneEdit.addTextChangedListener(simpleWatcher);
        passwordEdit.addTextChangedListener(simpleWatcher);
        rePasswordEdit.addTextChangedListener(simpleWatcher);

        createButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPresenter.createUser(nameEdit.getText().toString(),
                        emailEdit.getText().toString(),
                        Mask.unmask(cpfEdit.getText().toString()),
                        Mask.unmask(rgEdit.getText().toString()),
                        Mask.unmask(birthdayEdit.getText().toString()),
                        Mask.unmask(cellPhoneEdit.getText().toString()),
                        passwordEdit.getText().toString(),
                        rePasswordEdit.getText().toString(),
                        isDoctor);
            }
        });
    }

    @Override
    public void showInvalidFiled(@IdRes int layout, @StringRes int string) {
        TextInputLayout view = getActivity().findViewById(layout);
        view.setErrorEnabled(true);
        view.setError(getString(string));
        hideProgressBar();
    }

    @Override
    public void showSuccess(User user) {
        if (isDoctor) {
            Intent intent = new Intent(getActivity(), DoctorCreateActivity.class);
            intent.putExtra(DoctorCreateFragment.ARG_USER, user);
            startActivity(intent);
        } else
            startActivity(new Intent(getActivity(), MenuActivity.class));
        getActivity().finish();
        ActivityUtils.setAnimationOnClosed(this);
    }
}