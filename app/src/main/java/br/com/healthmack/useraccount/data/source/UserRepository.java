package br.com.healthmack.useraccount.data.source;

import javax.inject.Inject;
import javax.inject.Singleton;

import br.com.healthmack.useraccount.data.Clinic;
import br.com.healthmack.shared.model.User;

@Singleton
public class UserRepository implements UserDataSource {

    private final UserDataSource mUserDataSource;

    @Inject
    public UserRepository(UserDataSource userDataSource) {
        this.mUserDataSource = userDataSource;
    }

    @Override
    public void ourLogin(User user, UserLoginCallback callback) {
        mUserDataSource.ourLogin(user, callback);
    }

    @Override
    public void createUser(User user, UserCreateCallback callback) {
        mUserDataSource.createUser(user, callback);
    }

    @Override
    public void createClinic(Clinic clinic, User user, DoctorCreateCallback callback) {
        mUserDataSource.createClinic(clinic, user, callback);
    }

    @Override
    public void saveUser(User user, UserCreateCallback callback) {
        mUserDataSource.saveUser(user, callback);
    }

    @Override
    public void loadSpecialties(DoctorCreateCallback callback) {
        mUserDataSource.loadSpecialties(callback);
    }
}
