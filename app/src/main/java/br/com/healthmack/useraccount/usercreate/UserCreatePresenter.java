package br.com.healthmack.useraccount.usercreate;

import android.content.Context;

import javax.inject.Inject;

import br.com.healthmack.R;
import br.com.healthmack.shared.model.User;
import br.com.healthmack.useraccount.data.source.UserDataSource;
import br.com.healthmack.useraccount.data.source.UserRepository;

final class UserCreatePresenter implements UserCreate.Presenter, UserDataSource.UserCreateCallback {

    private final UserDataSource mUserRepository;
    private final UserCreate.View mUserCreateView;
    private Context mContext;

    @Inject
    UserCreatePresenter(UserRepository userLoginRepository, UserCreate.View userLoginView, Context context) {
        mUserRepository = userLoginRepository;
        mUserCreateView = userLoginView;
        mContext = context;
    }

    @Inject
    @Override
    public void setupListeners() {
        mUserCreateView.setPresenter(this);
    }


    @Override
    public void createUser(String name, String email, String cpf, String rg, String birthday, String cellPhone, String password, String rePassword, boolean isDoctor) {
        User user = new User(name, email, rg, cpf, birthday, cellPhone, password, rePassword);
        user.setDoctor(isDoctor);
        if (!user.isValidEmail())
            mUserCreateView.showInvalidFiled(R.id.emailLayout, R.string.error_invalid_email);
        if (!user.isValidPassword())
            mUserCreateView.showInvalidFiled(R.id.passwordLayout, R.string.error_invalid_password);
        if (!user.isValidRePassword())
            mUserCreateView.showInvalidFiled(R.id.rePasswordLayout, R.string.error_invalid_re_password);
        if (!user.isValidRg())
            mUserCreateView.showInvalidFiled(R.id.rgLayout, R.string.error_inavlid_rg);
        if (!user.isValidName())
            mUserCreateView.showInvalidFiled(R.id.nameLayout, R.string.error_invalid_name);
        if (!user.isValidCpf())
            mUserCreateView.showInvalidFiled(R.id.cpfLayout, R.string.error_invalid_cpf);
        if (!user.isValidBirthday())
            mUserCreateView.showInvalidFiled(R.id.birthdayLayout, R.string.error_invalid_birthday);
        if (!user.isValidCellPhone())
            mUserCreateView.showInvalidFiled(R.id.cellPhoneLayout, R.string.error_invalid_cell_phone);


        if (user.isValidName() && user.isValidEmail() && user.isValidPassword() && user.isValidRePassword()
                && user.isValidRg() && user.isValidCpf() && user.isValidBirthday() && user.isValidCellPhone()) {
            mUserCreateView.showProgressBar();
            mUserRepository.createUser(user, this);
        }
    }

    @Override
    public void userCreated(User ourUser) {
        mUserRepository.saveUser(ourUser, this);
    }

    @Override
    public void userNotCreated() {
        mUserCreateView.showMessage(R.string.error_create_user);
    }

    @Override
    public void userSaved(User user) {
        mUserCreateView.showSuccess(user);
    }

    @Override
    public void userNotSaved() {
        mUserCreateView.showMessage(R.string.error_create_user);
    }

    @Override
    public void invalidEmail() {
        mUserCreateView.showInvalidFiled(R.id.emailLayout, R.string.error_invalid_new_email);
    }

    @Override
    public void invalidCpf() {
        mUserCreateView.showInvalidFiled(R.id.cpfLayout, R.string.error_invalid_new_cpf);
    }
}
