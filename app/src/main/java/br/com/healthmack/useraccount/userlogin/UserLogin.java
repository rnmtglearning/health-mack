package br.com.healthmack.useraccount.userlogin;

import br.com.healthmack.utils.BasePresenter;
import br.com.healthmack.utils.BaseView;

public interface UserLogin {

    interface View extends BaseView<Presenter> {

        void showInvalidEmail();

        void showInvalidPassword();

        void showInvalidUser();

        void showLoginSuccess();
    }

    interface Presenter extends BasePresenter {
        void ourLogin(String email, String password);
    }
}
