package br.com.healthmack.useraccount.doctorcreate;

import android.os.Bundle;

import javax.inject.Inject;

import br.com.healthmack.AbstractActivity;
import br.com.healthmack.HealthMackApplication;
import br.com.healthmack.R;
import br.com.healthmack.modules.ContextModule;
import br.com.healthmack.utils.ActivityUtils;

public class DoctorCreateActivity extends AbstractActivity {

    @Inject
    DoctorCreatePresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DoctorCreateFragment doctorCreateFragment = ActivityUtils.addFragmentToActivity(this, DoctorCreateFragment.class);
        doctorCreateFragment.setArguments(getIntent().getExtras());
        DaggerDoctorCreateComponent.builder()
                .doctorCreatePresenterModule(new DoctorCreatePresenterModule(doctorCreateFragment))
                .userRepositoryComponent(((HealthMackApplication) getApplication()).getUserRepositoryComponent())
                .build()
                .inject(this);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.default_act;
    }

    @Override
    public String getStringResTitle() {
        return getString(R.string.Create_Account);
    }

    @Override
    public Integer getMenuRes() {
        return null;
    }
}