package br.com.healthmack.useraccount.doctorcreate;

import br.com.healthmack.useraccount.data.source.UserRepositoryComponent;
import br.com.healthmack.utils.FragmentScoped;
import dagger.Component;

@FragmentScoped
@Component(dependencies = UserRepositoryComponent.class, modules = {DoctorCreatePresenterModule.class})
public interface DoctorCreateComponent {

    void inject(DoctorCreateActivity doctorCreateActivity);
}
