package br.com.healthmack.useraccount.data.source;

import javax.inject.Singleton;

import br.com.healthmack.useraccount.data.source.remote.UserAccountRemoteDataSource;
import dagger.Binds;
import dagger.Module;

@Module
public abstract class UserRepositoryModule {

    @Singleton
    @Binds
    abstract UserDataSource provideUserAccountDataSource(UserAccountRemoteDataSource userAccountRemoteDataSource);
}
