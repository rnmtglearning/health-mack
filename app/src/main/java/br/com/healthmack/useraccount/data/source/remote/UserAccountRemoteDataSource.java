package br.com.healthmack.useraccount.data.source.remote;

import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import br.com.healthmack.useraccount.data.Clinic;
import br.com.healthmack.shared.model.User;
import br.com.healthmack.useraccount.data.source.UserDataSource;
import br.com.healthmack.utils.FirebaseChilds;

@Singleton
public class UserAccountRemoteDataSource implements UserDataSource {

    private final FirebaseDatabase mDatabase;
    private final FirebaseAuth mAuth;

    @Inject
    public UserAccountRemoteDataSource(FirebaseDatabase mDatabase, FirebaseAuth auth) {
        this.mDatabase = mDatabase;
        this.mAuth = auth;
    }

    @Override
    public void ourLogin(User user, final UserLoginCallback callback) {
        mAuth.signInWithEmailAndPassword(user.getEmail(), user.getPassword())
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful())
                            callback.userValid();
                        else
                            callback.userInvalid();
                    }
                });

    }

    @Override
    public void createUser(final User user, final UserCreateCallback callback) {
        mAuth.createUserWithEmailAndPassword(user.getEmail(), user.getPassword())
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            mAuth.signInWithEmailAndPassword(user.getEmail(), user.getPassword())
                                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                        @Override
                                        public void onComplete(@NonNull Task<AuthResult> task) {
                                            if (task.isSuccessful())
                                                callback.userCreated(user);
                                            else
                                                callback.userNotCreated();
                                        }
                                    });
                        } else if (task.getException() != null && task.getException().getMessage().contains("email"))
                            callback.invalidEmail();
                        else
                            callback.userNotCreated();
                    }
                });

    }

    @Override
    public void createClinic(Clinic clinic, User user, final DoctorCreateCallback callback) {
        mDatabase.getReference()
                .child(FirebaseChilds.CHILD_USER)
                .child(mAuth.getCurrentUser().getUid())
                .child(FirebaseChilds.CHILD_CLINIC)
                .setValue(clinic.toMap()).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful())
                    callback.doctorSaved();
                else
                    callback.doctorNotSaved();
            }
        });
    }

    @Override
    public void saveUser(final User user, final UserCreateCallback callback) {

        final DatabaseReference child = mDatabase.getReference()
                .child(FirebaseChilds.CHILD_USER)
                .child(mAuth.getCurrentUser().getUid());

        child.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() == null || (user.getCrm() != null && user.getCrm().length() > 0)) {
                    child.setValue(user.toMap()).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful())
                                callback.userSaved(user);
                            else
                                callback.userNotSaved();
                        }
                    });
                } else {
                    callback.invalidCpf();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                callback.userNotSaved();
            }
        });

        if (user.getSpecialties() != null && !user.getSpecialties().isEmpty()) {
            for (String string : user.getSpecialties()) {
                mDatabase.getReference()
                        .child(FirebaseChilds.CHILD_SPECIALTIES_DOCTORS)
                        .child(string)
                        .child(mAuth.getCurrentUser().getUid())
                        .setValue(user.getName());
            }
        }
    }

    @Override
    public void loadSpecialties(final DoctorCreateCallback callback) {
        mDatabase.getReference()
                .child(FirebaseChilds.CHILD_SPECIALTIES)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        List<String> specialties = new ArrayList<>();
                        for (DataSnapshot snap : dataSnapshot.getChildren()) {
                            specialties.add(snap.getKey());
                        }

                        callback.specialtiesLoaded(specialties);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });
    }
}
