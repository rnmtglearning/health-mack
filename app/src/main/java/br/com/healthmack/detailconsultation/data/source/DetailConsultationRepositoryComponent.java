package br.com.healthmack.detailconsultation.data.source;

import javax.inject.Singleton;

import br.com.healthmack.modules.FirebaseAuthModule;
import br.com.healthmack.modules.FirebaseDatabaseModule;
import br.com.healthmack.modules.PreferencesModule;
import br.com.healthmack.shared.preferences.HealthMackPreferences;
import dagger.Component;

@Singleton
@Component(modules = {DetailConsultationRepositoryModule.class,
        FirebaseDatabaseModule.class,
        FirebaseAuthModule.class,
        PreferencesModule.class})
public interface DetailConsultationRepositoryComponent {

    DetailConsultationRepository getRepository();
}
