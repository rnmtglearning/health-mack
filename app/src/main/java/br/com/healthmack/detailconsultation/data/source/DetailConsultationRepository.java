package br.com.healthmack.detailconsultation.data.source;

import javax.inject.Inject;
import javax.inject.Singleton;

import static dagger.internal.Preconditions.checkNotNull;

@Singleton
public class DetailConsultationRepository implements DetailConsultationDataSource {

    private final DetailConsultationDataSource dataSource;

    @Inject
    public DetailConsultationRepository(DetailConsultationDataSource detailConsultationDataSource) {
        this.dataSource = detailConsultationDataSource;
    }

    @Override
    public void loadDoctor(String key, ConfirmCallback callback) {
        checkNotNull(key);
        dataSource.loadDoctor(key, callback);
    }

    @Override
    public void loadSpecialty(String specialty, ConfirmCallback callback) {
        checkNotNull(specialty);
        dataSource.loadSpecialty(specialty, callback);
    }

    @Override
    public void confirm(String doctorKey, String specialty, String date, String day, String hour, String price, long dateMilis, ConfirmCallback callback) {
        dataSource.confirm(doctorKey, specialty, date, day, hour, price, dateMilis, callback);
    }

    @Override
    public void delete(String date, String key, String doctorKey, ConfirmCallback callback) {
        dataSource.delete(date, key, doctorKey, callback);
    }
}
