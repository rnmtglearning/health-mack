package br.com.healthmack.detailconsultation;

import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.CardView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.github.clans.fab.FloatingActionButton;

import br.com.healthmack.AbstractFragment;
import br.com.healthmack.R;
import br.com.healthmack.attestation.Attestation;
import br.com.healthmack.attestation.AttestationActivity;
import br.com.healthmack.detailconsultation.data.Specialty;
import br.com.healthmack.medicalprescription.MedicalPrescriptionActivity;
import br.com.healthmack.menu.MenuActivity;
import br.com.healthmack.scheduleconsultation.listdoctors.ListDoctorsActivity;
import br.com.healthmack.scheduleconsultation.listschedule.ListScheduleActivity;
import br.com.healthmack.shared.model.User;
import br.com.healthmack.shared.preferences.HealthMackPreferences;
import br.com.healthmack.shared.preferences.UserType;
import br.com.healthmack.useraccount.data.Clinic;
import br.com.healthmack.utils.ActivityUtils;
import br.com.healthmack.utils.FirebaseChilds;
import br.com.healthmack.utils.SnackbarHelper;
import butterknife.BindView;

public class DetailFragment extends AbstractFragment<DetailConsultation.Presenter>
        implements DetailConsultation.View {

    @BindView(R.id.personTitle)
    TextView personTitle;
    @BindView(R.id.confirmDoctorName)
    TextView personName;
    @BindView(R.id.confirmDoctorCrm)
    TextView personSubtitle;
    @BindView(R.id.addressDoctorStreet)
    TextView address;
    @BindView(R.id.phoneClinic)
    TextView phone;
    @BindView(R.id.confirmSpecialty)
    TextView specialtyText;
    @BindView(R.id.confirmDate)
    TextView dateText;
    @BindView(R.id.confirmPrice)
    TextView price;
    @BindView(R.id.medicalPrescription)
    Button medicalPrescription;
    @BindView(R.id.attestation)
    Button attestation;
    @BindView(R.id.cardDoctor)
    CardView cardPerson;
    @BindView(R.id.cardAdrress)
    CardView cardAdrress;

    private String hour;
    private String day;
    private String date;
    private String otherKey;
    private String specialty;
    private String specialtyPrice;
    private FloatingActionButton fabButton;
    private boolean isConfirm;
    private HealthMackPreferences mPreferences;
    private long dateMillis;

    @Override
    public int getLayoutRes() {
        return R.layout.detail_consultation_frag;
    }

    @Override
    public int getProgressBar() {
        return R.id.progressBar;
    }

    @Override
    public void onCustomCreateView(View view, ViewGroup container) {
        otherKey = getArguments().getString(ListScheduleActivity.ARG_OTHER_KEY);
        specialty = getArguments().getString(ListDoctorsActivity.ARG_SPECIALTY);
        isConfirm = getArguments().getBoolean(DetailActivity.ARG_IS_CONFIRM);
        hour = getArguments().getString(DetailActivity.ARG_SCHEDULE_HOUR);
        day = getArguments().getString(DetailActivity.ARG_SCHEDULE_DAY);
        date = getArguments().getString(DetailActivity.ARG_SCHEDULE_DATE);

        mPresenter.loadSpecialty(specialty);
        mPresenter.loadDoctor(otherKey);

        fabButton = ((DetailActivity) getActivity()).fabButton;
        if (fabButton != null && mPreferences.getUserType() == UserType.USER) {
            fabButton.setVisibility(View.VISIBLE);
            if (isConfirm) {
                fabButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_done));
                fabButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mPresenter.confirm(otherKey, specialty, date, day, hour, specialtyPrice, getArguments().getLong(DetailActivity.ARG_SCHEDULE_DATE_MILIS));
                    }
                });
            } else {
                fabButton.setColorNormal(getResources().getColor(android.R.color.holo_red_dark));
                fabButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_delete_forever));
                fabButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mPresenter.delete(date, getArguments().getString(DetailActivity.ARG_CONSULTATION_KEY), otherKey);
                    }
                });
            }
        }

        medicalPrescription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getContext(), MedicalPrescriptionActivity.class);
                intent.putExtra(ListDoctorsActivity.ARG_SPECIALTY, specialty);
                intent.putExtra(ListScheduleActivity.ARG_OTHER_KEY, otherKey);
                intent.putExtra(DetailActivity.ARG_SCHEDULE_HOUR, hour);
                intent.putExtra(DetailActivity.ARG_SCHEDULE_DAY, day);
                intent.putExtra(DetailActivity.ARG_SCHEDULE_DATE, date);
                intent.putExtra(DetailActivity.ARG_CONSULTATION_KEY, getArguments().getString(DetailActivity.ARG_CONSULTATION_KEY));

                getActivity().startActivity(intent);
                ActivityUtils.setAnimationOnClosed(getActivity());
            }
        });

        attestation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), AttestationActivity.class);
                intent.putExtra(ListDoctorsActivity.ARG_SPECIALTY, specialty);
                intent.putExtra(ListScheduleActivity.ARG_OTHER_KEY, otherKey);
                intent.putExtra(DetailActivity.ARG_SCHEDULE_DATE, date);
                intent.putExtra(DetailActivity.ARG_SCHEDULE_HOUR, hour);
                intent.putExtra(DetailActivity.ARG_CONSULTATION_KEY, getArguments().getString(DetailActivity.ARG_CONSULTATION_KEY));

                getActivity().startActivity(intent);
                ActivityUtils.setAnimationOnClosed(getActivity());
            }
        });

        if (isConfirm) {
            medicalPrescription.setVisibility(View.GONE);
            attestation.setVisibility(View.GONE);
        }

        if (mPreferences.getUserType() == UserType.DOCTOR)
            cardAdrress.setVisibility(View.GONE);
    }

    @Override
    public void setPreferences(HealthMackPreferences preferences) {
        mPreferences = preferences;
    }

    @Override
    public void showUser(User user) {
        if (mPreferences.getUserType() != UserType.DOCTOR) {
            personTitle.setText(R.string.Doctor);
            personName.setText(getString(R.string.format_name, user.getName()));
            personSubtitle.setText(getString(R.string.format_CRM, user.getCrm()));
        } else {
            personTitle.setText(R.string.Pacient);
            personName.setText(getString(R.string.format_name, user.getName()));
            personSubtitle.setText(getString(R.string.format_Email, user.getEmail()));
        }
    }

    @Override
    public void showClinic(Clinic clinic) {
        address.setText(getString(R.string.format_address, clinic.getCompleteAddress()));
        phone.setText(clinic.getFormattedPhone(getContext()));
    }

    @Override
    public void showSpecialty(Specialty specialty) {
        specialtyPrice = specialty.getPrice();
        specialtyText.setText(specialty.getTitle());
        this.price.setText(specialtyPrice);
        dateText.setText(getString(R.string.format_date_consultation, date,
                FirebaseChilds.translateDay(day.toLowerCase(), getContext()), hour.replace(":", "h")));
    }

    @Override
    public void showConfirmed() {
        SnackbarHelper.disableFields(getView());
        fabButton.setVisibility(View.GONE);
        SnackbarHelper.show(getActivity(), R.id.mainLayout, R.string.consultation_created, Snackbar.LENGTH_INDEFINITE, android.R.string.ok, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DetailFragment.this.startActivity(new Intent(DetailFragment.this.getActivity(), MenuActivity.class));
                DetailFragment.this.getActivity().finish();
                ActivityUtils.setAnimationOnClosed(DetailFragment.this);
            }
        });
    }

    @Override
    public void showDeleted() {
        SnackbarHelper.disableFields(getView());
        fabButton.setVisibility(View.GONE);
        SnackbarHelper.show(getActivity(), R.id.mainLayout, R.string.consultation_removed, Snackbar.LENGTH_INDEFINITE, android.R.string.ok, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DetailFragment.this.getActivity().finish();
                ActivityUtils.setAnimationOnClosed(DetailFragment.this);
            }
        });
    }
}