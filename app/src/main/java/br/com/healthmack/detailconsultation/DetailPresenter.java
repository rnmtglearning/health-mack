package br.com.healthmack.detailconsultation;

import android.content.Context;

import javax.inject.Inject;

import br.com.healthmack.R;
import br.com.healthmack.detailconsultation.data.Specialty;
import br.com.healthmack.detailconsultation.data.source.DetailConsultationDataSource;
import br.com.healthmack.detailconsultation.data.source.DetailConsultationRepository;
import br.com.healthmack.shared.model.User;
import br.com.healthmack.shared.preferences.HealthMackPreferences;
import br.com.healthmack.useraccount.data.Clinic;

final class DetailPresenter implements DetailConsultation.Presenter, DetailConsultationDataSource.ConfirmCallback {

    private final DetailConsultationRepository mRepository;
    private final DetailConsultation.View mView;
    private Context mContext;

    @Inject
    DetailPresenter(DetailConsultationRepository repository, DetailConsultation.View view, Context context) {
        mRepository = repository;
        mView = view;
        mContext = context;
    }

    @Inject
    @Override
    public void setupListeners() {
        mView.setPresenter(this);
    }

    @Inject
    @Override
    public void setupPreferences(HealthMackPreferences mackPreferences) {
        mView.setPreferences(mackPreferences);
    }

    @Override
    public void loadDoctor(String key) {
        mView.showProgressBar();
        mRepository.loadDoctor(key, this);
    }

    @Override
    public void onUserLoaded(User user, Clinic clinic) {
        mView.hideProgressBar();
        mView.showUser(user);
        if (clinic != null && clinic.getCompleteAddress() != null)
            mView.showClinic(clinic);
    }

    @Override
    public void loadSpecialty(String specialty) {
        mView.showProgressBar();
        mRepository.loadSpecialty(specialty, this);
    }

    @Override
    public void confirm(String doctorKey, String specialty, String date, String day, String hour, String price, long dateMilis) {
        mView.showProgressBar();
        mRepository.confirm(doctorKey, specialty, date, day, hour, price, dateMilis, this);

    }

    @Override
    public void delete(String date, String key, String doctorKey) {
        mView.showProgressBar();
        mRepository.delete(date, key, doctorKey, this);
    }

    @Override
    public void onSpecialtyLoaded(Specialty specialty) {
        mView.hideProgressBar();
        mView.showSpecialty(specialty);
    }

    @Override
    public void onConfirmed() {
        mView.hideProgressBar();
        mView.showConfirmed();
    }

    @Override
    public void onDeleted() {
        mView.hideProgressBar();
        mView.showDeleted();
    }

    @Override
    public void onError() {
        mView.showMessage(R.string.error_load_some_informations);
    }
}
