package br.com.healthmack.detailconsultation;

import br.com.healthmack.detailconsultation.data.Specialty;
import br.com.healthmack.shared.model.User;
import br.com.healthmack.shared.preferences.HealthMackPreferences;
import br.com.healthmack.useraccount.data.Clinic;
import br.com.healthmack.utils.BasePresenter;
import br.com.healthmack.utils.BaseView;

public interface DetailConsultation {

    interface View extends BaseView<Presenter> {

        void setPreferences(HealthMackPreferences preferences);

        void showUser(User user);

        void showClinic(Clinic clinic);

        void showSpecialty(Specialty specialty);

        void showConfirmed();

        void showDeleted();
    }

    interface Presenter extends BasePresenter {

        void setupPreferences(HealthMackPreferences mackPreferences);

        void loadDoctor(String key);

        void loadSpecialty(String specialty);

        void confirm(String doctorKey, String specialty, String date, String day, String hour, String price, long dateMilis);

        void delete(String date, String key, String doctorKey);
    }
}
