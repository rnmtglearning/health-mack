package br.com.healthmack.detailconsultation.data.source;

import br.com.healthmack.detailconsultation.data.Specialty;
import br.com.healthmack.shared.model.User;
import br.com.healthmack.useraccount.data.Clinic;

public interface DetailConsultationDataSource {

    void loadDoctor(String key, ConfirmCallback callback);

    void loadSpecialty(String specialty, ConfirmCallback callback);

    void confirm(String doctorKey, String specialty, String date, String day, String hour, String price, long dateMilis, ConfirmCallback callback);

    void delete(String date, String key, String doctorKey, ConfirmCallback callback);

    interface ConfirmCallback {

        void onUserLoaded(User user, Clinic clinic);

        void onSpecialtyLoaded(Specialty specialty);

        void onConfirmed();

        void onDeleted();

        void onError();
    }
}
