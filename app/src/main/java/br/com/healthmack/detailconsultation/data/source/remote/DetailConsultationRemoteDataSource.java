package br.com.healthmack.detailconsultation.data.source.remote;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import javax.inject.Inject;
import javax.inject.Singleton;

import br.com.healthmack.detailconsultation.data.Specialty;
import br.com.healthmack.detailconsultation.data.source.DetailConsultationDataSource;
import br.com.healthmack.shared.model.Consultation;
import br.com.healthmack.shared.model.User;
import br.com.healthmack.shared.preferences.HealthMackPreferences;
import br.com.healthmack.useraccount.data.Clinic;
import br.com.healthmack.utils.FirebaseChilds;

@Singleton
public class DetailConsultationRemoteDataSource implements DetailConsultationDataSource {

    private final FirebaseDatabase mDatabase;
    private final FirebaseAuth mAuth;
    private final HealthMackPreferences mPreferences;

    @Inject
    public DetailConsultationRemoteDataSource(FirebaseDatabase mDatabase, FirebaseAuth firebaseAuth, HealthMackPreferences preferences) {
        this.mDatabase = mDatabase;
        this.mAuth = firebaseAuth;
        this.mPreferences = preferences;
    }

    @Override
    public void loadDoctor(String key, final ConfirmCallback callback) {
        mDatabase.getReference()
                .child(FirebaseChilds.CHILD_USER)
                .child(key)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        User user = dataSnapshot.getValue(User.class);
                        Clinic clinic = dataSnapshot.child(FirebaseChilds.CHILD_CLINIC).getValue(Clinic.class);
                        callback.onUserLoaded(user, clinic);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        callback.onError();
                    }
                });
    }

    @Override
    public void loadSpecialty(final String specialty, final ConfirmCallback callback) {
        mDatabase.getReference()
                .child(FirebaseChilds.CHILD_SPECIALTIES)
                .child(specialty)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Specialty obj = new Specialty();
                        obj.setTitle(dataSnapshot.getKey());
                        obj.setPrice(dataSnapshot.getValue() + "");

                        callback.onSpecialtyLoaded(obj);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        callback.onError();
                    }
                });
    }

    @Override
    public void confirm(String doctorKey, String specialty, String date, String day, String hour, String price, long dateMilis, ConfirmCallback callback) {
        String userKey = mAuth.getCurrentUser().getUid();

        Consultation consultation = new Consultation(doctorKey, userKey, specialty, date, dateMilis, day, hour, price);

        DatabaseReference reference = mDatabase.getReference().child(FirebaseChilds.CHILD_CONSULTATIONS);

        DatabaseReference referenceUser = reference.child(userKey).child(consultation.getDate()).push();
        String consultationKey = referenceUser.getKey();
        referenceUser.setValue(consultation.toMap());

        consultation.setUserKey(doctorKey);
        consultation.setOtherKey(userKey);
        reference.child(doctorKey).child(consultation.getDate()).child(consultationKey).setValue(consultation.toMap());

        callback.onConfirmed();
    }

    @Override
    public void delete(String date, String key, String doctorKey, ConfirmCallback callback) {
        String userKey = mAuth.getCurrentUser().getUid();
        DatabaseReference reference = mDatabase.getReference().child(FirebaseChilds.CHILD_CONSULTATIONS);

        reference.child(userKey).child(date).child(key).removeValue();
        reference.child(doctorKey).child(date).child(key).removeValue();

        callback.onDeleted();
    }
}
