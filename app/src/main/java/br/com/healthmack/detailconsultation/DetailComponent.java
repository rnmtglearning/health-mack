package br.com.healthmack.detailconsultation;

import br.com.healthmack.detailconsultation.data.source.DetailConsultationRepositoryComponent;
import br.com.healthmack.modules.ContextModule;
import br.com.healthmack.modules.PreferencesModule;
import br.com.healthmack.utils.FragmentScoped;
import dagger.Component;

@FragmentScoped
@Component(dependencies = DetailConsultationRepositoryComponent.class, modules = {DetailPresenterModule.class,
        ContextModule.class,
        PreferencesModule.class})
public interface DetailComponent {

    void inject(DetailActivity detailActivity);
}
