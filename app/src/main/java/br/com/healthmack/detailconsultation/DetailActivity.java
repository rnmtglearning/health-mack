package br.com.healthmack.detailconsultation;

import android.os.Bundle;

import javax.inject.Inject;

import br.com.healthmack.AbstractActivity;
import br.com.healthmack.HealthMackApplication;
import br.com.healthmack.R;
import br.com.healthmack.modules.ContextModule;
import br.com.healthmack.modules.PreferencesModule;
import br.com.healthmack.utils.ActivityUtils;

public class DetailActivity extends AbstractActivity {

    public static final String ARG_SCHEDULE_DAY = "schedule-day";
    public static final String ARG_SCHEDULE_HOUR = "schedule-hour";
    public static final String ARG_SCHEDULE_DATE = "schedule-date";
    public static final String ARG_SCHEDULE_DATE_MILIS = "schedule-date-milis";
    public static final String ARG_IS_CONFIRM = "schedule-is-confirmation";
    public static final String ARG_CONSULTATION_KEY = "consultation-key";

    @Inject
    DetailPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DetailFragment detailFragment = ActivityUtils.addFragmentToActivity(this, DetailFragment.class);

        detailFragment.setArguments(getIntent().getExtras());
        DaggerDetailComponent.builder()
                .contextModule(new ContextModule(getApplicationContext()))
                .detailPresenterModule(new DetailPresenterModule(detailFragment))
                .detailConsultationRepositoryComponent(((HealthMackApplication) getApplication()).getDetailConsultationRepositoryComponent())
                .preferencesModule(new PreferencesModule(this))
                .build()
                .inject(this);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.default_act;
    }

    @Override
    public String getStringResTitle() {
        return getString(R.string.Make_Schedule);
    }

    @Override
    public Integer getMenuRes() {
        return null;
    }
}