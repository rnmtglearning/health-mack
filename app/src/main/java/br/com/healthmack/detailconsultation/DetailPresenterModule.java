package br.com.healthmack.detailconsultation;

import dagger.Module;
import dagger.Provides;

@Module
public class DetailPresenterModule {

    private final DetailConsultation.View mView;

    public DetailPresenterModule(DetailConsultation.View view) {
        mView = view;
    }

    @Provides
    DetailConsultation.View providesView() {
        return mView;
    }
}
