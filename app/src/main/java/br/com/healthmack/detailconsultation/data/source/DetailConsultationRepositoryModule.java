package br.com.healthmack.detailconsultation.data.source;

import javax.inject.Singleton;

import br.com.healthmack.detailconsultation.data.source.remote.DetailConsultationRemoteDataSource;
import dagger.Binds;
import dagger.Module;

@Module
public abstract class DetailConsultationRepositoryModule {

    @Singleton
    @Binds
    abstract DetailConsultationDataSource provideDataSource(DetailConsultationRemoteDataSource detailConsultationRemoteDataSource);
}
