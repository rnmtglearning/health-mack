package br.com.healthmack.addsignature.source;

import android.net.Uri;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class AddSignatureRepository implements AddSignatureDataSource {

    private final AddSignatureDataSource mAddSignatureDataSource;

    @Inject
    public AddSignatureRepository(AddSignatureDataSource addSignatureDataSource) {
        this.mAddSignatureDataSource = addSignatureDataSource;
    }

    @Override
    public void savePhoto(Uri image, AddCallback callback) {
        mAddSignatureDataSource.savePhoto(image, callback);
    }

    @Override
    public void loadPhoto(AddCallback callback) {
        mAddSignatureDataSource.loadPhoto(callback);
    }
}
