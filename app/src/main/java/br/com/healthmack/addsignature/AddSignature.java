package br.com.healthmack.addsignature;

import android.net.Uri;

import br.com.healthmack.utils.BasePresenter;
import br.com.healthmack.utils.BaseView;

public interface AddSignature {

    interface View extends BaseView<Presenter> {
        void showSignatureSaved(boolean saved);

        void showImage(String url);
    }

    interface Presenter extends BasePresenter {
        void savePhoto(Uri image);

        void loadImage();
    }
}
