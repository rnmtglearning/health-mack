package br.com.healthmack.addsignature.source;

import javax.inject.Singleton;

import br.com.healthmack.addsignature.source.remote.AddSignaturetRemoteDataSource;
import dagger.Binds;
import dagger.Module;

@Module
public abstract class AddSignatureRepositoryModule {

    @Singleton
    @Binds
    abstract AddSignatureDataSource provideDataSource(AddSignaturetRemoteDataSource remoteDataSource);
}
