package br.com.healthmack.addsignature.source;

import javax.inject.Singleton;

import br.com.healthmack.modules.FirebaseAuthModule;
import br.com.healthmack.modules.FirebaseDatabaseModule;
import br.com.healthmack.modules.FirebaseStorageModule;
import dagger.Component;

@Singleton
@Component(modules = {AddSignatureRepositoryModule.class, FirebaseDatabaseModule.class,
        FirebaseAuthModule.class, FirebaseStorageModule.class})
public interface AddSignatureRepositoryComponent {

    AddSignatureRepository getRepository();
}
