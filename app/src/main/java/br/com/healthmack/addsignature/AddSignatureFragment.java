package br.com.healthmack.addsignature;

import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import br.com.healthmack.AbstractFragment;
import br.com.healthmack.R;
import br.com.healthmack.utils.ActivityUtils;
import br.com.healthmack.utils.ToastHelper;
import butterknife.BindView;
import butterknife.OnClick;

public class AddSignatureFragment extends AbstractFragment<AddSignature.Presenter> implements AddSignature.View {

    @BindView(R.id.addSignatureImageButton)
    ImageButton addImage;
    @BindView(R.id.addSignatureButton)
    Button addButton;
    private Uri imageUri;

    @Override
    public int getLayoutRes() {
        return R.layout.add_signature_frag;
    }

    @Override
    public int getProgressBar() {
        return R.id.progressBar;
    }

    @Override
    public void onCustomCreateView(View view, ViewGroup container) {
        mPresenter.loadImage();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100) {
            addImage.setImageURI(data.getData());
            imageUri = data.getData();
        }
    }

    @OnClick(R.id.addSignatureButton)
    public void addListener() {
        if (imageUri == null) {
            Intent intent = new Intent(Intent.ACTION_PICK);
            intent.setType("image/*");
            startActivityForResult(intent, 100);
        } else {
            mPresenter.savePhoto(imageUri);
        }
    }

    @Override
    public void showImage(String url) {
        if (!TextUtils.isEmpty(url)) {
            Picasso.with(getContext())
                    .load(url)
                    .placeholder(R.drawable.estetoscopio)
                    .error(R.drawable.estetoscopio)
                    .priority(Picasso.Priority.HIGH)
                    .into(addImage);
        }
    }

    @Override
    public void showSignatureSaved(boolean saved) {
        if (saved)
            ToastHelper.show(this, "Assinatura salva com sucesso.", Toast.LENGTH_LONG);
        else
            ToastHelper.show(this, "Falha ao salvar assinatura.", Toast.LENGTH_LONG);

        getActivity().finish();
        ActivityUtils.setAnimationOnClosed(this);
    }
}