package br.com.healthmack.addsignature;

import br.com.healthmack.addsignature.source.AddSignatureRepositoryComponent;
import br.com.healthmack.modules.ContextModule;
import br.com.healthmack.utils.FragmentScoped;
import dagger.Component;

@FragmentScoped
@Component(dependencies = AddSignatureRepositoryComponent.class, modules = {AddSignaturePresenterModule.class,
        ContextModule.class})
public interface AddSignatureComponent {

    void inject(AddSignatureActivity addSignatureActivity);
}
