package br.com.healthmack.addsignature;

import android.content.Context;
import android.net.Uri;

import javax.inject.Inject;

import br.com.healthmack.addsignature.source.AddSignatureDataSource;
import br.com.healthmack.addsignature.source.AddSignatureRepository;

final class AddSignaturePresenter implements AddSignature.Presenter, AddSignatureDataSource.AddCallback {

    private final AddSignatureRepository mRepository;
    private final AddSignature.View mView;
    private Context mContext;

    @Inject
    AddSignaturePresenter(AddSignatureRepository repository, AddSignature.View view, Context context) {
        mRepository = repository;
        mView = view;
        mContext = context;
    }

    @Inject
    @Override
    public void setupListeners() {
        mView.setPresenter(this);
    }

    @Override
    public void savePhoto(Uri image) {
        mView.showProgressBar();
        mRepository.savePhoto(image, this);
    }

    @Override
    public void loadImage() {
        mView.showProgressBar();
        mRepository.loadPhoto(this);
    }

    @Override
    public void imageLoaded(String url) {
        mView.hideProgressBar();
        mView.showImage(url);
    }

    @Override
    public void signatureSaved(boolean saved) {
        mView.hideProgressBar();
        mView.showSignatureSaved(saved);
    }
}
