package br.com.healthmack.addsignature.source;

import android.net.Uri;

public interface AddSignatureDataSource {

    void savePhoto(Uri image, AddCallback callback);

    void loadPhoto(AddCallback callback);

    interface AddCallback {

        void signatureSaved(boolean saved);

        void imageLoaded(String url);
    }
}
