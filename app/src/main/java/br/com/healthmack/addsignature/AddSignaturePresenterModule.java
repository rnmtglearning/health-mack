package br.com.healthmack.addsignature;

import dagger.Module;
import dagger.Provides;

@Module
public class AddSignaturePresenterModule {

    private final AddSignature.View mView;

    public AddSignaturePresenterModule(AddSignature.View view) {
        mView = view;
    }

    @Provides
    AddSignature.View providesView() {
        return mView;
    }
}
