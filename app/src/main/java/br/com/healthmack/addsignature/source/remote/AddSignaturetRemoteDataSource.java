package br.com.healthmack.addsignature.source.remote;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.UploadTask;

import javax.inject.Inject;
import javax.inject.Singleton;

import br.com.healthmack.addsignature.source.AddSignatureDataSource;
import br.com.healthmack.utils.FirebaseChilds;

@Singleton
public class AddSignaturetRemoteDataSource implements AddSignatureDataSource {

    private final FirebaseDatabase mDatabase;
    private final FirebaseStorage mStorage;
    private final FirebaseAuth mAuth;

    @Inject
    public AddSignaturetRemoteDataSource(FirebaseDatabase mDatabase, FirebaseStorage storeage, FirebaseAuth auth) {
        this.mDatabase = mDatabase;
        this.mStorage = storeage;
        this.mAuth = auth;
    }

    @Override
    public void savePhoto(Uri image, final AddCallback callback) {
        UploadTask uploadTask = mStorage.getReference()
                .child(mAuth.getCurrentUser().getUid())
                .child(FirebaseChilds.CHILD_SIGNATURE)
                .putFile(image);

        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                callback.signatureSaved(false);
            }
        });

        uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                callback.signatureSaved(true);
                mDatabase.getReference(FirebaseChilds.CHILD_USER)
                        .child(mAuth.getCurrentUser().getUid())
                        .child(FirebaseChilds.CHILD_SIGNATURE)
                        .setValue(taskSnapshot.getDownloadUrl().toString());
            }
        });

        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                callback.signatureSaved(false);
            }
        });
    }

    @Override
    public void loadPhoto(final AddCallback callback) {
        mDatabase.getReference(FirebaseChilds.CHILD_USER)
                .child(mAuth.getCurrentUser().getUid())
                .child(FirebaseChilds.CHILD_SIGNATURE)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        callback.imageLoaded(dataSnapshot.getValue(String.class));
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        callback.imageLoaded(null);
                    }
                });
    }
}
