package br.com.healthmack.addsignature;

import android.os.Bundle;

import javax.inject.Inject;

import br.com.healthmack.AbstractActivity;
import br.com.healthmack.HealthMackApplication;
import br.com.healthmack.R;
import br.com.healthmack.modules.ContextModule;
import br.com.healthmack.utils.ActivityUtils;

public class AddSignatureActivity extends AbstractActivity {

    @Inject
    AddSignaturePresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AddSignatureFragment addSignatureFragment = ActivityUtils.addFragmentToActivity(this, AddSignatureFragment.class);

        DaggerAddSignatureComponent.builder()
                .addSignaturePresenterModule(new AddSignaturePresenterModule(addSignatureFragment))
                .addSignatureRepositoryComponent(((HealthMackApplication) getApplication()).getAddSignatureRepositoryComponent())
                .contextModule(new ContextModule(getApplicationContext()))
                .build()
                .inject(this);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.default_act;
    }

    @Override
    public String getStringResTitle() {
        return getString(R.string.Add_signature);
    }

    @Override
    public Integer getMenuRes() {
        return null;
    }
}