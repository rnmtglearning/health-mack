package br.com.healthmack.attestation;

import android.content.Context;

import com.google.firebase.auth.FirebaseAuth;

import javax.inject.Inject;

import br.com.healthmack.R;
import br.com.healthmack.attestation.source.AttestationDataSource;
import br.com.healthmack.attestation.source.AttestationRepository;
import br.com.healthmack.shared.model.User;
import br.com.healthmack.shared.preferences.HealthMackPreferences;
import br.com.healthmack.shared.preferences.UserType;

final class AttestationPresenter implements Attestation.Presenter, AttestationDataSource.AttestationCallback {

    private final AttestationRepository mRepository;
    private final Attestation.View mView;
    private final FirebaseAuth mAuth;
    private Context mContext;

    @Inject
    AttestationPresenter(AttestationRepository repository, Attestation.View view,
                         Context context, FirebaseAuth auth) {
        mRepository = repository;
        mView = view;
        mContext = context;
        mAuth = auth;
    }

    @Inject
    @Override
    public void setupListeners() {
        mView.setPresenter(this);
    }

    @Inject
    @Override
    public void setupPreferences(HealthMackPreferences preferences) {
        mView.setPreferences(preferences);
    }

    @Override
    public void loadUsers(String otherKey) {
        mView.showProgressBar();
        String userKey = mAuth.getCurrentUser().getUid();
        mRepository.loadUser(otherKey, this);
        mRepository.loadUser(userKey, this);
    }

    @Override
    public void loadSignature(String key) {
        mView.showProgressBar();
        mRepository.loadSignature(key, this);
    }

    @Override
    public void loadAttestation(String date, String consultationKey) {
        mView.showProgressBar();
        mRepository.loadAttestation(mAuth.getCurrentUser().getUid(), date, consultationKey, this);
    }

    @Override
    public void saveAttestation(String days, String otherKey, String date, String consultationKey) {
        mView.showProgressBar();
        if (days == null || days.equals("0") || days.isEmpty()) {
            mView.hideProgressBar();
            mView.showInvalidField(R.id.daysLayout, R.string.error_invalid_number_days_attestation);
        } else {
            mRepository.saveAttestation(days, otherKey, date, consultationKey, this);
        }
    }

    @Override
    public void onUserLoaded(User user) {
        mView.hideProgressBar();
        if (user != null) {
            if (user.isDoctor())
                mView.showDoctor(user);
            else
                mView.showUser(user);
        }
    }

    @Override
    public void onAttestationLoaded(String days) {
        mView.hideProgressBar();
        mView.showAttestation(days);
    }

    @Override
    public void onAttestationSaved() {
        mView.hideProgressBar();
        mView.showAttestationSaved();
    }

    @Override
    public void imageLoaded(String url) {
        mView.hideProgressBar();
        mView.showImage(url);
    }
}
