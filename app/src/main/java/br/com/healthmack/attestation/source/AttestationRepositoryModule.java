package br.com.healthmack.attestation.source;

import javax.inject.Singleton;

import br.com.healthmack.attestation.source.remote.AttestationRemoteDataSource;
import dagger.Binds;
import dagger.Module;

@Module
public abstract class AttestationRepositoryModule {

    @Singleton
    @Binds
    abstract AttestationDataSource provideDataSource(AttestationRemoteDataSource remoteDataSource);
}
