package br.com.healthmack.attestation;

import br.com.healthmack.attestation.source.AttestationRepositoryComponent;
import br.com.healthmack.modules.ContextModule;
import br.com.healthmack.modules.FirebaseAuthModule;
import br.com.healthmack.modules.PreferencesModule;
import br.com.healthmack.shared.preferences.HealthMackPreferences;
import br.com.healthmack.utils.FragmentScoped;
import dagger.Component;

@FragmentScoped
@Component(dependencies = AttestationRepositoryComponent.class, modules = {AttestationPresenterModule.class,
        ContextModule.class,
        FirebaseAuthModule.class,
        PreferencesModule.class})
public interface AttestationComponent {

    void inject(AttestationActivity attestationActivity);
}
