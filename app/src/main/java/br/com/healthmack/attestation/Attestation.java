package br.com.healthmack.attestation;

import android.support.annotation.IdRes;
import android.support.annotation.StringRes;

import br.com.healthmack.shared.model.User;
import br.com.healthmack.shared.preferences.HealthMackPreferences;
import br.com.healthmack.utils.BasePresenter;
import br.com.healthmack.utils.BaseView;

public interface Attestation {

    interface View extends BaseView<Presenter> {
        void showUser(User user);

        void showDoctor(User user);

        void showInvalidField(@IdRes int layout, @StringRes int message);

        void showAttestationSaved();

        void showAttestation(String string);

        void setPreferences(HealthMackPreferences preferences);

        void showImage(String url);
    }

    interface Presenter extends BasePresenter {
        void loadUsers(String doctorKey);

        void loadAttestation(String date, String consultationKey);

        void saveAttestation(String days, String otherKey, String date, String consultationKey);

        void setupPreferences(HealthMackPreferences preferences);

        void loadSignature(String key);
    }
}
