package br.com.healthmack.attestation;

import dagger.Module;
import dagger.Provides;

@Module
public class AttestationPresenterModule {

    private final Attestation.View mView;

    public AttestationPresenterModule(Attestation.View view) {
        mView = view;
    }

    @Provides
    Attestation.View providesView() {
        return mView;
    }
}
