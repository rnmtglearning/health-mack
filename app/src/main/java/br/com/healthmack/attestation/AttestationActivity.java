package br.com.healthmack.attestation;

import android.os.Bundle;

import javax.inject.Inject;

import br.com.healthmack.AbstractActivity;
import br.com.healthmack.HealthMackApplication;
import br.com.healthmack.R;
import br.com.healthmack.modules.ContextModule;
import br.com.healthmack.modules.FirebaseAuthModule;
import br.com.healthmack.modules.PreferencesModule;
import br.com.healthmack.shared.preferences.HealthMackPreferences;
import br.com.healthmack.utils.ActivityUtils;

public class AttestationActivity extends AbstractActivity {

    @Inject
    AttestationPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AttestationFragment attestationFragment = ActivityUtils.addFragmentToActivity(this, AttestationFragment.class);

        attestationFragment.setArguments(getIntent().getExtras());

        DaggerAttestationComponent.builder()
                .attestationPresenterModule(new AttestationPresenterModule(attestationFragment))
                .attestationRepositoryComponent(((HealthMackApplication) getApplication()).getAttestationRepositoryComponent())
                .contextModule(new ContextModule(getApplicationContext()))
                .firebaseAuthModule(new FirebaseAuthModule())
                .preferencesModule(new PreferencesModule(getApplicationContext()))
                .build()
                .inject(this);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.default_act;
    }

    @Override
    public String getStringResTitle() {
        return getString(R.string.Make_Schedule);
    }

    @Override
    public Integer getMenuRes() {
        return null;
    }
}