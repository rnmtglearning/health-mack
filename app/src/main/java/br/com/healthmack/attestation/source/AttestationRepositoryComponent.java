package br.com.healthmack.attestation.source;

import javax.inject.Singleton;

import br.com.healthmack.modules.FirebaseAuthModule;
import br.com.healthmack.modules.FirebaseDatabaseModule;
import br.com.healthmack.modules.PreferencesModule;
import dagger.Component;

@Singleton
@Component(modules = {AttestationRepositoryModule.class, FirebaseDatabaseModule.class, FirebaseAuthModule.class, PreferencesModule.class})
public interface AttestationRepositoryComponent {

    AttestationRepository getRepository();
}
