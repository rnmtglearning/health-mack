package br.com.healthmack.attestation;

import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionButton;
import com.squareup.picasso.Picasso;

import br.com.healthmack.AbstractActivity;
import br.com.healthmack.AbstractFragment;
import br.com.healthmack.R;
import br.com.healthmack.detailconsultation.DetailActivity;
import br.com.healthmack.scheduleconsultation.listdoctors.ListDoctorsActivity;
import br.com.healthmack.scheduleconsultation.listschedule.ListScheduleActivity;
import br.com.healthmack.shared.model.User;
import br.com.healthmack.shared.preferences.HealthMackPreferences;
import br.com.healthmack.shared.preferences.UserType;
import br.com.healthmack.utils.ToastHelper;
import butterknife.BindView;
import butterknife.OnTextChanged;

public class AttestationFragment extends AbstractFragment<Attestation.Presenter> implements Attestation.View {

    @BindView(R.id.attestationDoctorName)
    TextView doctorName;
    @BindView(R.id.attestationSubtitle)
    TextView subtitle;
    @BindView(R.id.attestationDescription)
    TextView description;
    @BindView(R.id.attestationDays)
    TextView attestationDays;
    @BindView(R.id.daysLayout)
    TextInputLayout daysLayout;
    @BindView(R.id.daysEdit)
    EditText daysEdit;
    @BindView(R.id.attestationSignature)
    ImageView signatureImage;

    private String consultationKey;
    private String consultationDate;
    private String otherKey;
    private String specialty;
    private String hour;
    private String days;
    private HealthMackPreferences mPreferences;

    @Override
    public int getLayoutRes() {
        return R.layout.attestation_frag;
    }

    @Override
    public int getProgressBar() {
        return R.id.progressBar;
    }

    @Override
    public void onCustomCreateView(View view, ViewGroup container) {

        specialty = getArguments().getString(ListDoctorsActivity.ARG_SPECIALTY);
        otherKey = getArguments().getString(ListScheduleActivity.ARG_OTHER_KEY);
        consultationDate = getArguments().getString(DetailActivity.ARG_SCHEDULE_DATE);
        consultationKey = getArguments().getString(DetailActivity.ARG_CONSULTATION_KEY);
        hour = getArguments().getString(DetailActivity.ARG_SCHEDULE_HOUR);

        mPresenter.loadSignature(otherKey);


        mPresenter.loadUsers(otherKey);
        mPresenter.loadAttestation(consultationDate, consultationKey);

        days = "0";
        attestationDays.setText(String.format(getString(R.string.attestation_days_text), "0"));
        daysEdit.setText(days);

        FloatingActionButton fabButton = ((AbstractActivity) getActivity()).fabButton;
        if (fabButton != null) {
            fabButton.setVisibility(View.VISIBLE);
            fabButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_done));
            fabButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mPresenter.saveAttestation(days, otherKey, consultationDate, consultationKey);
                }
            });
        }

        if (mPreferences.getUserType() == UserType.USER) {
            daysEdit.setVisibility(View.GONE);
            if (fabButton != null)
                fabButton.setVisibility(View.GONE);
        }
    }

    @Override
    public void setPreferences(HealthMackPreferences preferences) {
        mPreferences = preferences;
    }

    @OnTextChanged(R.id.daysEdit)
    public void daysChanged() {
        daysLayout.setErrorEnabled(false);
        days = daysEdit.getText().toString();
        attestationDays.setText(String.format(getString(R.string.attestation_days_text), days.isEmpty() ? "0" : days));
    }

    @Override
    public void showUser(User user) {
        description.setText(String.format(getString(R.string.attestation_text), user.getName()
                , user.getRg().substring(0, 3), user.getRg().substring(3, 6), user.getRg().substring(6, 9)
                , user.getRg().substring(9, user.getRg().length()), consultationDate, hour));
    }

    @Override
    public void showAttestation(String string) {
        if (string == null || (string != null && string.equals("0")))
            attestationDays.setVisibility(View.GONE);

        daysEdit.setText( string == null ? "0" : string);
        attestationDays.setText(String.format(getString(R.string.attestation_days_text), string == null ? "0" : string));
    }

    @Override
    public void showDoctor(User user) {
        doctorName.setText(user.getName().toUpperCase());
        subtitle.setText(String.format(getString(R.string.attestation_subtitle), specialty, user.getCrm()));
    }

    @Override
    public void showInvalidField(int layout, int message) {
        TextInputLayout component = getActivity().findViewById(layout);
        component.setErrorEnabled(true);
        component.setError(getString(message));
    }

    @Override
    public void showAttestationSaved() {
        ToastHelper.show(this, "Atestado salvo com sucesso.", Toast.LENGTH_SHORT);
        getActivity().finish();
    }

    @Override
    public void showImage(String url) {
        if (!TextUtils.isEmpty(url)) {
            Picasso.with(getContext())
                    .load(url)
                    .placeholder(R.drawable.estetoscopio)
                    .error(R.drawable.estetoscopio)
                    .priority(Picasso.Priority.HIGH)
                    .into(signatureImage);
        }
    }
}