package br.com.healthmack.attestation.source;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class AttestationRepository implements AttestationDataSource {

    private final AttestationDataSource mAttestationDataSource;

    @Inject
    public AttestationRepository(AttestationDataSource attestationDataSource) {
        this.mAttestationDataSource = attestationDataSource;
    }


    @Override
    public void loadUser(String key, AttestationCallback callback) {
        mAttestationDataSource.loadUser(key, callback);
    }

    @Override
    public void loadAttestation(String userKey, String date, String consultationKey, AttestationCallback callback) {
        mAttestationDataSource.loadAttestation(userKey, date, consultationKey, callback);
    }

    @Override
    public void saveAttestation(String days, String otherKey, String date, String consultationKey, AttestationCallback callback) {
        mAttestationDataSource.saveAttestation(days, otherKey, date, consultationKey, callback);
    }

    @Override
    public void loadSignature(String key, AttestationCallback callback) {
        mAttestationDataSource.loadSignature(key, callback);
    }
}
