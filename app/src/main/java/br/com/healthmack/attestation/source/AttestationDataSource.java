package br.com.healthmack.attestation.source;

import br.com.healthmack.shared.model.User;

public interface AttestationDataSource {

    void loadUser(String key, AttestationCallback callback);

    void loadAttestation(String userKey, String date, String consultationKey, AttestationCallback callback);

    void saveAttestation(String days, String otherKey, String date, String consultationKey, AttestationCallback callback);

    void loadSignature(String key, AttestationCallback callback);

    interface AttestationCallback {

        void onUserLoaded(User user);

        void onAttestationLoaded(String days);

        void onAttestationSaved();

        void imageLoaded(String url);
    }
}
