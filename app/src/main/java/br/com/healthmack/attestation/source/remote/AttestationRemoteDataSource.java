package br.com.healthmack.attestation.source.remote;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import javax.inject.Inject;
import javax.inject.Singleton;

import br.com.healthmack.attestation.source.AttestationDataSource;
import br.com.healthmack.shared.model.User;
import br.com.healthmack.shared.preferences.HealthMackPreferences;
import br.com.healthmack.shared.preferences.UserType;
import br.com.healthmack.utils.FirebaseChilds;

@Singleton
public class AttestationRemoteDataSource implements AttestationDataSource {

    private final FirebaseDatabase mDatabase;
    private final FirebaseAuth mAuth;
    private final HealthMackPreferences mPreferences;

    @Inject
    public AttestationRemoteDataSource(FirebaseDatabase mDatabase, FirebaseAuth auth, HealthMackPreferences preferences) {
        this.mDatabase = mDatabase;
        this.mAuth = auth;
        this.mPreferences = preferences;
    }

    @Override
    public void loadUser(String key, final AttestationCallback callback) {
        mDatabase.getReference().child(FirebaseChilds.CHILD_USER).child(key).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                callback.onUserLoaded(dataSnapshot.getValue(User.class));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public void loadAttestation(String userKey, final String date, String consultationKey, final AttestationCallback callback) {
        mDatabase.getReference().child(FirebaseChilds.CHILD_CONSULTATIONS)
                .child(userKey).child(date).child(consultationKey).child(FirebaseChilds.CHILD_ATTESTATION_DAY)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        callback.onAttestationLoaded(dataSnapshot.getValue(String.class));
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    @Override
    public void saveAttestation(String days, String otherKey, String date, String consultationKey, AttestationCallback callback) {
        String userKey = mAuth.getCurrentUser().getUid();

        DatabaseReference reference = mDatabase.getReference().child(FirebaseChilds.CHILD_CONSULTATIONS);
        reference.child(otherKey).child(date).child(consultationKey).child(FirebaseChilds.CHILD_ATTESTATION_DAY).setValue(days);
        reference.child(userKey).child(date).child(consultationKey).child(FirebaseChilds.CHILD_ATTESTATION_DAY).setValue(days);

        callback.onAttestationSaved();
    }

    @Override
    public void loadSignature(String key, final AttestationCallback callback) {
        String userKey = mPreferences.getUserType() == UserType.DOCTOR ? mAuth.getCurrentUser().getUid() : key;
        mDatabase.getReference(FirebaseChilds.CHILD_USER)
                .child(userKey)
                .child(FirebaseChilds.CHILD_SIGNATURE)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        callback.imageLoaded(dataSnapshot.getValue(String.class));
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        callback.imageLoaded(null);
                    }
                });
    }
}
