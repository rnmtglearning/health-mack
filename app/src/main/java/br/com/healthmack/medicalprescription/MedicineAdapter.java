package br.com.healthmack.medicalprescription;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import br.com.healthmack.R;
import br.com.healthmack.medicalprescription.source.Medicine;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MedicineAdapter extends RecyclerView.Adapter<MedicineAdapter.ViewHolder> {

    private List<Medicine> datasource = new ArrayList<>();
    private Context mContext;

    public MedicineAdapter(Context context) {
        this.mContext = context;
    }

    public List<Medicine> getDatasource() {
        return datasource;
    }

    public void setDatasource(List<Medicine> datasource) {
        this.datasource = datasource;
    }

    @Override
    public MedicineAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.medical_prescriptions_card, parent, false);
        return new MedicineAdapter.ViewHolder(root);
    }

    @Override
    public void onBindViewHolder(MedicineAdapter.ViewHolder holder, int position) {
        Medicine medicine = datasource.get(position);
        holder.title.setText(medicine.getName());
        holder.prescription.setText(String.format(mContext.getString(R.string.medical_prescription_detail), medicine.getQuantity(), medicine.getQuantityUnit(), medicine.getQuantityDay(), medicine.getDuration(), medicine.getDurationUnit()));
    }

    @Override
    public int getItemCount() {
        return datasource != null ? datasource.size() : 0;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.medicalNameText)
        TextView title;
        @BindView(R.id.medicalPrescription)
        TextView prescription;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}