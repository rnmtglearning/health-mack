package br.com.healthmack.medicalprescription;

import br.com.healthmack.medicalprescription.source.Medicine;
import br.com.healthmack.shared.preferences.HealthMackPreferences;
import br.com.healthmack.utils.BasePresenter;
import br.com.healthmack.utils.BaseView;

public interface MedicalPrescription {

    interface View extends BaseView<Presenter> {

        void setPreferences(HealthMackPreferences preferences);

        void showMedicals(Medicine medicine);

        void showSignature(String url);
    }

    interface Presenter extends BasePresenter {

        void setupPreferences(HealthMackPreferences preferences);

        void saveMedicine(Medicine medicine, String otherKey, String date, String consultationKey);

        void loadMedicals(String date, String consultationKey);

        void loadSignature(String key);
    }
}
