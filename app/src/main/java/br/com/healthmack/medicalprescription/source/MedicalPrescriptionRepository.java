package br.com.healthmack.medicalprescription.source;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class MedicalPrescriptionRepository implements MedicalPrescriptionDataSource {

    private final MedicalPrescriptionDataSource mMedicalPrescriptionDataSource;

    @Inject
    public MedicalPrescriptionRepository(MedicalPrescriptionDataSource medicalPrescriptionDataSource) {
        this.mMedicalPrescriptionDataSource = medicalPrescriptionDataSource;
    }

    @Override
    public void createMedicine(String date, String otherKey, String consultationKey, Medicine medicine, MedicalPrescriptionCallback callback) {
        mMedicalPrescriptionDataSource.createMedicine(date, otherKey, consultationKey, medicine, callback);
    }

    @Override
    public void loadMedicals(String date, String consultationKey, MedicalPrescriptionCallback callback) {
        mMedicalPrescriptionDataSource.loadMedicals(date, consultationKey, callback);
    }

    @Override
    public void loadSignature(String key, MedicalPrescriptionCallback callback) {
        mMedicalPrescriptionDataSource.loadSignature(key, callback);
    }
}
