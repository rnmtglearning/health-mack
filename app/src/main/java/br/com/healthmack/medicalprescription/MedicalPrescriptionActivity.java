package br.com.healthmack.medicalprescription;

import android.os.Bundle;

import javax.inject.Inject;

import br.com.healthmack.AbstractActivity;
import br.com.healthmack.HealthMackApplication;
import br.com.healthmack.R;
import br.com.healthmack.modules.ContextModule;
import br.com.healthmack.modules.PreferencesModule;
import br.com.healthmack.utils.ActivityUtils;

public class MedicalPrescriptionActivity extends AbstractActivity {

    @Inject
    MedicalPrescriptionPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        MedicalPrescriptionFragment medicalPrescriptionFragment = ActivityUtils.addFragmentToActivity(this, MedicalPrescriptionFragment.class);
        medicalPrescriptionFragment.setArguments(getIntent().getExtras());

        DaggerMedicalPrescriptionComponent.builder()
                .contextModule(new ContextModule(getApplicationContext()))
                .medicalPrescriptionPresenterModule(new MedicalPrescriptionPresenterModule(medicalPrescriptionFragment))
                .medicalPrescriptionRepositoryComponent(((HealthMackApplication) getApplication()).getMedicalPrescriptionRepositoryComponent())
                .preferencesModule(new PreferencesModule(getApplicationContext()))
                .build()
                .inject(this);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.default_act;
    }

    @Override
    public String getStringResTitle() {
        return getString(R.string.Make_Schedule);
    }

    @Override
    public Integer getMenuRes() {
        return null;
    }
}