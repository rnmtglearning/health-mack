package br.com.healthmack.medicalprescription.source;

import java.util.HashMap;
import java.util.Map;

public class Medicine {

    private String name;
    private String quantity;
    private String quantityUnit;
    private String quantityDay;
    private String duration;
    private String durationUnit;

    public Medicine() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getQuantityUnit() {
        return quantityUnit;
    }

    public void setQuantityUnit(String quantityUnit) {
        this.quantityUnit = quantityUnit;
    }

    public String getQuantityDay() {
        return quantityDay;
    }

    public void setQuantityDay(String quantityDay) {
        this.quantityDay = quantityDay;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getDurationUnit() {
        return durationUnit;
    }

    public void setDurationUnit(String durationUnit) {
        this.durationUnit = durationUnit;
    }

    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("name", name);
        result.put("quantity", quantity);
        result.put("quantityUnit", quantityUnit);
        result.put("quantityDay", quantityDay);
        result.put("duration", duration);
        result.put("durationUnit", durationUnit);
        return result;
    }
}
