package br.com.healthmack.medicalprescription.source;

import javax.inject.Singleton;

import br.com.healthmack.medicalprescription.source.remote.MedicalPrescriptionRemoteDataSource;
import dagger.Binds;
import dagger.Module;

@Module
public abstract class MedicalPrescriptionRepositoryModule {

    @Singleton
    @Binds
    abstract MedicalPrescriptionDataSource provideDataSource(MedicalPrescriptionRemoteDataSource remoteDataSource);
}
