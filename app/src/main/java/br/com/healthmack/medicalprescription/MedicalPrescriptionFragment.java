package br.com.healthmack.medicalprescription;

import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import com.squareup.picasso.Picasso;

import java.util.List;

import br.com.healthmack.AbstractFragment;
import br.com.healthmack.R;
import br.com.healthmack.detailconsultation.DetailActivity;
import br.com.healthmack.medicalprescription.source.Medicine;
import br.com.healthmack.scheduleconsultation.listschedule.ListScheduleActivity;
import br.com.healthmack.shared.preferences.HealthMackPreferences;
import br.com.healthmack.shared.preferences.UserType;
import butterknife.BindArray;
import butterknife.BindView;
import butterknife.OnItemSelected;
import butterknife.OnTextChanged;

public class MedicalPrescriptionFragment extends AbstractFragment<MedicalPrescription.Presenter> implements MedicalPrescription.View {

    @BindView(R.id.newMedicineCard)
    CardView cardView;

    @BindView(R.id.medicineName)
    EditText medicineName;
    @BindView(R.id.medicineNameLayot)
    TextInputLayout medicineNameLayout;

    @BindView(R.id.quantityEdit)
    EditText quantityEdit;
    @BindView(R.id.quantityEditLayout)
    TextInputLayout quantityEditLayout;
    @BindView(R.id.spinnerQuantity)
    Spinner spinnerQuantity;

    @BindView(R.id.spinnerQuantityDay)
    Spinner spinnerQuantityDay;

    @BindView(R.id.durationEdit)
    EditText durationEdit;
    @BindView(R.id.durationEditLayout)
    TextInputLayout durationEditLayout;
    @BindView(R.id.spinnerDuration)
    Spinner spinnerDuration;

    @BindArray(R.array.unit_quantity)
    String[] quantity;
    @BindArray(R.array.unit_quantity_day)
    String[] quantityDay;
    @BindArray(R.array.unit_duration)
    String[] duration;

    @BindView(R.id.addMedicine)
    Button addMedicine;

    @BindView(R.id.recycler)
    RecyclerView mRecycler;

    @BindView(R.id.medicalSignature)
    ImageView signature;

    private Medicine mMedicine;
    private MedicineAdapter mAdapter;
    private HealthMackPreferences mPreferences;

    @Override
    public int getLayoutRes() {
        return R.layout.medical_prescription_frag;
    }

    @Override
    public int getProgressBar() {
        return R.id.progressBar;
    }

    @Override
    public void onCustomCreateView(View view, ViewGroup container) {

        ArrayAdapter<String> quantityAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, quantity);
        ArrayAdapter<String> quantityDayAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, quantityDay);
        ArrayAdapter<String> durationAdapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_item, duration);

        quantityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        quantityDayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        durationAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinnerQuantity.setAdapter(quantityAdapter);
        spinnerQuantityDay.setAdapter(quantityDayAdapter);
        spinnerDuration.setAdapter(durationAdapter);

        mMedicine = new Medicine();

        final String doctorKey = getArguments().getString(ListScheduleActivity.ARG_OTHER_KEY);
        final String date = getArguments().getString(DetailActivity.ARG_SCHEDULE_DATE);
        final String consultationKey = getArguments().getString(DetailActivity.ARG_CONSULTATION_KEY);

        mPresenter.loadSignature(doctorKey);

        addMedicine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPresenter.saveMedicine(mMedicine,
                        doctorKey,
                        date,
                        consultationKey);
            }
        });

        mPresenter.loadMedicals(date, consultationKey);

        mAdapter = new MedicineAdapter(getContext());
        mRecycler.setAdapter(mAdapter);
        mRecycler.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));

        if (mPreferences.getUserType() == UserType.USER)
            cardView.setVisibility(View.GONE);
    }

    @Override
    public void setPreferences(HealthMackPreferences preferences) {
        mPreferences = preferences;
    }

    @OnItemSelected(R.id.spinnerQuantityDay)
    public void quantityDaySelected(int position) {
        mMedicine.setQuantityDay(quantityDay[position]);
    }

    @OnItemSelected(R.id.spinnerQuantity)
    public void quantityUnitSelected(int position) {
        mMedicine.setQuantityUnit(quantity[position]);
    }

    @OnItemSelected(R.id.spinnerDuration)
    public void durationUnitSelected(int position) {
        mMedicine.setDurationUnit(duration[position]);
    }

    @OnTextChanged(R.id.quantityEdit)
    public void quantityChanged() {
        mMedicine.setQuantity(quantityEdit.getText() != null ? quantityEdit.getText().toString() : "");
    }

    @OnTextChanged(R.id.durationEdit)
    public void durationChanged() {
        mMedicine.setDuration(durationEdit.getText() != null ? durationEdit.getText().toString() : "");
    }

    @OnTextChanged(R.id.medicineName)
    public void nameChanged() {
        mMedicine.setName(medicineName.getText() != null ? medicineName.getText().toString() : "");
    }

    @Override
    public void showMedicals(Medicine medicine) {
        List<Medicine> datasource = mAdapter.getDatasource();
        datasource.add(medicine);
        mAdapter.notifyDataSetChanged();

        medicineName.setText(null);
        quantityEdit.setText(null);
        spinnerQuantity.setSelection(0);
        spinnerQuantityDay.setSelection(0);
        durationEdit.setText(null);
        spinnerDuration.setSelection(0);

        mMedicine = new Medicine();
    }

    @Override
    public void showSignature(String url) {
        if (!TextUtils.isEmpty(url)) {
            Picasso.with(getContext())
                    .load(url)
                    .placeholder(R.drawable.estetoscopio)
                    .error(R.drawable.estetoscopio)
                    .priority(Picasso.Priority.HIGH)
                    .into(signature);
        }
    }
}