package br.com.healthmack.medicalprescription;

import br.com.healthmack.medicalprescription.source.MedicalPrescriptionRepositoryComponent;
import br.com.healthmack.modules.ContextModule;
import br.com.healthmack.modules.PreferencesModule;
import br.com.healthmack.shared.preferences.HealthMackPreferences;
import br.com.healthmack.utils.FragmentScoped;
import dagger.Component;

@FragmentScoped
@Component(dependencies = MedicalPrescriptionRepositoryComponent.class, modules = {MedicalPrescriptionPresenterModule.class,
        ContextModule.class,
        PreferencesModule.class})
public interface MedicalPrescriptionComponent {

    void inject(MedicalPrescriptionActivity medicalPrescriptionActivity);
}
