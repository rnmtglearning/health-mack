package br.com.healthmack.medicalprescription.source;

import javax.inject.Singleton;

import br.com.healthmack.modules.FirebaseAuthModule;
import br.com.healthmack.modules.FirebaseDatabaseModule;
import br.com.healthmack.modules.PreferencesModule;
import dagger.Component;

@Singleton
@Component(modules = {MedicalPrescriptionRepositoryModule.class,
        FirebaseDatabaseModule.class,
        FirebaseAuthModule.class,
        PreferencesModule.class})
public interface MedicalPrescriptionRepositoryComponent {

    MedicalPrescriptionRepository getRepository();
}
