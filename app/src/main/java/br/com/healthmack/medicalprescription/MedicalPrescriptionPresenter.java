package br.com.healthmack.medicalprescription;

import android.content.Context;
import android.text.TextUtils;

import javax.inject.Inject;

import br.com.healthmack.R;
import br.com.healthmack.medicalprescription.source.MedicalPrescriptionDataSource;
import br.com.healthmack.medicalprescription.source.MedicalPrescriptionRepository;
import br.com.healthmack.medicalprescription.source.Medicine;
import br.com.healthmack.shared.preferences.HealthMackPreferences;

final class MedicalPrescriptionPresenter implements MedicalPrescription.Presenter, MedicalPrescriptionDataSource.MedicalPrescriptionCallback {

    private final MedicalPrescriptionRepository mRepository;
    private final MedicalPrescription.View mView;
    private Context mContext;

    @Inject
    MedicalPrescriptionPresenter(MedicalPrescriptionRepository repository, MedicalPrescription.View view, Context context) {
        mRepository = repository;
        mView = view;
        mContext = context;
    }

    @Inject
    @Override
    public void setupListeners() {
        mView.setPresenter(this);
    }

    @Inject
    @Override
    public void setupPreferences(HealthMackPreferences preferences) {
        mView.setPreferences(preferences);
    }

    @Override
    public void saveMedicine(Medicine medicine, String otherKey, String date, String consultationKey) {
        mView.showProgressBar();
        if (TextUtils.isEmpty(medicine.getName()))
            mView.showMessage(R.string.error_invalid_name);
        else if (TextUtils.isEmpty(medicine.getQuantity()))
            mView.showMessage(R.string.error_invalid_quantity);
        else if (TextUtils.isEmpty(medicine.getQuantityUnit()))
            mView.showMessage(R.string.error_invalid_unit_quantity);
        else if (TextUtils.isEmpty(medicine.getQuantityDay()))
            mView.showMessage(R.string.error_invalid_quantity_day);
        else if (TextUtils.isEmpty(medicine.getDuration()))
            mView.showMessage(R.string.error_invalida_duration);
        else if (TextUtils.isEmpty(medicine.getDurationUnit()))
            mView.showMessage(R.string.error_invalid_duration_unit);
        else
            mRepository.createMedicine(date, otherKey, consultationKey, medicine, this);
    }

    @Override
    public void loadMedicals(String date, String consultationKey) {
        mRepository.loadMedicals(date, consultationKey, this);
    }

    @Override
    public void loadSignature(String key) {
        mView.showProgressBar();
        mRepository.loadSignature(key, this);
    }

    @Override
    public void onMedicineCreated() {
        mView.hideProgressBar();
        mView.showMessage(R.string.medical_created);
    }

    @Override
    public void onMedicineLoaded(Medicine medicine) {
        mView.showMedicals(medicine);
    }

    @Override
    public void imageLoaded(String url) {
        mView.hideProgressBar();
        mView.showSignature(url);
    }
}
