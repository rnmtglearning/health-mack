package br.com.healthmack.medicalprescription.source.remote;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import javax.inject.Inject;
import javax.inject.Singleton;

import br.com.healthmack.medicalprescription.source.MedicalPrescriptionDataSource;
import br.com.healthmack.medicalprescription.source.Medicine;
import br.com.healthmack.shared.preferences.HealthMackPreferences;
import br.com.healthmack.shared.preferences.UserType;
import br.com.healthmack.utils.FirebaseChilds;

@Singleton
public class MedicalPrescriptionRemoteDataSource implements MedicalPrescriptionDataSource {

    private final FirebaseDatabase mDatabase;
    private final FirebaseAuth mAuth;
    private final HealthMackPreferences mPreferences;

    @Inject
    public MedicalPrescriptionRemoteDataSource(FirebaseDatabase mDatabase, FirebaseAuth auth, HealthMackPreferences preferences) {
        this.mDatabase = mDatabase;
        this.mAuth = auth;
        this.mPreferences = preferences;
    }

    @Override
    public void createMedicine(String date, String otherKey, String consultationKey, Medicine medicine, MedicalPrescriptionCallback callback) {
        String userKey = mAuth.getCurrentUser().getUid();

        DatabaseReference reference = mDatabase.getReference().child(FirebaseChilds.CHILD_CONSULTATIONS);

        DatabaseReference userReference = reference.child(userKey).child(date)
                .child(consultationKey)
                .child(FirebaseChilds.CHILD_MEDICAL_PRESCRIPTIONS)
                .push();

        String prescriptionKey = userReference.getKey();
        userReference.setValue(medicine);

        reference.child(otherKey).child(date).child(consultationKey)
                .child(FirebaseChilds.CHILD_MEDICAL_PRESCRIPTIONS)
                .child(prescriptionKey)
                .setValue(medicine);

        callback.onMedicineCreated();
    }

    @Override
    public void loadMedicals(String date, String consultationKey, final MedicalPrescriptionCallback callback) {
        String userKey = mAuth.getCurrentUser().getUid();

        mDatabase.getReference()
                .child(FirebaseChilds.CHILD_CONSULTATIONS)
                .child(userKey).child(date)
                .child(consultationKey)
                .child(FirebaseChilds.CHILD_MEDICAL_PRESCRIPTIONS)
                .addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        Medicine value = dataSnapshot.getValue(Medicine.class);
                        if (value != null)
                            callback.onMedicineLoaded(value);
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {

                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    @Override
    public void loadSignature(String key, final MedicalPrescriptionCallback callback) {
        String userKey = mPreferences.getUserType() == UserType.DOCTOR ? mAuth.getCurrentUser().getUid() : key;
        mDatabase.getReference(FirebaseChilds.CHILD_USER)
                .child(userKey)
                .child(FirebaseChilds.CHILD_SIGNATURE)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        callback.imageLoaded(dataSnapshot.getValue(String.class));
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        callback.imageLoaded(null);
                    }
                });
    }
}
