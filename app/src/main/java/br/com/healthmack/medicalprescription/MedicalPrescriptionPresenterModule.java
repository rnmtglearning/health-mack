package br.com.healthmack.medicalprescription;

import dagger.Module;
import dagger.Provides;

@Module
public class MedicalPrescriptionPresenterModule {

    private final MedicalPrescription.View mView;

    public MedicalPrescriptionPresenterModule(MedicalPrescription.View view) {
        mView = view;
    }

    @Provides
    MedicalPrescription.View providesView() {
        return mView;
    }
}
