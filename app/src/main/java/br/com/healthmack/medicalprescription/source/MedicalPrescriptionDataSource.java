package br.com.healthmack.medicalprescription.source;

public interface MedicalPrescriptionDataSource {

    void createMedicine(String date, String otherKey, String consultationKey, Medicine medicine, MedicalPrescriptionCallback callback);

    void loadMedicals(String date, String consultationKey, MedicalPrescriptionCallback callback);

    void loadSignature(String key, MedicalPrescriptionCallback callback);

    interface MedicalPrescriptionCallback {

        void onMedicineCreated();

        void onMedicineLoaded(Medicine medicine);

        void imageLoaded(String url);
    }
}
