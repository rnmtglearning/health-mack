package br.com.healthmack;

import android.app.Application;

import com.google.firebase.FirebaseApp;

import br.com.healthmack.addsignature.DaggerAddSignatureComponent;
import br.com.healthmack.addsignature.source.AddSignatureRepositoryComponent;
import br.com.healthmack.addsignature.source.DaggerAddSignatureRepositoryComponent;
import br.com.healthmack.attestation.DaggerAttestationComponent;
import br.com.healthmack.attestation.source.AttestationRepositoryComponent;
import br.com.healthmack.attestation.source.DaggerAttestationRepositoryComponent;
import br.com.healthmack.detailconsultation.data.source.DaggerDetailConsultationRepositoryComponent;
import br.com.healthmack.detailconsultation.data.source.DetailConsultationRepositoryComponent;
import br.com.healthmack.listconsultations.source.DaggerListConsultationsRepositoryComponent;
import br.com.healthmack.listconsultations.source.ListConsultationsRepositoryComponent;
import br.com.healthmack.makeschedule.data.source.DaggerMakeScheduleRepositoryComponent;
import br.com.healthmack.makeschedule.data.source.MakeScheduleRepositoryComponent;
import br.com.healthmack.medicalprescription.source.DaggerMedicalPrescriptionRepositoryComponent;
import br.com.healthmack.medicalprescription.source.MedicalPrescriptionRepositoryComponent;
import br.com.healthmack.menu.source.DaggerMenuRepositoryComponent;
import br.com.healthmack.menu.source.MenuRepositoryComponent;
import br.com.healthmack.modules.FirebaseAuthModule;
import br.com.healthmack.modules.FirebaseDatabaseModule;
import br.com.healthmack.modules.PreferencesModule;
import br.com.healthmack.scheduleconsultation.data.source.DaggerScheduleConsultationRepositoryComponent;
import br.com.healthmack.scheduleconsultation.data.source.ScheduleConsultationRepositoryComponent;
import br.com.healthmack.useraccount.data.source.DaggerUserRepositoryComponent;
import br.com.healthmack.useraccount.data.source.UserRepositoryComponent;

public class HealthMackApplication extends Application {

    private UserRepositoryComponent userRepositoryComponent;
    private ScheduleConsultationRepositoryComponent scheduleConsultationRepositoryComponent;
    private MakeScheduleRepositoryComponent makeScheduleRepositoryComponent;
    private MenuRepositoryComponent menuRepositoryComponent;
    private ListConsultationsRepositoryComponent listConsultationsRepositoryComponent;
    private DetailConsultationRepositoryComponent detailConsultationRepositoryComponent;
    private MedicalPrescriptionRepositoryComponent medicalPrescriptionRepositoryComponent;
    private AttestationRepositoryComponent attestationRepositoryComponent;
    private AddSignatureRepositoryComponent addSignatureRepositoryComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        FirebaseApp.initializeApp(getApplicationContext());

        userRepositoryComponent = DaggerUserRepositoryComponent.builder()
                .firebaseAuthModule(new FirebaseAuthModule())
                .firebaseDatabaseModule(new FirebaseDatabaseModule())
                .build();

        scheduleConsultationRepositoryComponent = DaggerScheduleConsultationRepositoryComponent.builder()
                .firebaseDatabaseModule(new FirebaseDatabaseModule())
                .firebaseAuthModule(new FirebaseAuthModule())
                .build();

        makeScheduleRepositoryComponent = DaggerMakeScheduleRepositoryComponent.builder()
                .firebaseDatabaseModule(new FirebaseDatabaseModule())
                .build();

        menuRepositoryComponent = DaggerMenuRepositoryComponent.builder()
                .firebaseDatabaseModule(new FirebaseDatabaseModule())
                .firebaseAuthModule(new FirebaseAuthModule())
                .build();

        listConsultationsRepositoryComponent = DaggerListConsultationsRepositoryComponent.builder()
                .firebaseDatabaseModule(new FirebaseDatabaseModule())
                .firebaseAuthModule(new FirebaseAuthModule())
                .build();

        detailConsultationRepositoryComponent = DaggerDetailConsultationRepositoryComponent.builder()
                .firebaseAuthModule(new FirebaseAuthModule())
                .firebaseDatabaseModule(new FirebaseDatabaseModule())
                .preferencesModule(new PreferencesModule(getApplicationContext()))
                .build();

        medicalPrescriptionRepositoryComponent = DaggerMedicalPrescriptionRepositoryComponent.builder()
                .firebaseDatabaseModule(new FirebaseDatabaseModule())
                .firebaseAuthModule(new FirebaseAuthModule())
                .preferencesModule(new PreferencesModule(getApplicationContext()))
                .build();

        attestationRepositoryComponent = DaggerAttestationRepositoryComponent.builder()
                .firebaseDatabaseModule(new FirebaseDatabaseModule())
                .firebaseAuthModule(new FirebaseAuthModule())
                .preferencesModule(new PreferencesModule(getApplicationContext()))
                .build();

        addSignatureRepositoryComponent = DaggerAddSignatureRepositoryComponent.builder()
                .firebaseDatabaseModule(new FirebaseDatabaseModule())
                .build();
    }

    public UserRepositoryComponent getUserRepositoryComponent() {
        return userRepositoryComponent;
    }

    public ScheduleConsultationRepositoryComponent getScheduleConsultationRepositoryComponent() {
        return scheduleConsultationRepositoryComponent;
    }

    public MakeScheduleRepositoryComponent getMakeScheduleRepositoryComponent() {
        return makeScheduleRepositoryComponent;
    }

    public ListConsultationsRepositoryComponent getListConsultationsRepositoryComponent() {
        return listConsultationsRepositoryComponent;
    }

    public MenuRepositoryComponent getMenuRepositoryComponent() {
        return menuRepositoryComponent;
    }

    public DetailConsultationRepositoryComponent getDetailConsultationRepositoryComponent() {
        return detailConsultationRepositoryComponent;
    }

    public MedicalPrescriptionRepositoryComponent getMedicalPrescriptionRepositoryComponent() {
        return medicalPrescriptionRepositoryComponent;
    }

    public AttestationRepositoryComponent getAttestationRepositoryComponent() {
        return attestationRepositoryComponent;
    }

    public AddSignatureRepositoryComponent getAddSignatureRepositoryComponent() {
        return addSignatureRepositoryComponent;
    }
}
