package br.com.healthmack.modules;

import com.google.firebase.database.FirebaseDatabase;

import dagger.Module;
import dagger.Provides;

@Module
public class FirebaseDatabaseModule {

    private final FirebaseDatabase mDatabase;

    public FirebaseDatabaseModule() {
        mDatabase = FirebaseDatabase.getInstance();
    }

    @Provides
    public FirebaseDatabase privideFirebaseDatabase() {
        return mDatabase;
    }
}
