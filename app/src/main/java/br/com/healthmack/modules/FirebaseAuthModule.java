package br.com.healthmack.modules;

import com.google.firebase.auth.FirebaseAuth;

import dagger.Module;
import dagger.Provides;

@Module
public class FirebaseAuthModule {

    private final FirebaseAuth mAuth;

    public FirebaseAuthModule() {
        mAuth = FirebaseAuth.getInstance();
    }

    @Provides
    public FirebaseAuth privideFirebaseAuth() {
        return mAuth;
    }
}
