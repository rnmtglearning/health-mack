package br.com.healthmack.modules;

import com.google.firebase.storage.FirebaseStorage;

import dagger.Module;
import dagger.Provides;

@Module
public class FirebaseStorageModule {

    private final FirebaseStorage mStorage;

    public FirebaseStorageModule() {
        mStorage = FirebaseStorage.getInstance();
    }

    @Provides
    public FirebaseStorage provideFirebaseStorage() {
        return mStorage;
    }
}
