package br.com.healthmack.modules;


import android.content.Context;

import dagger.Module;
import dagger.Provides;

@Module
public class ContextModule {

    private final Context mContext;

    /**
     * Instantiates a new Application module.
     *
     * @param context the context
     */
    public ContextModule(Context context) {
        mContext = context;
    }

    /**
     * Provide a context.
     *
     * @return the context
     */
    @Provides
    public Context provideContext() {
        return mContext;
    }

}
