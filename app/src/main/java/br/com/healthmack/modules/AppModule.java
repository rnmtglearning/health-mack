package br.com.healthmack.modules;

import android.app.Application;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public final class AppModule {

    private final Application mApplication;

    /**
     * Instantiates a new App module.
     *
     * @param application the application
     */
    public AppModule(Application application) {
        mApplication = application;
    }

    /**
     * Provide a application.
     *
     * @return the application
     */
    @Provides
    @Singleton
    public Application provideApplication() {
        return mApplication;
    }

}