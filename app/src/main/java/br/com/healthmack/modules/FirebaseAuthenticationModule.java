package br.com.healthmack.modules;

import com.google.firebase.auth.FirebaseAuth;

import dagger.Module;
import dagger.Provides;

@Module
public class FirebaseAuthenticationModule {

    private FirebaseAuth mAuth;

    public FirebaseAuthenticationModule() {
        mAuth = FirebaseAuth.getInstance();
    }

    @Provides
    public FirebaseAuth provideAuthentication() {
        return mAuth;
    }
}
