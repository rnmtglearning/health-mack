package br.com.healthmack.modules;

import android.content.Context;

import br.com.healthmack.shared.preferences.HealthMackPreferences;
import dagger.Module;
import dagger.Provides;

@Module
public class PreferencesModule {
    private final HealthMackPreferences mPreferences;

    public PreferencesModule(Context context) {
        mPreferences = new HealthMackPreferences(context);
    }

    @Provides
    public HealthMackPreferences providePreferences() {
        return mPreferences;
    }
}