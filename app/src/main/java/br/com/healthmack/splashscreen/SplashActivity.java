package br.com.healthmack.splashscreen;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;

import java.util.Timer;
import java.util.TimerTask;

import br.com.healthmack.R;
import br.com.healthmack.menu.MenuActivity;
import br.com.healthmack.useraccount.userlogin.UserLoginActivity;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_act);

        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                if (FirebaseAuth.getInstance().getCurrentUser() != null)
                    startActivity(new Intent(SplashActivity.this, MenuActivity.class));
                else
                    startActivity(new Intent(SplashActivity.this, UserLoginActivity.class));
                finish();
            }
        }, 2000);
    }
}
