package br.com.healthmack.scheduleconsultation.listdoctors;

import dagger.Module;
import dagger.Provides;

@Module
public class ListDoctorsPresenterModule {

    private final ListDoctors.View mView;

    public ListDoctorsPresenterModule(ListDoctors.View view) {
        mView = view;
    }

    @Provides
    ListDoctors.View providesView() {
        return mView;
    }
}
