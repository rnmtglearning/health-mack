package br.com.healthmack.scheduleconsultation.data.source;

import javax.inject.Singleton;

import br.com.healthmack.modules.FirebaseAuthModule;
import br.com.healthmack.modules.FirebaseDatabaseModule;
import dagger.Component;

@Singleton
@Component(modules = {ScheduleConsultationRepositoryModule.class, FirebaseDatabaseModule.class, FirebaseAuthModule.class})
public interface ScheduleConsultationRepositoryComponent {

    ScheduleConsultationRepository getRepository();
}
