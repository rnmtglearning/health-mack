package br.com.healthmack.scheduleconsultation.data.source;

import java.util.List;

import br.com.healthmack.scheduleconsultation.data.Specialty;
import br.com.healthmack.shared.model.User;
import br.com.healthmack.useraccount.data.Clinic;

public interface ScheduleConsultationDataSource {

    void loadSpecialties(ListSpecialtiesCallback listSpecialtiesCallback);

    void loadDoctors(String specialty, ListDoctorsCallback listDoctorsCallback);

    void loadDoctorSchedule(String doctorKey, ListScheduleCallback listScheduleCallback);

    interface ListSpecialtiesCallback {

        void onSpecialtiesLoaded(List<Specialty> specialties);

        void onSpecialtiesNotLoaded();

    }

    interface ListDoctorsCallback {
        void onDoctorLoaded(List<User> doctors);

        void onDoctorNotLoaded();
    }

    interface ListScheduleCallback {
        void onScheduleLoaded(List<String> schedule);
    }
}
