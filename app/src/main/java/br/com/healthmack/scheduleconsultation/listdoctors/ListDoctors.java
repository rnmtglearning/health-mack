package br.com.healthmack.scheduleconsultation.listdoctors;

import java.util.List;

import br.com.healthmack.shared.model.User;
import br.com.healthmack.utils.BasePresenter;
import br.com.healthmack.utils.BaseView;

public interface ListDoctors {

    interface View extends BaseView<Presenter> {
        void showDoctor(List<User> user);
    }

    interface Presenter extends BasePresenter {
        void loadDoctors(String specialty);
    }
}

