package br.com.healthmack.scheduleconsultation.listdoctors;

import br.com.healthmack.scheduleconsultation.data.source.ScheduleConsultationRepositoryComponent;
import br.com.healthmack.utils.FragmentScoped;
import dagger.Component;

@FragmentScoped
@Component(dependencies = ScheduleConsultationRepositoryComponent.class, modules = {ListDoctorsPresenterModule.class})
public interface ListDoctorsComponent {

    void inject(ListDoctorsActivity listDoctorsActivity);
}
