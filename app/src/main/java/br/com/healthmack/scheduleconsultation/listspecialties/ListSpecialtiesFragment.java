package br.com.healthmack.scheduleconsultation.listspecialties;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import br.com.healthmack.AbstractFragment;
import br.com.healthmack.R;
import br.com.healthmack.scheduleconsultation.data.Specialty;
import butterknife.BindView;

public class ListSpecialtiesFragment extends AbstractFragment<ListSpecialties.Presenter> implements ListSpecialties.View {

    private ListSpecialtiesAdapter mAdapter;

    @BindView(R.id.recycler)
    RecyclerView mRecycler;

    @Override
    public int getLayoutRes() {
        return R.layout.simple_list_frag;
    }

    @Override
    public int getProgressBar() {
        return R.id.progressBar;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onCustomCreateView(View view, ViewGroup container) {
        mPresenter.loadSpecialties();
    }

    @Override
    public void showSpecialties(List<Specialty> specialties) {
        mAdapter = new ListSpecialtiesAdapter(getActivity());
        mAdapter.setDatasource(specialties);
        mAdapter.notifyDataSetChanged();

        mRecycler.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecycler.setAdapter(mAdapter);

    }
}