package br.com.healthmack.scheduleconsultation.listspecialties;

import br.com.healthmack.scheduleconsultation.data.source.ScheduleConsultationRepositoryComponent;
import br.com.healthmack.utils.FragmentScoped;
import dagger.Component;

@FragmentScoped
@Component(dependencies = ScheduleConsultationRepositoryComponent.class, modules = {ListSpecialtiesPresenterModule.class})
public interface ListSpecialtiesComponent {

    void inject(ListSpecialtiesActivity listSpecialtiesActivity);
}
