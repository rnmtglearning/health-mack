package br.com.healthmack.scheduleconsultation.listdoctors;

import android.os.Bundle;

import javax.inject.Inject;

import br.com.healthmack.AbstractActivity;
import br.com.healthmack.HealthMackApplication;
import br.com.healthmack.R;
import br.com.healthmack.utils.ActivityUtils;

public class ListDoctorsActivity extends AbstractActivity {

    public static final String ARG_SPECIALTY = "specialty";

    @Inject
    ListDoctorsPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ListDoctorsFragment listDoctorsFragment = ActivityUtils.addFragmentToActivity(this, ListDoctorsFragment.class);
        listDoctorsFragment.setArguments(getIntent().getExtras());

        DaggerListDoctorsComponent.builder()
                .listDoctorsPresenterModule(new ListDoctorsPresenterModule(listDoctorsFragment))
                .scheduleConsultationRepositoryComponent(((HealthMackApplication) getApplication()).getScheduleConsultationRepositoryComponent())
                .build()
                .inject(this);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.default_act;
    }

    @Override
    public String getStringResTitle() {
        return getString(R.string.Doctors);
    }

    @Override
    public Integer getMenuRes() {
        return null;
    }
}