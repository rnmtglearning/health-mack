package br.com.healthmack.scheduleconsultation.listspecialties;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import br.com.healthmack.R;
import br.com.healthmack.scheduleconsultation.data.Specialty;
import br.com.healthmack.scheduleconsultation.listdoctors.ListDoctorsActivity;
import br.com.healthmack.utils.ActivityUtils;
import br.com.healthmack.utils.AnimUtils;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ListSpecialtiesAdapter extends RecyclerView.Adapter<ListSpecialtiesAdapter.ViewHolder> {

    private List<Specialty> datasource = new ArrayList<>();
    private Activity mContext;

    public ListSpecialtiesAdapter(Activity context) {
        mContext = context;
    }

    public void setDatasource(List<Specialty> datasource) {
        this.datasource = datasource;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_specialties_card, parent, false);
        return new ViewHolder(root);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final Specialty specialty = datasource.get(holder.getAdapterPosition());
        holder.circle.setText(specialty.getTitle().substring(0, 1));
        holder.title.setText(specialty.getTitle());
        holder.price.setText(specialty.getFormattedPrice());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, ListDoctorsActivity.class);
                intent.putExtra(ListDoctorsActivity.ARG_SPECIALTY, specialty.getTitle());
                mContext.startActivity(intent);
                ActivityUtils.setAnimationOnClosed(mContext);
            }
        });
    }

    @Override
    public int getItemCount() {
        return datasource.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.circleView)
        TextView circle;
        @BindView(R.id.titleText)
        TextView title;
        @BindView(R.id.priceText)
        TextView price;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}