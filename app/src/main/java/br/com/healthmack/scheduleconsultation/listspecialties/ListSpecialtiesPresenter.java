package br.com.healthmack.scheduleconsultation.listspecialties;

import java.util.List;

import javax.inject.Inject;

import br.com.healthmack.R;
import br.com.healthmack.scheduleconsultation.data.Specialty;
import br.com.healthmack.scheduleconsultation.data.source.ScheduleConsultationDataSource;
import br.com.healthmack.scheduleconsultation.data.source.ScheduleConsultationRepository;

final class ListSpecialtiesPresenter implements ListSpecialties.Presenter, ScheduleConsultationDataSource.ListSpecialtiesCallback {

    private final ScheduleConsultationDataSource mRepository;
    private final ListSpecialties.View mView;

    @Inject
    ListSpecialtiesPresenter(ScheduleConsultationRepository repository, ListSpecialties.View userLoginView) {
        mRepository = repository;
        mView = userLoginView;
    }

    @Inject
    @Override
    public void setupListeners() {
        mView.setPresenter(this);
    }

    @Override
    public void loadSpecialties() {
        mView.showProgressBar();
        mRepository.loadSpecialties(this);
    }

    @Override
    public void onSpecialtiesLoaded(List<Specialty> specialties) {
        mView.hideProgressBar();
        mView.showSpecialties(specialties);
    }

    @Override
    public void onSpecialtiesNotLoaded() {
        mView.showMessage(R.string.error_load_especialties);
    }
}
