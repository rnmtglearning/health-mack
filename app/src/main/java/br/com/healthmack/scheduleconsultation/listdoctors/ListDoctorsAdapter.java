package br.com.healthmack.scheduleconsultation.listdoctors;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import br.com.healthmack.R;
import br.com.healthmack.scheduleconsultation.listschedule.ListScheduleActivity;
import br.com.healthmack.shared.model.User;
import br.com.healthmack.utils.ActivityUtils;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ListDoctorsAdapter extends RecyclerView.Adapter<ListDoctorsAdapter.ViewHolder> {

    private final String mSpecialty;
    private List<User> datasource = new ArrayList<>();
    private Activity mContext;

    public ListDoctorsAdapter(Activity context, String specialty) {
        mContext = context;
        mSpecialty = specialty;
    }

    public void setDatasource(List<User> datasource) {
        this.datasource = datasource;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_doctors_card, parent, false);
        return new ViewHolder(root);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final User doctor = datasource.get(holder.getAdapterPosition());
        holder.circle.setText(doctor.getName().substring(0, 1).toUpperCase());
        holder.title.setText(doctor.getName());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, ListScheduleActivity.class);
                intent.putExtra(ListScheduleActivity.ARG_OTHER_KEY, doctor.getUid());
                intent.putExtra(ListDoctorsActivity.ARG_SPECIALTY, mSpecialty);
                mContext.startActivity(intent);
                ActivityUtils.setAnimationOnClosed(mContext);
            }
        });
    }

    @Override
    public int getItemCount() {
        return datasource.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.circleView)
        TextView circle;
        @BindView(R.id.titleText)
        TextView title;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}