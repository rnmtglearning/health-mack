package br.com.healthmack.scheduleconsultation.listspecialties;

import android.os.Bundle;

import javax.inject.Inject;

import br.com.healthmack.AbstractActivity;
import br.com.healthmack.HealthMackApplication;
import br.com.healthmack.R;
import br.com.healthmack.utils.ActivityUtils;

public class ListSpecialtiesActivity extends AbstractActivity {

    @Inject
    ListSpecialtiesPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ListSpecialtiesFragment listSpecialtiesFragment = ActivityUtils.addFragmentToActivity(this, ListSpecialtiesFragment.class);
        listSpecialtiesFragment.setArguments(getIntent().getExtras());

        DaggerListSpecialtiesComponent.builder()
                .listSpecialtiesPresenterModule(new ListSpecialtiesPresenterModule(listSpecialtiesFragment))
                .scheduleConsultationRepositoryComponent(((HealthMackApplication) getApplication()).getScheduleConsultationRepositoryComponent())
                .build()
                .inject(this);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.default_act;
    }

    @Override
    public String getStringResTitle() {
        return getString(R.string.Specialties);
    }

    @Override
    public Integer getMenuRes() {
        return null;
    }
}