package br.com.healthmack.scheduleconsultation.listdoctors;

import java.util.List;

import javax.inject.Inject;

import br.com.healthmack.R;
import br.com.healthmack.scheduleconsultation.data.source.ScheduleConsultationDataSource;
import br.com.healthmack.scheduleconsultation.data.source.ScheduleConsultationRepository;
import br.com.healthmack.shared.model.User;

final class ListDoctorsPresenter implements ListDoctors.Presenter, ScheduleConsultationDataSource.ListDoctorsCallback {

    private final ScheduleConsultationDataSource mRepository;
    private final ListDoctors.View mView;

    @Inject
    ListDoctorsPresenter(ScheduleConsultationRepository repository, ListDoctors.View userLoginView) {
        mRepository = repository;
        mView = userLoginView;
    }

    @Inject
    @Override
    public void setupListeners() {
        mView.setPresenter(this);
    }

    @Override
    public void loadDoctors(String specialty) {
        mView.showProgressBar();
        mRepository.loadDoctors(specialty, this);
    }

    @Override
    public void onDoctorLoaded(List<User> doctors) {
        mView.hideProgressBar();
        mView.showDoctor(doctors);
    }

    @Override
    public void onDoctorNotLoaded() {
        mView.hideProgressBar();
        mView.showMessage(R.string.error_load_doctor);
    }
}
