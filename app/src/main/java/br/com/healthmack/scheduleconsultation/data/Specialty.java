package br.com.healthmack.scheduleconsultation.data;

public class Specialty {

    private String title;
    private String price;

    public Specialty() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPrice() {
        return price;
    }

    public String getFormattedPrice() {
        return "Preços apartir de R$ " + price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
