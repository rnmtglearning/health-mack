package br.com.healthmack.scheduleconsultation.data.source.remote;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import br.com.healthmack.scheduleconsultation.data.Specialty;
import br.com.healthmack.scheduleconsultation.data.source.ScheduleConsultationDataSource;
import br.com.healthmack.shared.model.User;
import br.com.healthmack.utils.FirebaseChilds;

@Singleton
public class ScheduleConsultationRemoteDataSource implements ScheduleConsultationDataSource {

    private final FirebaseDatabase mDatabase;
    private final FirebaseAuth mAuth;

    @Inject
    public ScheduleConsultationRemoteDataSource(FirebaseDatabase mDatabase, FirebaseAuth firebaseAuth) {
        this.mDatabase = mDatabase;
        this.mAuth = firebaseAuth;
    }

    @Override
    public void loadSpecialties(final ListSpecialtiesCallback listSpecialtiesCallback) {
        mDatabase.getReference()
                .child(FirebaseChilds.CHILD_SPECIALTIES)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        List<Specialty> specialties = new ArrayList<>();
                        for (DataSnapshot snap : dataSnapshot.getChildren()) {
                            Specialty specialty = new Specialty();
                            specialty.setTitle(snap.getKey());
                            specialty.setPrice(snap.getValue().toString());
                            specialties.add(specialty);
                        }
                        if (!specialties.isEmpty())
                            listSpecialtiesCallback.onSpecialtiesLoaded(specialties);
                        else
                            listSpecialtiesCallback.onSpecialtiesNotLoaded();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        listSpecialtiesCallback.onSpecialtiesNotLoaded();
                    }
                });

    }

    @Override
    public void loadDoctors(String specialty, final ListDoctorsCallback listDoctorsCallback) {
        mDatabase.getReference()
                .child(FirebaseChilds.CHILD_SPECIALTIES_DOCTORS)
                .child(specialty)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        List<User> doctors = new ArrayList<User>();
                        for (DataSnapshot snap : dataSnapshot.getChildren()) {
                            User user = new User();
                            user.setName(snap.getValue().toString());
                            user.setUid(snap.getKey());
                            doctors.add(user);
                        }

                        listDoctorsCallback.onDoctorLoaded(doctors);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        listDoctorsCallback.onDoctorNotLoaded();
                    }
                });
    }

    @Override
    public void loadDoctorSchedule(String doctorKey, final ListScheduleCallback listScheduleCallback) {
        mDatabase.getReference()
                .child(FirebaseChilds.CHILD_SCHEDULE)
                .child(doctorKey)
                .child(FirebaseChilds.CHILD_SCHEDULE_HOUR)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.getValue() != null)
                            listScheduleCallback.onScheduleLoaded((List<String>) dataSnapshot.getValue());
                        else
                            listScheduleCallback.onScheduleLoaded(null);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }
}
