package br.com.healthmack.scheduleconsultation.listspecialties;

import java.util.List;

import br.com.healthmack.scheduleconsultation.data.Specialty;
import br.com.healthmack.utils.BasePresenter;
import br.com.healthmack.utils.BaseView;

public interface ListSpecialties {

    interface View extends BaseView<Presenter> {
        void showSpecialties(List<Specialty> specialties);
    }

    interface Presenter extends BasePresenter {
        void loadSpecialties();
    }
}

