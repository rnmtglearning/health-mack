package br.com.healthmack.scheduleconsultation.data.source;

import javax.inject.Inject;
import javax.inject.Singleton;

import static dagger.internal.Preconditions.checkNotNull;

@Singleton
public class ScheduleConsultationRepository implements ScheduleConsultationDataSource {

    private final ScheduleConsultationDataSource dataSource;

    @Inject
    public ScheduleConsultationRepository(ScheduleConsultationDataSource scheduleConsultationDataSource) {
        this.dataSource = scheduleConsultationDataSource;
    }

    @Override
    public void loadSpecialties(ListSpecialtiesCallback listSpecialtiesCallback) {
        checkNotNull(listSpecialtiesCallback);
        dataSource.loadSpecialties(listSpecialtiesCallback);
    }

    @Override
    public void loadDoctors(String specialty, ListDoctorsCallback listDoctorsCallback) {
        checkNotNull(specialty);
        dataSource.loadDoctors(specialty, listDoctorsCallback);
    }

    @Override
    public void loadDoctorSchedule(String doctorKey, ListScheduleCallback listScheduleCallback) {
        checkNotNull(doctorKey);
        dataSource.loadDoctorSchedule(doctorKey, listScheduleCallback);
    }
}
