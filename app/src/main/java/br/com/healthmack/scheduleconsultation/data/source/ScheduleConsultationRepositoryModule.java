package br.com.healthmack.scheduleconsultation.data.source;

import javax.inject.Singleton;

import br.com.healthmack.scheduleconsultation.data.source.remote.ScheduleConsultationRemoteDataSource;
import dagger.Binds;
import dagger.Module;

@Module
public abstract class ScheduleConsultationRepositoryModule {

    @Singleton
    @Binds
    abstract ScheduleConsultationDataSource provideDataSource(ScheduleConsultationRemoteDataSource scheduleConsultationRemoteDataSource);
}
