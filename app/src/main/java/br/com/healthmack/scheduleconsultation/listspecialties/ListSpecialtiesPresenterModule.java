package br.com.healthmack.scheduleconsultation.listspecialties;

import dagger.Module;
import dagger.Provides;

@Module
public class ListSpecialtiesPresenterModule {

    private final ListSpecialties.View mView;

    public ListSpecialtiesPresenterModule(ListSpecialties.View view) {
        mView = view;
    }

    @Provides
    ListSpecialties.View providesView() {
        return mView;
    }
}
