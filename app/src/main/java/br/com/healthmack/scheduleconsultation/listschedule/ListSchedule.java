package br.com.healthmack.scheduleconsultation.listschedule;

import java.util.List;

import br.com.healthmack.utils.BasePresenter;
import br.com.healthmack.utils.BaseView;

public interface ListSchedule {

    interface View extends BaseView<Presenter> {
        void showSchedule(List<String> schedule);

        void showEmptySchedule();
    }

    interface Presenter extends BasePresenter {
        void listDoctorSchedule(String doctorKey);
    }
}
