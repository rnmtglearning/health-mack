package br.com.healthmack.scheduleconsultation.listdoctors;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import br.com.healthmack.AbstractFragment;
import br.com.healthmack.R;
import br.com.healthmack.shared.model.User;
import butterknife.BindView;

public class ListDoctorsFragment extends AbstractFragment<ListDoctors.Presenter> implements ListDoctors.View {

    @BindView(R.id.recycler)
    RecyclerView mRecycler;

    private ListDoctorsAdapter mAdapter;

    @Override
    public int getLayoutRes() {
        return R.layout.simple_list_frag;
    }

    @Override
    public int getProgressBar() {
        return R.id.progressBar;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onCustomCreateView(View view, ViewGroup container) {
        mAdapter = new ListDoctorsAdapter(getActivity(), getArguments().getString(ListDoctorsActivity.ARG_SPECIALTY));
        mPresenter.loadDoctors(getArguments().getString(ListDoctorsActivity.ARG_SPECIALTY));
        mRecycler.setLayoutManager(new GridLayoutManager(getContext(), 3));
        mRecycler.setAdapter(mAdapter);
    }

    @Override
    public void showDoctor(List<User> doctors) {
        mAdapter.setDatasource(doctors);
        mAdapter.notifyDataSetChanged();
    }
}