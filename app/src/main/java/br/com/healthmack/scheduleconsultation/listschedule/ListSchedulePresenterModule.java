package br.com.healthmack.scheduleconsultation.listschedule;

import dagger.Module;
import dagger.Provides;

@Module
public class ListSchedulePresenterModule {

    private final ListSchedule.View mView;

    public ListSchedulePresenterModule(ListSchedule.View view) {
        mView = view;
    }

    @Provides
    ListSchedule.View providesView() {
        return mView;
    }
}
