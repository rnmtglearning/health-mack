package br.com.healthmack.scheduleconsultation.listschedule;

import android.content.Context;

import java.util.List;

import javax.inject.Inject;

import br.com.healthmack.scheduleconsultation.data.source.ScheduleConsultationDataSource;
import br.com.healthmack.scheduleconsultation.data.source.ScheduleConsultationRepository;

final class ListSchedulePresenter implements ListSchedule.Presenter, ScheduleConsultationDataSource.ListScheduleCallback {

    private final ScheduleConsultationRepository mRepository;
    private final ListSchedule.View mView;
    private Context mContext;

    @Inject
    ListSchedulePresenter(ScheduleConsultationRepository repository, ListSchedule.View view, Context context) {
        mRepository = repository;
        mView = view;
        mContext = context;
    }

    @Inject
    @Override
    public void setupListeners() {
        mView.setPresenter(this);
    }

    @Override
    public void listDoctorSchedule(String doctorKey) {
        mView.showProgressBar();
        mRepository.loadDoctorSchedule(doctorKey, this);
    }

    @Override
    public void onScheduleLoaded(List<String> schedule) {
        if (schedule == null || schedule.isEmpty())
            mView.showEmptySchedule();
        else {
            mView.hideProgressBar();
            mView.showSchedule(schedule);
        }
    }
}
