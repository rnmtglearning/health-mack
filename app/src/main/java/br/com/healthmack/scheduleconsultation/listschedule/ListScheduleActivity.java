package br.com.healthmack.scheduleconsultation.listschedule;

import android.os.Bundle;

import javax.inject.Inject;

import br.com.healthmack.AbstractActivity;
import br.com.healthmack.HealthMackApplication;
import br.com.healthmack.R;
import br.com.healthmack.modules.ContextModule;
import br.com.healthmack.utils.ActivityUtils;

public class ListScheduleActivity extends AbstractActivity {

    public static final String ARG_OTHER_KEY = "other-key";

    @Inject
    ListSchedulePresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ListScheduleFragment listScheduleFragment = ActivityUtils.addFragmentToActivity(this, ListScheduleFragment.class);

        listScheduleFragment.setArguments(getIntent().getExtras());
        DaggerListScheduleComponent.builder()
                .contextModule(new ContextModule(getBaseContext()))
                .listSchedulePresenterModule(new ListSchedulePresenterModule(listScheduleFragment))
                .scheduleConsultationRepositoryComponent(((HealthMackApplication) getApplication()).getScheduleConsultationRepositoryComponent())
                .build()
                .inject(this);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.default_act;
    }

    @Override
    public String getStringResTitle() {
        return getString(R.string.Make_Schedule);
    }

    @Override
    public Integer getMenuRes() {
        return null;
    }
}