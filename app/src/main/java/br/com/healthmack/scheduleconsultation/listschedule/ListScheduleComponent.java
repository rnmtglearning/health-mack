package br.com.healthmack.scheduleconsultation.listschedule;

import br.com.healthmack.modules.ContextModule;
import br.com.healthmack.scheduleconsultation.data.source.ScheduleConsultationRepositoryComponent;
import br.com.healthmack.utils.FragmentScoped;
import dagger.Component;

@FragmentScoped
@Component(dependencies = ScheduleConsultationRepositoryComponent.class, modules = {ListSchedulePresenterModule.class,
        ContextModule.class})
public interface ListScheduleComponent {

    void inject(ListScheduleActivity listScheduleActivity);
}
