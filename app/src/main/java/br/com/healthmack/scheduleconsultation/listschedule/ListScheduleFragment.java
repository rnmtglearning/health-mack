package br.com.healthmack.scheduleconsultation.listschedule;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CalendarView;

import com.github.clans.fab.FloatingActionButton;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import br.com.healthmack.AbstractFragment;
import br.com.healthmack.R;
import br.com.healthmack.detailconsultation.DetailActivity;
import br.com.healthmack.scheduleconsultation.listdoctors.ListDoctorsActivity;
import br.com.healthmack.shared.adapter.ListScheduleAdapter;
import br.com.healthmack.utils.ActivityUtils;
import br.com.healthmack.utils.AlertDialogHelper;
import butterknife.BindView;

public class ListScheduleFragment extends AbstractFragment<ListSchedule.Presenter> implements ListSchedule.View, ListScheduleAdapter.OnClickListener {

    @BindView(R.id.calendar)
    CalendarView calendarView;
    @BindView(R.id.recycler)
    RecyclerView recyclerView;

    private ListScheduleAdapter mAdapter;
    private long dateConsultation;

    private String dayWeek;
    private String date;
    private String hour;
    private long dateMilis;

    @Override
    public int getLayoutRes() {
        return R.layout.list_schedule_frag;
    }

    @Override
    public int getProgressBar() {
        return R.id.progressBar;
    }

    @Override
    public void onCustomCreateView(View view, ViewGroup container) {
        calendarView.setDate(System.currentTimeMillis(), true, false);
        calendarView.setMinDate(System.currentTimeMillis());
        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView calendarView, int i, int i1, int i2) {
                String dtStart = i2 + "-" + (i1 + 1) + "-" + i;
                SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy", Locale.ROOT);
                try {
                    dateMilis = format.parse(dtStart).getTime();
                    dayWeek = DateUtils.formatDateTime(getContext(), dateMilis, DateUtils.FORMAT_SHOW_WEEKDAY);
                    date = DateUtils.formatDateTime(getContext(), dateMilis, DateUtils.FORMAT_NUMERIC_DATE);
                    showFabButton();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        });

        mPresenter.listDoctorSchedule(getArguments().getString(ListScheduleActivity.ARG_OTHER_KEY));

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        mAdapter = new ListScheduleAdapter(this, getActivity());
        recyclerView.setAdapter(mAdapter);
        recyclerView.setNestedScrollingEnabled(true);
    }

    @Override
    public void showSchedule(List<String> schedule) {
        mAdapter.setDatasource(schedule);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void showEmptySchedule() {
        AlertDialogHelper.simpleDialogOk(R.string.within_hours, getString(R.string.doctor_whitin_hours), false, getContext(), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                getActivity().finish();
                ActivityUtils.setAnimationOnClosed(getActivity());
            }
        });
    }

    @Override
    public void onClick(String hour) {
        List<String> datasource = mAdapter.getDatasource();
        datasource.clear();
        datasource.add(hour);
        mAdapter.notifyDataSetChanged();
        this.hour = hour;
        showFabButton();
    }

    private void showFabButton() {
        if (!TextUtils.isEmpty(hour) && !TextUtils.isEmpty(date) && !TextUtils.isEmpty(dayWeek)) {
            FloatingActionButton fabButton = ((ListScheduleActivity) getActivity()).fabButton;
            if (fabButton != null) {
                fabButton.setVisibility(View.VISIBLE);
                fabButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_done, getActivity().getTheme()));
                fabButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(getContext(), DetailActivity.class);
                        intent.putExtra(ListDoctorsActivity.ARG_SPECIALTY, getArguments().getSerializable(ListDoctorsActivity.ARG_SPECIALTY));
                        intent.putExtra(ListScheduleActivity.ARG_OTHER_KEY, getArguments().getSerializable(ListScheduleActivity.ARG_OTHER_KEY));
                        intent.putExtra(DetailActivity.ARG_SCHEDULE_HOUR, hour);
                        intent.putExtra(DetailActivity.ARG_SCHEDULE_DAY, dayWeek);
                        intent.putExtra(DetailActivity.ARG_SCHEDULE_DATE, date);
                        intent.putExtra(DetailActivity.ARG_SCHEDULE_DATE_MILIS, dateMilis);
                        intent.putExtra(DetailActivity.ARG_IS_CONFIRM, true);

                        getActivity().startActivity(intent);
                        ActivityUtils.setAnimationOnClosed(getActivity());
                    }
                });
            }
        }
    }
}