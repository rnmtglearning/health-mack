package br.com.healthmack.listconsultations.source;

import br.com.healthmack.shared.model.Consultation;

public interface ListConsultationsDataSource {

    void loadConsultaions(ListConsultationsCallback callback);

    interface ListConsultationsCallback {
        void onConsultationLoaded(Consultation consultation);

        void onConsultationRemoved(Consultation consultation);
    }
}
