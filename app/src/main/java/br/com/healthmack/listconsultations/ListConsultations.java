package br.com.healthmack.listconsultations;

import br.com.healthmack.shared.model.Consultation;
import br.com.healthmack.shared.preferences.HealthMackPreferences;
import br.com.healthmack.shared.preferences.UserType;
import br.com.healthmack.utils.BasePresenter;
import br.com.healthmack.utils.BaseView;

public interface ListConsultations {

    interface View extends BaseView<Presenter> {

        void setUserType(UserType user);

        void showConsultation(Consultation consultation);

        void removeConsultation(Consultation consultation);
    }

    interface Presenter extends BasePresenter {

        void setupUserType(HealthMackPreferences preferences);

        void loadConsultations();
    }
}
