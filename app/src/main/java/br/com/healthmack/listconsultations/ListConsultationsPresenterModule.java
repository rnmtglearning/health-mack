package br.com.healthmack.listconsultations;

import dagger.Module;
import dagger.Provides;

@Module
public class ListConsultationsPresenterModule {

    private final ListConsultations.View mView;

    public ListConsultationsPresenterModule(ListConsultations.View view) {
        mView = view;
    }

    @Provides
    ListConsultations.View providesView() {
        return mView;
    }
}
