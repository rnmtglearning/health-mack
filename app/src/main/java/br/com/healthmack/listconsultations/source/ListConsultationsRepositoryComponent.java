package br.com.healthmack.listconsultations.source;

import javax.inject.Singleton;

import br.com.healthmack.modules.FirebaseAuthModule;
import br.com.healthmack.modules.FirebaseDatabaseModule;
import dagger.Component;

@Singleton
@Component(modules = {ListConsultationsRepositoryModule.class, FirebaseDatabaseModule.class, FirebaseAuthModule.class})
public interface ListConsultationsRepositoryComponent {

    ListConsultationsRepository getRepository();
}
