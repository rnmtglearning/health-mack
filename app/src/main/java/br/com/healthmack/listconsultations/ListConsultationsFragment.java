package br.com.healthmack.listconsultations;

import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import java.util.Collections;
import java.util.List;

import br.com.healthmack.AbstractFragment;
import br.com.healthmack.R;
import br.com.healthmack.detailconsultation.DetailActivity;
import br.com.healthmack.shared.model.Consultation;
import br.com.healthmack.scheduleconsultation.listdoctors.ListDoctorsActivity;
import br.com.healthmack.scheduleconsultation.listschedule.ListScheduleActivity;
import br.com.healthmack.shared.preferences.UserType;
import br.com.healthmack.utils.ActivityUtils;
import butterknife.BindView;

public class ListConsultationsFragment extends AbstractFragment<ListConsultations.Presenter> implements ListConsultations.View {

    @BindView(R.id.recycler)
    RecyclerView mRecyclerView;
    private ListConsultationsAdapter mAdapter;
    private UserType mUserType;

    private ListConsultationsAdapter.ClickListener onClickListener = new ListConsultationsAdapter.ClickListener() {
        @Override
        public void onClick(Consultation consultation) {
            Intent intent = new Intent(getContext(), DetailActivity.class);
            intent.putExtra(ListDoctorsActivity.ARG_SPECIALTY, consultation.getSpecialty());
            intent.putExtra(ListScheduleActivity.ARG_OTHER_KEY, consultation.getOtherKey());
            intent.putExtra(DetailActivity.ARG_SCHEDULE_HOUR, consultation.getHour());
            intent.putExtra(DetailActivity.ARG_SCHEDULE_DAY, consultation.getDay());
            intent.putExtra(DetailActivity.ARG_SCHEDULE_DATE, consultation.getDate());
            intent.putExtra(DetailActivity.ARG_SCHEDULE_DATE_MILIS, consultation.getDateMillis());
            intent.putExtra(DetailActivity.ARG_IS_CONFIRM, false);
            intent.putExtra(DetailActivity.ARG_CONSULTATION_KEY, consultation.getKey());

            getActivity().startActivity(intent);
            ActivityUtils.setAnimationOnClosed(getActivity());
        }
    };

    @Override
    public int getLayoutRes() {
        return R.layout.simple_list_frag;
    }

    @Override
    public int getProgressBar() {
        return R.id.progressBar;
    }

    @Override
    public void onCustomCreateView(View view, ViewGroup container) {
        mPresenter.loadConsultations();
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        mAdapter = new ListConsultationsAdapter(getContext(), onClickListener);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void setUserType(UserType user) {
        mUserType = user;
    }

    @Override
    public void showConsultation(Consultation consultation) {
        List<Consultation> datasource = mAdapter.getDatasource();
        datasource.add(consultation);
        Collections.sort(datasource);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void removeConsultation(Consultation consultation) {
        List<Consultation> datasource = mAdapter.getDatasource();
        for (Consultation value : datasource) {
            if (value.getKey().equals(consultation.getKey()))
                datasource.remove(value);
        }

        Collections.sort(datasource);
        mAdapter.notifyDataSetChanged();
    }
}