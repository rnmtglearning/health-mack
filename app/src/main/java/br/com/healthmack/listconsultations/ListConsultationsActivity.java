package br.com.healthmack.listconsultations;

import android.os.Bundle;

import javax.inject.Inject;

import br.com.healthmack.AbstractActivity;
import br.com.healthmack.HealthMackApplication;
import br.com.healthmack.R;
import br.com.healthmack.modules.ContextModule;
import br.com.healthmack.modules.PreferencesModule;
import br.com.healthmack.utils.ActivityUtils;

public class ListConsultationsActivity extends AbstractActivity {

    @Inject
    ListConsultationsPresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ListConsultationsFragment listConsultationsFragment = ActivityUtils.addFragmentToActivity(this, ListConsultationsFragment.class);

        DaggerListConsultationsComponent.builder()
                .contextModule(new ContextModule(getBaseContext()))
                .listConsultationsPresenterModule(new ListConsultationsPresenterModule(listConsultationsFragment))
                .listConsultationsRepositoryComponent(((HealthMackApplication) getApplication()).getListConsultationsRepositoryComponent())
                .preferencesModule(new PreferencesModule(getBaseContext()))
                .build()
                .inject(this);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.default_act;
    }

    @Override
    public String getStringResTitle() {
        return getString(R.string.Consultations);
    }

    @Override
    public Integer getMenuRes() {
        return null;
    }
}