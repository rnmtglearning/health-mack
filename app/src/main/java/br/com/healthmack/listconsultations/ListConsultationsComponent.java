package br.com.healthmack.listconsultations;

import br.com.healthmack.listconsultations.source.ListConsultationsRepositoryComponent;
import br.com.healthmack.modules.ContextModule;
import br.com.healthmack.modules.PreferencesModule;
import br.com.healthmack.utils.FragmentScoped;
import dagger.Component;

@FragmentScoped
@Component(dependencies = ListConsultationsRepositoryComponent.class, modules = {ListConsultationsPresenterModule.class,
        ContextModule.class,
        PreferencesModule.class})
public interface ListConsultationsComponent {

    void inject(ListConsultationsActivity listConsultationsActivity);
}
