package br.com.healthmack.listconsultations.source;

import javax.inject.Singleton;

import br.com.healthmack.listconsultations.source.remote.ListConsultationsRemoteDataSource;
import dagger.Binds;
import dagger.Module;

@Module
public abstract class ListConsultationsRepositoryModule {

    @Singleton
    @Binds
    abstract ListConsultationsDataSource provideDataSource(ListConsultationsRemoteDataSource remoteDataSource);
}
