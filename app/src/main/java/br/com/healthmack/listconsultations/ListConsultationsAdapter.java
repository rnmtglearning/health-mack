package br.com.healthmack.listconsultations;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import br.com.healthmack.R;
import br.com.healthmack.shared.model.Consultation;
import br.com.healthmack.utils.FirebaseChilds;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ListConsultationsAdapter extends RecyclerView.Adapter<ListConsultationsAdapter.ViewHolder> {

    private List<Consultation> datasource = new ArrayList<>();
    private Context mContext;
    private ClickListener mListener;

    public ListConsultationsAdapter(Context context, ClickListener listener) {
        this.mContext = context;
        this.mListener = listener;
    }

    public List<Consultation> getDatasource() {
        return datasource;
    }

    public void setDatasource(List<Consultation> datasource) {
        this.datasource = datasource;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_consultations_card, parent, false);
        return new ViewHolder(root);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        int mPos = holder.getAdapterPosition();
        final Consultation consultation = datasource.get(mPos);
        holder.specialty.setText(consultation.getSpecialty());
        holder.date.setText(String.format(mContext.getString(R.string.schedule_date), consultation.getInverseDate(), FirebaseChilds.translateDay(consultation.getDay().toLowerCase(), mContext)));
        holder.hour.setText(consultation.getHour());
        holder.price.setText(String.format(mContext.getString(R.string.format_price), consultation.getPrice()));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.onClick(consultation);
            }
        });
    }

    @Override
    public int getItemCount() {
        return datasource != null ? datasource.size() : 0;
    }

    interface ClickListener {
        void onClick(Consultation consultation);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.specialtyText)
        TextView specialty;
        @BindView(R.id.dateText)
        TextView date;
        @BindView(R.id.hourText)
        TextView hour;
        @BindView(R.id.priceText)
        TextView price;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
