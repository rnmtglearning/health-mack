package br.com.healthmack.listconsultations.source.remote;

import android.text.TextUtils;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;

import javax.inject.Inject;
import javax.inject.Singleton;

import br.com.healthmack.listconsultations.source.ListConsultationsDataSource;
import br.com.healthmack.shared.model.Consultation;
import br.com.healthmack.utils.FirebaseChilds;

@Singleton
public class ListConsultationsRemoteDataSource implements ListConsultationsDataSource {

    private final FirebaseDatabase mDatabase;
    private final FirebaseAuth mAuth;

    @Inject
    public ListConsultationsRemoteDataSource(FirebaseDatabase mDatabase, FirebaseAuth auth) {
        this.mDatabase = mDatabase;
        this.mAuth = auth;
    }

    @Override
    public void loadConsultaions(final ListConsultationsCallback callback) {
        if (mAuth.getCurrentUser() != null)
            mDatabase.getReference(FirebaseChilds.CHILD_CONSULTATIONS)
                    .child(mAuth.getCurrentUser().getUid())
                    .addChildEventListener(new ChildEventListener() {
                        @Override
                        public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                            for (DataSnapshot snap : dataSnapshot.getChildren()) {
                                for (DataSnapshot snap1 : snap.getChildren()) {
                                    Consultation value = snap1.getValue(Consultation.class);
                                    value.setKey(snap1.getKey());
                                    if (!TextUtils.isEmpty(value.getDate())) {
                                        snap1.getValue(Consultation.class);
                                        callback.onConsultationLoaded(value);
                                    } else {
                                        value = snap1.getChildren().iterator().next().getValue(Consultation.class);
                                        value.setKey(snap1.getChildren().iterator().next().getKey());
                                        if (!TextUtils.isEmpty(value.getDate()))
                                            callback.onConsultationLoaded(value);
                                    }
                                }
                            }

                        }

                        @Override
                        public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                        }

                        @Override
                        public void onChildRemoved(DataSnapshot dataSnapshot) {
                            Consultation value = dataSnapshot.getChildren().iterator().next().getChildren().iterator().next().getValue(Consultation.class);
                            value.setKey(dataSnapshot.getChildren().iterator().next().getChildren().iterator().next().getKey());
                            if (!TextUtils.isEmpty(value.getDate())) {
                                dataSnapshot.getValue(Consultation.class);
                                callback.onConsultationRemoved(value);
                            } else {
                                value = dataSnapshot.getChildren().iterator().next().getChildren().iterator().next().getChildren().iterator().next().getValue(Consultation.class);
                                value.setKey(dataSnapshot.getChildren().iterator().next().getChildren().iterator().next().getChildren().iterator().next().getKey());
                                if (!TextUtils.isEmpty(value.getDate()))
                                    callback.onConsultationRemoved(value);
                            }
                        }

                        @Override
                        public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
    }
}
