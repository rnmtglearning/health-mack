package br.com.healthmack.listconsultations;

import android.content.Context;

import javax.inject.Inject;

import br.com.healthmack.listconsultations.source.ListConsultationsDataSource;
import br.com.healthmack.listconsultations.source.ListConsultationsRepository;
import br.com.healthmack.shared.model.Consultation;
import br.com.healthmack.shared.preferences.HealthMackPreferences;

final class ListConsultationsPresenter implements ListConsultations.Presenter, ListConsultationsDataSource.ListConsultationsCallback {

    private final ListConsultationsRepository mRepository;
    private final ListConsultations.View mView;
    private Context mContext;

    @Inject
    ListConsultationsPresenter(ListConsultationsRepository repository, ListConsultations.View view, Context context) {
        mRepository = repository;
        mView = view;
        mContext = context;
    }

    @Inject
    @Override
    public void setupListeners() {
        mView.setPresenter(this);
    }

    @Inject
    @Override
    public void setupUserType(HealthMackPreferences preferences) {
        mView.setUserType(preferences.getUserType());
    }

    @Override
    public void loadConsultations() {
        mView.showProgressBar();
        mRepository.loadConsultaions(this);
    }

    @Override
    public void onConsultationLoaded(Consultation consultation) {
        mView.hideProgressBar();
        mView.showConsultation(consultation);
    }

    @Override
    public void onConsultationRemoved(Consultation consultation) {
        mView.hideProgressBar();
        mView.removeConsultation(consultation);
    }
}
