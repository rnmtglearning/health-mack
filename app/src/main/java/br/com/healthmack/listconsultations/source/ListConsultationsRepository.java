package br.com.healthmack.listconsultations.source;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class ListConsultationsRepository implements ListConsultationsDataSource {

    private final ListConsultationsDataSource dataSource;

    @Inject
    public ListConsultationsRepository(ListConsultationsDataSource listConsultationsDataSource) {
        this.dataSource = listConsultationsDataSource;
    }

    @Override
    public void loadConsultaions(ListConsultationsCallback callback) {
        dataSource.loadConsultaions(callback);
    }
}