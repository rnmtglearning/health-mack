package br.com.healthmack.menu;

import br.com.healthmack.utils.BasePresenter;
import br.com.healthmack.utils.BaseView;

public interface Menu {

    interface View extends BaseView<Presenter> {
        void showMenuDoctor();

        void showMenuUser();

        void makeLogout();
    }

    interface Presenter extends BasePresenter {
        void loadUserType();
    }
}
