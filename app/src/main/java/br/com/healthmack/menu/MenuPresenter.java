package br.com.healthmack.menu;

import android.content.Context;

import javax.inject.Inject;

import br.com.healthmack.menu.source.MenuDataSource;
import br.com.healthmack.menu.source.MenuRepository;
import br.com.healthmack.shared.preferences.HealthMackPreferences;
import br.com.healthmack.shared.preferences.UserType;

final class MenuPresenter implements Menu.Presenter, MenuDataSource.MenuCallback {

    private final MenuRepository mRepository;
    private final Menu.View mView;
    private Context mContext;
    private HealthMackPreferences mPreferences;

    @Inject
    MenuPresenter(MenuRepository repository, Menu.View view, Context context, HealthMackPreferences preferences) {
        mRepository = repository;
        mView = view;
        mContext = context;
        mPreferences = preferences;
    }

    @Inject
    @Override
    public void setupListeners() {
        mView.setPresenter(this);
    }

    @Override
    public void loadUserType() {
        mView.showProgressBar();
        mRepository.loadUserType(this);
    }

    @Override
    public void onTypeLoaded(boolean isDoctor) {
        mView.hideProgressBar();
        mPreferences.setPrefUserType(isDoctor ? UserType.DOCTOR : UserType.USER);

        if (isDoctor)
            mView.showMenuDoctor();
        else
            mView.showMenuUser();
    }

    @Override
    public void onTypeNotLoaded() {
        mView.hideProgressBar();
        mView.makeLogout();
    }
}
