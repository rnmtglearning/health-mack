package br.com.healthmack.menu;

import dagger.Module;
import dagger.Provides;

@Module
public class MenuPresenterModule {

    private final Menu.View mView;

    public MenuPresenterModule(Menu.View view) {
        mView = view;
    }

    @Provides
    Menu.View providesView() {
        return mView;
    }
}
