package br.com.healthmack.menu;

import br.com.healthmack.menu.source.MenuRepositoryComponent;
import br.com.healthmack.modules.ContextModule;
import br.com.healthmack.modules.PreferencesModule;
import br.com.healthmack.sample.MakeScheduleActivity;
import br.com.healthmack.utils.FragmentScoped;
import dagger.Component;

@FragmentScoped
@Component(dependencies = MenuRepositoryComponent.class, modules = {MenuPresenterModule.class,
        ContextModule.class,
        PreferencesModule.class})
public interface MenuComponent {

    void inject(MenuActivity activity);
}
