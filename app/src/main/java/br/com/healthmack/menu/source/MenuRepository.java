package br.com.healthmack.menu.source;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class MenuRepository implements MenuDataSource {

    private final MenuDataSource mMenuDataSource;

    @Inject
    public MenuRepository(MenuDataSource menuDataSource) {
        this.mMenuDataSource = menuDataSource;
    }

    @Override
    public void loadUserType(MenuCallback callback) {
        mMenuDataSource.loadUserType(callback);
    }
}
