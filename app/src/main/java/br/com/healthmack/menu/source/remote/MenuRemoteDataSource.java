package br.com.healthmack.menu.source.remote;

import android.text.TextUtils;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import javax.inject.Inject;
import javax.inject.Singleton;

import br.com.healthmack.menu.source.MenuDataSource;
import br.com.healthmack.utils.FirebaseChilds;

@Singleton
public class MenuRemoteDataSource implements MenuDataSource {

    private final FirebaseDatabase mDatabase;
    private final FirebaseAuth mAuth;

    @Inject
    public MenuRemoteDataSource(FirebaseDatabase mDatabase, FirebaseAuth auth) {
        this.mDatabase = mDatabase;
        this.mAuth = auth;
    }

    @Override
    public void loadUserType(final MenuCallback callback) {
        if (mAuth.getCurrentUser() != null && !TextUtils.isEmpty(mAuth.getCurrentUser().getUid())) {
            mDatabase.getReference()
                    .child(FirebaseChilds.CHILD_USER)
                    .child(mAuth.getCurrentUser().getUid())
                    .child(FirebaseChilds.CHILD_USER_DOCTOR)
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            callback.onTypeLoaded(dataSnapshot.getValue(Boolean.class));
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            callback.onTypeNotLoaded();
                        }
                    });
        } else
            callback.onTypeNotLoaded();
    }
}
