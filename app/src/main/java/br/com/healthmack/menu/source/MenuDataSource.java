package br.com.healthmack.menu.source;

public interface MenuDataSource {

    void loadUserType(MenuCallback callback);

    interface MenuCallback {
        void onTypeLoaded(boolean isDoctor);

        void onTypeNotLoaded();
    }
}
