package br.com.healthmack.menu.source;

import javax.inject.Singleton;

import br.com.healthmack.modules.FirebaseAuthModule;
import br.com.healthmack.modules.FirebaseDatabaseModule;
import dagger.Component;

@Singleton
@Component(modules = {MenuRepositoryModule.class, FirebaseDatabaseModule.class, FirebaseAuthModule.class})
public interface MenuRepositoryComponent {

    MenuRepository getRepository();
}
