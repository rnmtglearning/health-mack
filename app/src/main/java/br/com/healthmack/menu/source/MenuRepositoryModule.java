package br.com.healthmack.menu.source;

import javax.inject.Singleton;

import br.com.healthmack.menu.source.remote.MenuRemoteDataSource;
import dagger.Binds;
import dagger.Module;

@Module
public abstract class MenuRepositoryModule {

    @Singleton
    @Binds
    abstract MenuDataSource provideDataSource(MenuRemoteDataSource remoteDataSource);
}
