package br.com.healthmack.menu;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.LinearLayout;

import com.google.firebase.auth.FirebaseAuth;

import br.com.healthmack.AbstractFragment;
import br.com.healthmack.R;
import br.com.healthmack.addsignature.AddSignatureActivity;
import br.com.healthmack.listconsultations.ListConsultationsActivity;
import br.com.healthmack.makeschedule.MakeScheduleActivity;
import br.com.healthmack.scheduleconsultation.listspecialties.ListSpecialtiesActivity;
import br.com.healthmack.splashscreen.SplashActivity;
import br.com.healthmack.useraccount.userlogin.UserLoginActivity;
import br.com.healthmack.utils.ActivityUtils;
import br.com.healthmack.utils.AnimUtils;
import br.com.healthmack.utils.SnackbarHelper;
import butterknife.BindView;

import static br.com.healthmack.menu.Menu.Presenter;

public class MenuFragment extends AbstractFragment<Presenter> implements br.com.healthmack.menu.Menu.View {

    @BindView(R.id.scheduleConsultant)
    Button scheduleConsultant;
    @BindView(R.id.makeSchedule)
    Button makeSchedule;
    @BindView(R.id.viewSchedule)
    Button viewSchedule;

    @BindView(R.id.mainLayout)
    LinearLayout mainLayout;

    private Animation animation;
    private MenuItem signatureItem;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        super.onCreate(savedInstanceState);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.menu_frag;
    }

    @Override
    public int getProgressBar() {
        return R.id.progressBar;
    }

    @Override
    public void onCustomCreateView(View view, ViewGroup container) {
        animation = AnimUtils.makeAnimation(getContext(), R.anim.move, null);

        scheduleConsultant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), ListSpecialtiesActivity.class));
                ActivityUtils.setAnimationOnClosed(getActivity());
            }
        });

        makeSchedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), MakeScheduleActivity.class));
                ActivityUtils.setAnimationOnClosed(getActivity());
            }
        });

        viewSchedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), ListConsultationsActivity.class));
                ActivityUtils.setAnimationOnClosed(getActivity());
            }
        });

        scheduleConsultant.setVisibility(View.GONE);
        makeSchedule.setVisibility(View.GONE);
        viewSchedule.setVisibility(View.GONE);

        mPresenter.loadUserType();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main, menu);
        signatureItem = menu.findItem(R.id.menu_main_signature);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();

        if (itemId == R.id.menu_main_logout) {
            FirebaseAuth.getInstance().signOut();
            startActivity(new Intent(getActivity(), SplashActivity.class));
            ActivityUtils.setAnimationOnClosed(this);
            return true;
        } else if (itemId == R.id.menu_main_signature) {
            startActivity(new Intent(getActivity(), AddSignatureActivity.class));
            ActivityUtils.setAnimationOnClosed(this);
            return true;
        }
        return false;
    }

    @Override
    public void showMenuDoctor() {
        makeSchedule.setVisibility(View.VISIBLE);
        viewSchedule.setVisibility(View.VISIBLE);
        mainLayout.startAnimation(animation);
    }

    @Override
    public void showMenuUser() {
        scheduleConsultant.setVisibility(View.VISIBLE);
        viewSchedule.setVisibility(View.VISIBLE);
        mainLayout.startAnimation(animation);
        if (signatureItem != null)
            signatureItem.setVisible(false);
    }

    @Override
    public void makeLogout() {
        SnackbarHelper.show(getActivity(), R.id.mainLayout, R.string.error_load_user,
                Snackbar.LENGTH_INDEFINITE, android.R.string.ok, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        FirebaseAuth.getInstance().signOut();

                        startActivity(new Intent(getActivity(), UserLoginActivity.class));
                        ActivityUtils.setAnimationOnClosed(MenuFragment.this);
                    }
                });
    }
}
