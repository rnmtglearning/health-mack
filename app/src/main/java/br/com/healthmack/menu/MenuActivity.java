package br.com.healthmack.menu;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;

import javax.inject.Inject;

import br.com.healthmack.AbstractActivity;
import br.com.healthmack.HealthMackApplication;
import br.com.healthmack.R;
import br.com.healthmack.modules.ContextModule;
import br.com.healthmack.modules.PreferencesModule;
import br.com.healthmack.utils.ActivityUtils;

public class MenuActivity extends AbstractActivity {

    @Inject
    MenuPresenter presenter;

    @Override
    public int getLayoutRes() {
        return R.layout.default_act;
    }

    @Override
    public String getStringResTitle() {
        return getString(R.string.app_name);
    }

    @Override
    public Integer getMenuRes() {
        return null;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        MenuFragment menuFragment = ActivityUtils.addFragmentToActivity(this, MenuFragment.class);

        DaggerMenuComponent.builder()
                .menuRepositoryComponent(((HealthMackApplication) getApplication()).getMenuRepositoryComponent())
                .menuPresenterModule(new MenuPresenterModule(menuFragment))
                .contextModule(new ContextModule(getBaseContext()))
                .preferencesModule(new PreferencesModule(getBaseContext()))
                .build()
                .inject(this);

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleMarginStart(50);

        ActionBar bar = getSupportActionBar();
        if (bar != null)
            bar.setDisplayHomeAsUpEnabled(false);
    }
}
