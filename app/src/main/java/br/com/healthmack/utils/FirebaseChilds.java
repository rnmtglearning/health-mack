package br.com.healthmack.utils;

import android.content.Context;

import br.com.healthmack.R;

public class FirebaseChilds {
    public static final String CHILD_USER = "users";
    public static final String CHILD_USER_DOCTOR = "doctor";
    public static final String CHILD_CLINIC = "clinic";
    public static final String CHILD_SPECIALTIES = "specialties";
    public static final String CHILD_SCHEDULE = "schedule-doctors";
    public static final String CHILD_SCHEDULE_DAY = "day";
    public static final String CHILD_SCHEDULE_HOUR = "hour";
    public static final String CHILD_SPECIALTIES_DOCTORS = "specialties-doctors";
    public static final String CHILD_CONSULTATIONS = "consultations";
    public static final String CHILD_MEDICAL_PRESCRIPTIONS = "prescriptions";
    public static final String CHILD_SIGNATURE = "signature";
    public static final String CHILD_ATTESTATION_DAY = "attestation";

    public static final String CHILD_SCHEDULE_MONDAY = "monday";
    public static final String CHILD_SCHEDULE_FRIDAY = "friday";
    public static final String CHILD_SCHEDULE_SUNDAY = "sunday";
    public static final String CHILD_SCHEDULE_TUESDAY = "tuesday";
    public static final String CHILD_SCHEDULE_WEDNESDAY = "wednesday";
    public static final String CHILD_SCHEDULE_THURSDAY = "thursday";
    public static final String CHILD_SCHEDULE_SATURDAY = "saturday";

    public static String translateDay(String day, Context context) {
        switch (day) {
            case FirebaseChilds.CHILD_SCHEDULE_MONDAY:
                return context.getString(R.string.Monday);
            case FirebaseChilds.CHILD_SCHEDULE_TUESDAY:
                return context.getString(R.string.Tuesday);
            case FirebaseChilds.CHILD_SCHEDULE_WEDNESDAY:
                return context.getString(R.string.Wednesday);
            case FirebaseChilds.CHILD_SCHEDULE_THURSDAY:
                return context.getString(R.string.Thursday);
            case FirebaseChilds.CHILD_SCHEDULE_FRIDAY:
                return context.getString(R.string.Friday);
            case FirebaseChilds.CHILD_SCHEDULE_SATURDAY:
                return context.getString(R.string.Saturday);
            case FirebaseChilds.CHILD_SCHEDULE_SUNDAY:
                return context.getString(R.string.Sunday);
            default:
                return "";
        }
    }
}
