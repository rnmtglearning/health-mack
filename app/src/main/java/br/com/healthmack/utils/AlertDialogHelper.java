package br.com.healthmack.utils;

import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.StringRes;
import android.support.v7.app.AlertDialog;

import java.util.List;

import br.com.healthmack.R;

public class AlertDialogHelper {

    /**
     * A listener the dismiss the dialog on button click
     */
    private static DialogInterface.OnClickListener simpleListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            dialog.dismiss();
        }
    };

    private static AlertDialog createDialog(Context context) {
        return new AlertDialog.Builder(context, R.style.AlertDialogTheme).create();
    }

    private static AlertDialog.Builder createBuilder(Context context) {
        return new AlertDialog.Builder(context, R.style.AlertDialogTheme);
    }

    /**
     * Dialog with values to multiple choice.
     *
     * @param context     the context
     * @param titleId     the title id
     * @param itens       the itens
     * @param listener    the listener
     * @param allListener the all listener
     */
    public static void multiChoiseDialog(Context context, @StringRes int titleId, final List<String> itens,
                                         final List<Boolean> selected, DialogInterface.OnMultiChoiceClickListener listener, final SelectAllOptionsListener allListener) {
        CharSequence[] mItens = new CharSequence[itens.size()];

        for (int i = 0; i < itens.size(); i++)
            mItens[i] = itens.get(i);

        boolean[] mSelected = new boolean[selected.size()];

        for (int i = 0; i < selected.size(); i++)
            mSelected[i] = selected.get(i);

        final AlertDialog.Builder alertBuilder = createBuilder(context);
        alertBuilder.setTitle(titleId);
        alertBuilder.setMultiChoiceItems(mItens, mSelected.length == 0 ? null : mSelected, listener);
        alertBuilder.setCancelable(true);
        alertBuilder.setPositiveButton(android.R.string.ok, simpleListener);
        if (allListener != null) {
            alertBuilder.setNeutralButton(R.string.select_all, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    allListener.onClick(itens);
                }
            });
        }
        alertBuilder.create().show();
    }

    public static void simpleDialogOk(@StringRes int title, String message, boolean cancel, Context context, DialogInterface.OnClickListener listener) {
        final AlertDialog alertDialog = createDialog(context);
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, context.getString(android.R.string.ok), listener != null ? listener : simpleListener);
        alertDialog.show();
    }

    /**
     * The interface Select all options listener.
     */
    public interface SelectAllOptionsListener {

        /**
         * On click.
         *
         * @param itens the itens
         */
        void onClick(List<String> itens);
    }
}