package br.com.healthmack.utils;

import android.support.annotation.StringRes;

public interface BaseView<T> {

    void setPresenter(T presenter);

    void showProgressBar();

    void hideProgressBar();

    void showCommunicationError();

    void showMessage(@StringRes int messageId);
}
