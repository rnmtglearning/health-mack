package br.com.healthmack.utils;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.widget.Toast;

public class ToastHelper {

    public static void show(Fragment fragment, String text, int duration) {
        Toast.makeText(fragment.getContext(), text, duration).show();
    }

    public static void show(Fragment fragment, int text, int duration) {
        Toast.makeText(fragment.getContext(), text, duration).show();
    }

    public static void show(Context context, String text, int duration) {
        Toast.makeText(context, text, duration).show();
    }

    public static void show(Context context, int text, int duration) {
        Toast.makeText(context, text, duration).show();
    }
}