package br.com.healthmack.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidateUtils {

    public static boolean checkIsEmail(String... emails) {
        final String emailPattern = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        for (int i = 0; i < emails.length; i++) {
            Pattern pattern = Pattern.compile(emailPattern);
            String email = emails[i];
            Matcher matcher = pattern.matcher(email);
            if (!matcher.matches()) {
                return false;
            }
        }
        return true;
    }
}
