package br.com.healthmack.utils;

import android.app.Activity;
import android.graphics.Color;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.TextView;

import br.com.healthmack.R;


public class SnackbarHelper {

    public static void show(Activity activity, @IdRes int layoutId, @StringRes int messageId, @NonNull int duration, @StringRes int actionMessage, final View.OnClickListener callback) {
        Snackbar snackbar = Snackbar.make(activity.findViewById(layoutId), activity.getString(messageId), duration);
        if (actionMessage != 0 && callback != null) {
            snackbar.setAction(actionMessage, callback);
            snackbar.setActionTextColor(ContextCompat.getColor(activity, R.color.colorPrimary));
        }

        TextView textView = (TextView) snackbar.getView().findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.WHITE);
        snackbar.show();
    }

    public static void disableFields(View view) {
        for (View mView : view.getTouchables()) {
            mView.setFocusable(false);
            mView.setClickable(false);
            mView.setEnabled(false);
        }
    }

    public static void enableFields(View view) {
        for (View mView : view.getTouchables()) {
            mView.setFocusable(true);
            mView.setClickable(true);
            mView.setEnabled(true);
        }
    }
}
