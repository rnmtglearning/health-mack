package br.com.healthmack.utils;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import br.com.healthmack.R;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * This provides methods to help Activities load their UI.
 */
public class ActivityUtils {

    /**
     * The {@code fragment} is added to the container view with id {@code frameId}. The operation is
     * performed by the {@code fragmentManager}.
     *
     */
    public static void addFragmentToActivity(@NonNull FragmentManager fragmentManager,
                                             @NonNull Fragment fragment, int frameId) {
        checkNotNull(fragmentManager);
        checkNotNull(fragment);
        if (!fragment.isAdded()) {
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.add(frameId, fragment);
            transaction.commit();
        }
    }

    public static <T>T addFragmentToActivity(@NonNull FragmentActivity activity, @NonNull Class<T> clazz) {
        FragmentManager fragmentManager = activity.getSupportFragmentManager();
        Fragment fragment = fragmentManager.findFragmentById(R.id.contentFrame);

        if (fragment == null)
            fragment = Fragment.instantiate(activity, clazz.getName());

        checkNotNull(fragmentManager);
        checkNotNull(fragment);
        if (!fragment.isAdded()) {
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.add(R.id.contentFrame, fragment);
            transaction.commit();
        }
        return (T) fragment;
    }

    public static void setAnimationOnClosed(Activity activity) {
        activity.overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }

    public static void setAnimationOnClosed(Fragment fragment) {
        fragment.getActivity().overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }

    public static void setAnimationOnClosed(Fragment fragment, int animIn) {
        fragment.getActivity().overridePendingTransition(animIn, R.anim.push_left_out);
    }

    public static boolean isTablet(Context ctx){
        return (ctx.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_LARGE;
    }

}