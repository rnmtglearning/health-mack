
package br.com.healthmack.utils;

/**
 * The Base Presenter Interface is responsible for inject the Presenter to View (a.k.a. Fragment)
 *
 * @author tiago.aguiar @moving.com.br (Tiago Aguiar).
 */
public interface BasePresenter {

    /**
     * All Presenter must implement this method and annotate it with {@link javax.inject.Inject}.
     */
    void setupListeners();
}
