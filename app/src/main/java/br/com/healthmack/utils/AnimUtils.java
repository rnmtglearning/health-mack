package br.com.healthmack.utils;

import android.content.Context;
import android.support.annotation.AnimRes;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

public class AnimUtils {

    private static Animation.AnimationListener simpleListener = new Animation.AnimationListener() {
        @Override
        public void onAnimationStart(Animation animation) {

        }

        @Override
        public void onAnimationEnd(Animation animation) {

        }

        @Override
        public void onAnimationRepeat(Animation animation) {

        }
    };

    public static Animation makeAnimation(Context context, @AnimRes int anim, Animation.AnimationListener listener) {
        Animation animation = AnimationUtils.loadAnimation(context, anim);
        if (listener != null)
            animation.setAnimationListener(listener);
        else
            animation.setAnimationListener(simpleListener);

        return animation;
    }
}
