package br.com.healthmack;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.github.clans.fab.FloatingActionButton;

import br.com.healthmack.utils.ActivityUtils;

/**
 * This is class is responsible for configuring your children with layouts, screen orientation
 * {@link ActivityInfo#SCREEN_ORIENTATION_PORTRAIT} and {@link ActivityInfo#SCREEN_ORIENTATION_LANDSCAPE}
 * and changes real estate dynamic configurations
 */
public abstract class AbstractActivity extends AppCompatActivity {

    @Nullable
    public FloatingActionButton fabButton;

    protected Toolbar toolbar;

    /**
     * Initialize any configuration from activity like orientation screen and toolbar
     */
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (ActivityUtils.isTablet(this))
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        else
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        setContentView(getLayoutRes());
        fabButton = (FloatingActionButton) findViewById(R.id.fabDefault);

        toolbar = (Toolbar) findViewById(R.id.toolbar);

        if (toolbar != null) {
            setSupportActionBar(toolbar);
            if (getStringResTitle() != null && !getStringResTitle().isEmpty() && toolbar != null)
                toolbar.setTitle(getStringResTitle());
        }
    }

    /**
     * Inflate the menu defined in inheritance class to put at toolbar
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        Integer menuRes = getMenuRes();
        if (menuRes == null)
            return true;

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(getMenuRes(), menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                ActivityUtils.setAnimationOnClosed(this);
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Gets layout resources from inheritance class and put in in {@link AppCompatActivity#setContentView(int)}.
     *
     * @return the layout res
     */
    public abstract int getLayoutRes();

    /**
     * Gets string resource to put in Toolbar Title.
     *
     * @return the string res title
     */
    public abstract String getStringResTitle();

    /**
     * Gets menu resource to put in toolbar.
     *
     * @return the menu res or NULL if the menu is not showing
     */
    public abstract Integer getMenuRes();
}
