package br.com.healthmack.sample;

import br.com.healthmack.utils.BasePresenter;
import br.com.healthmack.utils.BaseView;

public interface MakeSchedule {

    interface View extends BaseView<Presenter> {
    }

    interface Presenter extends BasePresenter {
    }
}
