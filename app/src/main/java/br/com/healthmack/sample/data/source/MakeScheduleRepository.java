package br.com.healthmack.sample.data.source;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class MakeScheduleRepository implements MakeScheduleDataSource {

    private final MakeScheduleDataSource mMakeScheduleDataSource;

    @Inject
    public MakeScheduleRepository(MakeScheduleDataSource makeScheduleDataSource) {
        this.mMakeScheduleDataSource = makeScheduleDataSource;
    }
}
