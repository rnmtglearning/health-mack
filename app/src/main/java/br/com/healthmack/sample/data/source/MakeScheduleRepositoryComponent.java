package br.com.healthmack.sample.data.source;

import javax.inject.Singleton;

import br.com.healthmack.modules.FirebaseDatabaseModule;
import dagger.Component;

@Singleton
@Component(modules = {MakeScheduleRepositoryModule.class, FirebaseDatabaseModule.class})
public interface MakeScheduleRepositoryComponent {

    MakeScheduleRepository getRepository();
}
