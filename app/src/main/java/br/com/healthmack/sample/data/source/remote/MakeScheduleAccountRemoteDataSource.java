package br.com.healthmack.sample.data.source.remote;

import com.google.firebase.database.FirebaseDatabase;

import javax.inject.Inject;
import javax.inject.Singleton;

import br.com.healthmack.sample.data.source.MakeScheduleDataSource;

@Singleton
public class MakeScheduleAccountRemoteDataSource implements MakeScheduleDataSource {

    private final FirebaseDatabase mDatabase;

    @Inject
    public MakeScheduleAccountRemoteDataSource(FirebaseDatabase mDatabase) {
        this.mDatabase = mDatabase;
    }
}
