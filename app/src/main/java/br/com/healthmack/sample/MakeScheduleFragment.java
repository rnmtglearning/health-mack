package br.com.healthmack.sample;

import android.view.View;
import android.view.ViewGroup;

import br.com.healthmack.AbstractFragment;
import br.com.healthmack.R;

public class MakeScheduleFragment extends AbstractFragment<MakeSchedule.Presenter> implements MakeSchedule.View {

    @Override
    public int getLayoutRes() {
        return R.layout.user_create_frag;
    }

    @Override
    public int getProgressBar() {
        return R.id.progressBar;
    }

    @Override
    public void onCustomCreateView(View view, ViewGroup container) {
    }
}