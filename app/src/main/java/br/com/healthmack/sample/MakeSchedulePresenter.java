package br.com.healthmack.sample;

import android.content.Context;

import javax.inject.Inject;

import br.com.healthmack.sample.data.source.MakeScheduleRepository;

final class MakeSchedulePresenter implements MakeSchedule.Presenter {

    private final MakeScheduleRepository mRepository;
    private final MakeSchedule.View mView;
    private Context mContext;

    @Inject
    MakeSchedulePresenter(MakeScheduleRepository repository, MakeSchedule.View view, Context context) {
        mRepository = repository;
        mView = view;
        mContext = context;
    }

    @Inject
    @Override
    public void setupListeners() {
        mView.setPresenter(this);
    }
}
