package br.com.healthmack.sample;

import android.os.Bundle;

import javax.inject.Inject;

import br.com.healthmack.AbstractActivity;
import br.com.healthmack.HealthMackApplication;
import br.com.healthmack.R;
import br.com.healthmack.modules.ContextModule;
import br.com.healthmack.utils.ActivityUtils;

public class MakeScheduleActivity extends AbstractActivity {

    @Inject
    MakeSchedulePresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        MakeScheduleFragment makeScheduleFragment = ActivityUtils.addFragmentToActivity(this, MakeScheduleFragment.class);

//        DaggerMakeScheduleComponent.builder()
//                .contextModule(new ContextModule(getBaseContext()))
//                .makeSchedulePresenterModule(new ListSchedulePresenterModule(makeScheduleFragment))
//                .makeScheduleRepositoryComponent(((HealthMackApplication) getApplication()).getMakeScheduleRepositoryComponent())
//                .build()
//                .inject(this);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.default_act;
    }

    @Override
    public String getStringResTitle() {
        return getString(R.string.Make_Schedule);
    }

    @Override
    public Integer getMenuRes() {
        return null;
    }
}