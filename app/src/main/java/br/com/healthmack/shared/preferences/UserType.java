package br.com.healthmack.shared.preferences;

import java.io.Serializable;

/**
 * Created by ramoncardoso on 11/13/17.
 */

public enum UserType implements Serializable {

    DOCTOR(1),
    USER(2);

    int value;

    UserType(int value) {
        this.value = value;
    }

    public static UserType newInstance(int value) {
        switch (value) {
            case 1:
                return DOCTOR;
            case 2:
                return USER;
            default:
                return USER;
        }
    }

    public int getValue() {
        return this.value;
    }
}
