package br.com.healthmack.shared.preferences;

import android.content.Context;
import android.content.SharedPreferences;

public class HealthMackPreferences {

    private static final String PREF_APP = "health-mack-preferences";

    private static final String PREF_USER_TYPE = "user-type";

    private final SharedPreferences preferences;
    private final SharedPreferences.Editor editor;

    public HealthMackPreferences(Context context) {
        preferences = context.getSharedPreferences(PREF_APP, 0);
        editor = preferences.edit();
    }

    public void setPrefUserType(UserType userType) {
        editor.putInt(PREF_USER_TYPE, userType.getValue());
        editor.commit();
    }

    public void setPrefUserType(int userType) {
        editor.putInt(PREF_USER_TYPE, userType);
    }

    public UserType getUserType() {
        return UserType.newInstance(preferences.getInt(PREF_USER_TYPE, 2));
    }

}
