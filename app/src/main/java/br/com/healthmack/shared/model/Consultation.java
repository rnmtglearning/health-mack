package br.com.healthmack.shared.model;

import android.support.annotation.NonNull;

import java.util.HashMap;
import java.util.Map;

public class Consultation implements Comparable<Consultation> {

    private String key;
    private String otherKey;
    private String userKey;
    private String specialty;
    private String date;
    private long dateMillis;
    private String day;
    private String hour;
    private String price;

    public Consultation() {
    }

    public Consultation(String otherKey, String userKey, String specialty, String date, long dateMillis, String day, String hour, String price) {
        this.otherKey = otherKey;
        this.userKey = userKey;
        this.specialty = specialty;
        this.date = date;
        this.dateMillis = dateMillis;
        this.day = day;
        this.hour = hour;
        this.price = price;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    public String getOtherKey() {
        return otherKey;
    }

    public void setOtherKey(String otherKey) {
        this.otherKey = otherKey;
    }

    public String getUserKey() {
        return userKey;
    }

    public void setUserKey(String userKey) {
        this.userKey = userKey;
    }

    public String getSpecialty() {
        return specialty;
    }

    public void setSpecialty(String specialty) {
        this.specialty = specialty;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getInverseDate() {
        String[] split = date.split("/");
        if (split.length > 2)
            return getDate();
        return split[1] + "/" + split[0];
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getHour() {
        return hour;
    }

    public void setHour(String hour) {
        this.hour = hour;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public long getDateMillis() {
        return dateMillis;
    }

    public void setDateMillis(long dateMillis) {
        this.dateMillis = dateMillis;
    }

    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("otherKey", otherKey);
        result.put("userKey", userKey);
        result.put("specialty", specialty);
        result.put("date", date);
        result.put("dateMillis", dateMillis);
        result.put("day", day);
        result.put("hour", hour);
        result.put("price", price);
        return result;
    }

    @Override
    public int compareTo(@NonNull Consultation consultation) {
        return this.date.replaceAll("/", "").compareTo(consultation.getDate().replaceAll("/", ""));
    }
}
