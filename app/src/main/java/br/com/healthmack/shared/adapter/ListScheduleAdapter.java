package br.com.healthmack.shared.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import br.com.healthmack.R;
import br.com.healthmack.makeschedule.MakeScheduleActivity;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ListScheduleAdapter extends RecyclerView.Adapter<ListScheduleAdapter.ViewHolder> {

    private String mDoctorKey;
    private String mSpecialty;
    private OnClickListener mListener;
    private Activity mContext;
    private List<String> datasource = new ArrayList<>();
    private List<String> selecteds = new ArrayList<>();

    public ListScheduleAdapter(Activity context) {
        mContext = context;
    }

    public ListScheduleAdapter(OnClickListener listener, Activity context) {
        mListener = listener;
        mContext = context;
    }

    public void setDatasource(List<String> datasource) {
        this.datasource.addAll(datasource);
    }

    public List<String> getDatasource() {
        return datasource;
    }

    public List<String> getSelecteds() {
        return selecteds;
    }

    public void setSelecteds(List<String> selecteds) {
        this.selecteds = selecteds;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.make_schedule_card, parent, false);
        return new ViewHolder(root);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        if (mContext instanceof MakeScheduleActivity) {
            holder.text.setText(datasource.get(holder.getAdapterPosition()));

            if (selecteds.contains(datasource.get(position))) {
                holder.check.setChecked(true);
                holder.text.setTextColor(mContext.getResources().getColor(R.color.colorPrimary));
            }

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!holder.check.isChecked())
                        holder.text.setTextColor(mContext.getResources().getColor(R.color.colorPrimary));
                    else
                        holder.text.setTextColor(mContext.getResources().getColor(android.R.color.black));

                    holder.check.setChecked(!holder.check.isChecked());

                    if (holder.check.isChecked())
                        selecteds.add(datasource.get(holder.getAdapterPosition()));
                    else if (selecteds.contains(datasource.get(holder.getAdapterPosition())))
                        selecteds.remove(datasource.get(holder.getAdapterPosition()));
                }
            });
        } else {
            holder.check.setVisibility(View.GONE);
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null) {
                        mListener.onClick(datasource.get(holder.getAdapterPosition()));
                    }
                }
            });
            holder.text.setText(datasource.get(holder.getAdapterPosition()));
        }
    }

    @Override
    public int getItemCount() {
        return datasource.size();
    }

    private boolean isDayName(Context context, String value) {
        if (value.equals(context.getString(R.string.Monday)) ||
                value.equals(context.getString(R.string.Tuesday)) ||
                value.equals(context.getString(R.string.Wednesday)) ||
                value.equals(context.getString(R.string.Thursday)) ||
                value.equals(context.getString(R.string.Friday)) ||
                value.equals(context.getString(R.string.Saturday)) ||
                value.equals(context.getString(R.string.Sunday))) {
            return true;
        }
        return false;
    }

    public interface OnClickListener {
        void onClick(String hour);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.scheduleHourText)
        TextView text;
        @BindView(R.id.scheduleHourCheck)
        CheckBox check;
        @BindView(R.id.card)
        CardView card;

        public ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}