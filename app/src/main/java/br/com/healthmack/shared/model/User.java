package br.com.healthmack.shared.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import br.com.healthmack.utils.ValidateUtils;

public class User implements Serializable {
    private String uid;
    private String name;
    private String email;
    private String rg;
    private String cpf;
    private String birthday;
    private String cellPhone;
    private String crm;
    private List<String> specialties;
    private String password;
    private String rePassword;
    private boolean doctor;

    public User() {
    }

    public User(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public User(String name, String email, String rg, String cpf, String birthday, String cellPhone, String password, String rePassword) {
        this.name = name;
        this.email = email;
        this.rg = rg;
        this.cpf = cpf;
        this.birthday = birthday;
        this.cellPhone = cellPhone;
        this.password = password;
        this.rePassword = rePassword;
    }

    public User(String name, String email, String rg, String cpf, String birthday,
                String cellPhone, String password, String rePassword, boolean isDoctor) {
        User user = new User(name, email, rg, cpf, birthday, cellPhone, password, rePassword);
        user.setDoctor(isDoctor);
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRePassword() {
        return rePassword;
    }

    public void setRePassword(String rePassword) {
        this.rePassword = rePassword;
    }

    public boolean isDoctor() {
        return doctor;
    }

    public void setDoctor(boolean doctor) {
        this.doctor = doctor;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getCellPhone() {
        return cellPhone;
    }

    public void setCellPhone(String cellPhone) {
        this.cellPhone = cellPhone;
    }

    public String getCrm() {
        return crm;
    }

    public void setCrm(String crm) {
        this.crm = crm;
    }

    public List<String> getSpecialties() {
        return specialties;
    }

    public void setSpecialties(List<String> specialties) {
        this.specialties = specialties;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public boolean isValidEmail() {
        return ValidateUtils.checkIsEmail(this.email);
    }

    public boolean isValidPassword() {
        return this.password.length() > 5 && this.password.length() <= 15;
    }

    public boolean isValidCpf() {
        return this.cpf.length() == 11;
    }

    public boolean isValidBirthday() {
        return this.birthday.length() == 8;
    }

    public boolean isValidCellPhone() {
        return this.cellPhone.length() == 11;
    }

    public boolean isValidRePassword() {
        return this.password.equals(this.rePassword);
    }

    public boolean isValidRg() {
        return this.rg.length() >= 8;
    }

    public boolean isValidName() {
        return this.name.length() > 2;
    }

    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("name", name);
        result.put("email", email);
        result.put("rg", rg);
        result.put("cpf", cpf);
        result.put("birthday", birthday);
        result.put("cellphone", cellPhone);
        result.put("crm", crm);
        result.put("doctor", doctor);
        return result;
    }
}
