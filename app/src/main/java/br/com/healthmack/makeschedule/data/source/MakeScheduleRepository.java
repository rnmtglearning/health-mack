package br.com.healthmack.makeschedule.data.source;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class MakeScheduleRepository implements MakeScheduleDataSource {

    private final MakeScheduleDataSource mMakeScheduleDataSource;

    @Inject
    public MakeScheduleRepository(MakeScheduleDataSource makeScheduleDataSource) {
        this.mMakeScheduleDataSource = makeScheduleDataSource;
    }

    @Override
    public void saveSchedule(List<String> days, List<String> hours, MakeScheduleCallback callback) {
        mMakeScheduleDataSource.saveSchedule(days, hours, callback);
    }

    @Override
    public void loadSelecteds(MakeScheduleCallback callback) {
        mMakeScheduleDataSource.loadSelecteds(callback);
    }
}
