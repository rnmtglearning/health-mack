package br.com.healthmack.makeschedule;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import br.com.healthmack.R;
import br.com.healthmack.shared.adapter.ListScheduleAdapter;
import butterknife.BindArray;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by Ramon Cardoso on 24/10/2017.
 */

public class WeekendFragment extends Fragment {

    private static final String ARG_TITLE = "title";

    @BindView(R.id.scheduleList)
    RecyclerView mRecycler;

    @BindArray(R.array.days_week)
    String[] days;
    @BindArray(R.array.hours_day)
    String[] hours;

    private Unbinder unbinder;
    private ListScheduleAdapter mAdapter;

    public WeekendFragment() {
    }

    public static WeekendFragment newInstance(String title) {
        Bundle bundle = new Bundle();
        bundle.putString(ARG_TITLE, title);
        WeekendFragment weekendFragment = new WeekendFragment();
        weekendFragment.setArguments(bundle);
        return weekendFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.make_schedule_frag, container, false);
        unbinder = ButterKnife.bind(this, view);

        mRecycler.setHasFixedSize(true);
        mRecycler.setItemViewCacheSize(30);

        mAdapter = new ListScheduleAdapter(getActivity());

        if (Objects.equals(getArguments().getString(ARG_TITLE), MakeScheduleActivity.TITLE_DAYS))
            mAdapter.setDatasource(Arrays.asList(days));
        else if (Objects.equals(getArguments().getString(ARG_TITLE), MakeScheduleActivity.TITLE_HOURS))
            mAdapter.setDatasource(Arrays.asList(hours));

        mRecycler.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));

        return view;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    public List<String> getSelecteds() {
        return mAdapter == null ? null : mAdapter.getSelecteds();
    }

    public void setSelected(List<String> source) {
       mAdapter.setSelecteds(source);
       mRecycler.setAdapter(mAdapter);
//       mAdapter.notifyDataSetChanged();
    }
}
