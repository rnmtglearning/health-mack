package br.com.healthmack.makeschedule;

import br.com.healthmack.modules.ContextModule;
import br.com.healthmack.makeschedule.data.source.MakeScheduleRepositoryComponent;
import br.com.healthmack.utils.FragmentScoped;
import dagger.Component;

@FragmentScoped
@Component(dependencies = MakeScheduleRepositoryComponent.class, modules = {MakeSchedulePresenterModule.class,
        ContextModule.class})
public interface MakeScheduleComponent {

    void inject(MakeScheduleActivity makeScheduleActivity);
}
