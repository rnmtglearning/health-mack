package br.com.healthmack.makeschedule;

import java.util.List;

public interface MakeSchedule {

    interface View {
        void showProgressBar();

        void hideProgressBar();

        void onScheduleSaved();

        void onScheduleLoaded(List<String> days, List<String> hours);
    }

    interface Presenter {

        void saveSchedule(List<String> days, List<String> hours);

        void loadSelected();
    }
}
