package br.com.healthmack.makeschedule;

import dagger.Module;
import dagger.Provides;

@Module
public class MakeSchedulePresenterModule {

    private final MakeSchedule.View mView;

    public MakeSchedulePresenterModule(MakeSchedule.View view) {
        mView = view;
    }

    @Provides
    MakeSchedule.View providesView() {
        return mView;
    }
}
