package br.com.healthmack.makeschedule.data.source;

import javax.inject.Singleton;

import br.com.healthmack.makeschedule.data.source.remote.MakeScheduleAccountRemoteDataSource;
import dagger.Binds;
import dagger.Module;

@Module
public abstract class MakeScheduleRepositoryModule {

    @Singleton
    @Binds
    abstract MakeScheduleDataSource provideDataSource(MakeScheduleAccountRemoteDataSource remoteDataSource);
}
