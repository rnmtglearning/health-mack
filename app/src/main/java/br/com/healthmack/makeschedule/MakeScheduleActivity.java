package br.com.healthmack.makeschedule;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionButton;

import java.util.List;

import javax.inject.Inject;

import br.com.healthmack.AbstractActivity;
import br.com.healthmack.HealthMackApplication;
import br.com.healthmack.R;
import br.com.healthmack.modules.ContextModule;
import br.com.healthmack.utils.ActivityUtils;
import br.com.healthmack.utils.ToastHelper;
import br.com.healthmack.utils.ViewPagerAdapter;
import butterknife.BindView;
import butterknife.ButterKnife;

public class MakeScheduleActivity extends AbstractActivity implements MakeSchedule.View {

    public static final String TITLE_DAYS = "Dias";
    public static final String TITLE_HOURS = "Horário";
    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @BindView(R.id.tablayout)
    TabLayout tabLayout;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.scheduleDone)
    FloatingActionButton doneButton;

    @Inject
    MakeSchedulePresenter presenter;
    private WeekendFragment days;
    private WeekendFragment hours;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ButterKnife.bind(this);

        days = WeekendFragment.newInstance(TITLE_DAYS);
        hours = WeekendFragment.newInstance(TITLE_HOURS);

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(days, TITLE_DAYS);
        adapter.addFragment(hours, TITLE_HOURS);

        viewPager.setAdapter(adapter);

        tabLayout.setupWithViewPager(viewPager);
        TabLayout.Tab tabAt0 = tabLayout.getTabAt(0);
        TabLayout.Tab tabAt1 = tabLayout.getTabAt(1);

        if (tabAt0 != null)
            tabAt0.setIcon(R.drawable.ic_calendar_day);
        if (tabAt1 != null)
            tabAt1.setIcon(R.drawable.ic_clock);

        DaggerMakeScheduleComponent.builder()
                .contextModule(new ContextModule(getBaseContext()))
                .makeSchedulePresenterModule(new MakeSchedulePresenterModule(this))
                .makeScheduleRepositoryComponent(((HealthMackApplication) getApplication()).getMakeScheduleRepositoryComponent())
                .build()
                .inject(this);

        presenter.loadSelected();

        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.saveSchedule(days.getSelecteds(), hours.getSelecteds());
            }
        });

        hideProgressBar();
    }

    @Override
    public void showProgressBar() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onScheduleSaved() {
        ToastHelper.show(this, R.string.Schedule_saved, Toast.LENGTH_SHORT);
        finish();
        ActivityUtils.setAnimationOnClosed(this);
    }

    @Override
    public void onScheduleLoaded(List<String> days, List<String> hours) {
        this.days.setSelected(days);
        this.hours.setSelected(hours);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.make_schedule_act;
    }

    @Override
    public String getStringResTitle() {
        return getString(R.string.Make_Schedule);
    }

    @Override
    public Integer getMenuRes() {
        return null;
    }
}