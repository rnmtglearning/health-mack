package br.com.healthmack.makeschedule.data.source;

import java.util.List;

public interface MakeScheduleDataSource {

    void saveSchedule(List<String> days, List<String> hours, MakeScheduleCallback callback);

    void loadSelecteds(MakeScheduleCallback callback);

    interface MakeScheduleCallback {
        void onScheduleSaved();
        void onScheduleLoaded(List<String> days, List<String> hours);
    }
}
