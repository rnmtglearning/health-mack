package br.com.healthmack.makeschedule.data.source.remote;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.OnDisconnect;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import br.com.healthmack.makeschedule.data.source.MakeScheduleDataSource;
import br.com.healthmack.utils.FirebaseChilds;

@Singleton
public class MakeScheduleAccountRemoteDataSource implements MakeScheduleDataSource {

    private final FirebaseDatabase mDatabase;
    private final FirebaseAuth mAuth;

    @Inject
    public MakeScheduleAccountRemoteDataSource(FirebaseDatabase mDatabase, FirebaseAuth mAtuh) {
        this.mDatabase = mDatabase;
        this.mAuth = mAtuh;
    }

    @Override
    public void saveSchedule(List<String> days, List<String> hours, MakeScheduleCallback callback) {
        final DatabaseReference child = mDatabase.getReference(FirebaseChilds.CHILD_SCHEDULE)
                .child(mAuth.getCurrentUser().getUid());

        child.child(FirebaseChilds.CHILD_SCHEDULE_DAY).setValue(days);
        child.child(FirebaseChilds.CHILD_SCHEDULE_HOUR).setValue(hours);
        
        callback.onScheduleSaved();
    }


    @Override
    public void loadSelecteds(final MakeScheduleCallback callback) {

        final List<String> days = new ArrayList<>();
        final List<String> hours = new ArrayList<>();

        final DatabaseReference child = mDatabase.getReference(FirebaseChilds.CHILD_SCHEDULE)
                .child(mAuth.getCurrentUser().getUid());

        child.child(FirebaseChilds.CHILD_SCHEDULE_DAY).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot snap: dataSnapshot.getChildren()) {
                    days.add(snap.getValue(String.class));
                }

                child.child(FirebaseChilds.CHILD_SCHEDULE_HOUR).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for (DataSnapshot snap: dataSnapshot.getChildren()) {
                            hours.add(snap.getValue(String.class));
                        }

                        callback.onScheduleLoaded(days, hours);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
