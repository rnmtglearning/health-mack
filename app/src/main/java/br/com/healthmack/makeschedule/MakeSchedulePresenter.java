package br.com.healthmack.makeschedule;

import android.content.Context;

import java.util.List;

import javax.inject.Inject;

import br.com.healthmack.makeschedule.data.source.MakeScheduleDataSource;
import br.com.healthmack.makeschedule.data.source.MakeScheduleRepository;

final class MakeSchedulePresenter implements MakeSchedule.Presenter, MakeScheduleDataSource.MakeScheduleCallback {

    private final MakeScheduleRepository mRepository;
    private final MakeSchedule.View mView;
    private Context mContext;

    @Inject
    MakeSchedulePresenter(MakeScheduleRepository repository, MakeSchedule.View view, Context context) {
        mRepository = repository;
        mView = view;
        mContext = context;
    }

    @Override
    public void saveSchedule(List<String> days, List<String> hours) {
        mView.showProgressBar();
        mRepository.saveSchedule(days, hours, this);
    }

    @Override
    public void onScheduleSaved() {
        mView.hideProgressBar();
        mView.onScheduleSaved();
    }

    @Override
    public void loadSelected() {
        mView.showProgressBar();
        mRepository.loadSelecteds(this);
    }

    @Override
    public void onScheduleLoaded(List<String> days, List<String> hours) {
        mView.hideProgressBar();
        mView.onScheduleLoaded(days, hours);
    }
}
